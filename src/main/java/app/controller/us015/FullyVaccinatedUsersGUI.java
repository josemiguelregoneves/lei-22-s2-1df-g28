package app.controller.us015;

import app.domain.conceptual.VaccinationCenter;
import app.domain.conceptual.VaccineSchedule;
import app.domain.dto.VaccineScheduleDTO;
import app.domain.model.Company;
import app.domain.shared.Validator;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;

public class FullyVaccinatedUsersGUI implements Initializable {

    /**
     * text field for minimum date
     */
    @FXML
    private TextField txtMinDate;

    /**
     * text field for maximum date
     */
    @FXML
    private TextField txtMaxDate;

    /**
     * button to export the data to a .csv file
     */
    @FXML
    private Button btnExport;

    /**
     * txt field for .csv file path
     */
    @FXML
    private TextField txtFilePath;

    /**
     * button to repeat the process
     */
    @FXML
    private Button btnAgain;

    /**
     * controller for get the fully vaccinated users
     */
    private static FullyVaccinatedUsersController ctrl;

    /**
     * vaccination center
     */
    private static VaccinationCenter center;

    /**
     * method to get the vaccination center value
     *
     * @return center
     */
    public static VaccinationCenter getCenter() {
        return center;
    }

    /**
     * method to set the vaccination center value
     *
     * @param center
     */
    public static void setCenter(VaccinationCenter center) {
        FullyVaccinatedUsersGUI.center = center;
    }

    /**
     * method to export the data to a .csv file
     *
     * @param actionEvent
     */
    public void exportDataToCsvFile(ActionEvent actionEvent) {
        /**
         * minimum date on string
         */
        String strMinDate = txtMinDate.getText();

        /**
         * minimum date
         */
        Date minDate = Validator.convertStringToDate(strMinDate);

        /**
         * maximum date on string
         */
        String strMaxDate = txtMaxDate.getText();

        /**
         * maximum date
         */
        Date maxDate = Validator.convertStringToDate(strMaxDate);

        /**
         * file path
         */
        String path = txtFilePath.getText();

        Alert a;
        if (strMinDate != null && strMaxDate != null && path != null) {
            /**
             * dates list
             */
            ArrayList<Date> datesList = ctrl.datesBetweenInterval(minDate, maxDate);

            /**
             * completed schedules of the center
             */
            ArrayList<VaccineScheduleDTO> fullVaccinatedListDto = ctrl.getFullCompletedSchedules(center);
            ArrayList<VaccineSchedule> fullVaccinatedList = Company.getListSchedulesMapper().toModel(fullVaccinatedListDto);

            /**
             * total full vaccinated per day, on the defined interval
             */
            int[] fullVaccinatedPerDay = ctrl.getFullVaccinatedNumberPerDay(datesList, fullVaccinatedList);

            /**
             * write the data on the .csv file
             */
            ctrl.writeFullVaccinated(new File(path), datesList, fullVaccinatedPerDay);

            a = new Alert(Alert.AlertType.CONFIRMATION);
            a.setTitle("Export Data Complete!");
            a.setContentText("Export Data Complete!");
            a.show();
        } else {
            a = new Alert(Alert.AlertType.ERROR);
            a.setTitle("Cannot Export Data");
            a.setContentText("Invalid Date Interval/Path");
            a.show();
        }
    }

    /**
     * method to repeat the process
     *
     * @param actionEvent
     */
    public void repeatProcess(ActionEvent actionEvent) {
        /**
         * clear the text field
         */
        txtMinDate.clear();

        /**
         * clear the text field
         */
        txtMaxDate.clear();

        /**
         * clear the text field
         */
        txtFilePath.clear();
    }

    /**
     * initializable method
     *
     * @param url
     * @param resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        ctrl = new FullyVaccinatedUsersController();
    }
}
