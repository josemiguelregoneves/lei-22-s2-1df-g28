package app.controller.us015;

import app.domain.conceptual.VaccinationCenter;
import app.domain.conceptual.VaccineSchedule;
import app.domain.dto.VaccineScheduleDTO;
import app.domain.model.Company;
import app.domain.shared.Validator;

import java.io.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class FullyVaccinatedUsersController {

  /**
   * method to get the full completed schedules of a center
   *
   * @param center
   *
   * @return fullCompleted
   */
  public ArrayList<VaccineScheduleDTO> getFullCompletedSchedules(VaccinationCenter center) {
    ArrayList<VaccineSchedule> completed = center.getCompletedSchedulesList();
    ArrayList<VaccineSchedule> fullVaccinated = new ArrayList<>();
    for (VaccineSchedule schedule : completed) {
      if (Validator.clientFullVaccinated(schedule)) {
        fullVaccinated.add(schedule);
      }
    }
    return Company.getListSchedulesMapper().toDto(fullVaccinated);
  }

  /**
   * method to get the list of all dates between two given dates
   *
   * @param minDate
   * @param maxDate
   *
   * @return datesList
   */
  public ArrayList<Date> datesBetweenInterval(Date minDate, Date maxDate) {
    ArrayList<Date> datesInterval = new ArrayList<>();
    datesInterval.add(minDate);
    while (!minDate.equals(maxDate)) {
      Calendar cal = Calendar.getInstance();
      cal.setTime(minDate);
      cal.add(Calendar.DATE, 1);
      minDate = cal.getTime();
      datesInterval.add(minDate);
    }
    return datesInterval;
  }

  /**
   * method to get the number of full vaccinated clients per day
   *
   * @param datesInterval
   * @param fullCompletedSchedules
   *
   * @return fullVaccinatedArray
   */
  public int[] getFullVaccinatedNumberPerDay(ArrayList<Date> datesInterval,
      ArrayList<VaccineSchedule> fullCompletedSchedules) {
      int[] fullVaccinated = new int[datesInterval.size()];
      for (int i = 0; i < datesInterval.size(); i++) {
        int total = 0;
        for (int j = 0; j < fullCompletedSchedules.size(); j++) {
          if (Validator.dateEqualsToDate(fullCompletedSchedules.get(j).getScheduleDate(), datesInterval.get(i))) {
            total++;
          }
        }
        fullVaccinated[i] = total;
      }
      return fullVaccinated;
  }

  /**
   * method to print the data in a .csv file
   *
   * @param file
   * @param datesList
   * @param fullVaccinatedList
   */
  public void writeFullVaccinated(File file, ArrayList<Date> datesList, int[] fullVaccinatedList) {
    try {
      PrintWriter write = new PrintWriter(file);
      for (int i = 0; i < fullVaccinatedList.length; i++) {
        write.write(String.format("Date: %s - Total Full Vaccinated: %d\n",
            Validator.convertDateToString(datesList.get(i)), fullVaccinatedList[i]));
      }
      write.close();
    } catch (FileNotFoundException e) {
      throw new IllegalArgumentException(e);
    }
  }

}
