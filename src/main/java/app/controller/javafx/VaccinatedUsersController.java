package app.controller.javafx;

import app.domain.conceptual.VaccinationCenter;
import app.domain.model.Company;
import app.domain.shared.Alert;

import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Properties;

public class VaccinatedUsersController {

  /**
   * list of all centers DTO
   */
  private ArrayList<VaccinationCenter> centerList;

  /**
   * Live reader for config.properties
   */
  private InputStream inputStream;

  /**
   * constructor that initialize the controller
   */
  public VaccinatedUsersController() {
    this.centerList = Company.getCenterStore().getCentersList();
  }

  /**
   * get configurations from config.properties file
   *
   * @return time for configuration
   *
   * @throws IOException in case of ocuring file errors
   */
  public String getConfigs() throws IOException {

    String configs = "";
    try {
      Properties prop = new Properties();
      String propFileName = "config.properties";

      inputStream = getClass().getClassLoader().getResourceAsStream((propFileName));

      if (inputStream != null) {
        prop.load(inputStream);
      } else {
        throw new FileNotFoundException("Property file '" + propFileName + "' not found in the classpath");
      }

      configs = prop.getProperty("TIME");
    } catch (Exception e) {
      System.out.println("Exception: " + e);
    } finally {
      inputStream.close();
    }
    return configs;
  }

  /**
   * Writes information of all Centers on a CSV file
   */
  public void fileWritter() {
    File file = new File("DGSOutput.csv");
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy;HH:mm:ss");
    LocalDateTime now = LocalDateTime.now();
    try {
      FileWriter outputFile = new FileWriter(file, true);
      for (VaccinationCenter center : centerList) {
        String line = center.getName() + ";" + center.getCompletedSchedulesList().size();
        outputFile.write(dtf.format(now) + ";" + line + "\n");
      }
      outputFile.close();
    } catch (IOException e) {
      Alert.message(e.getMessage());
    }
  }
}
