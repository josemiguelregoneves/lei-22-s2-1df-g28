
package app.controller.register;

import app.controller.App;
import app.domain.dto.EmployeeDTO;
import app.domain.model.Company;
import app.domain.conceptual.Employee;
import app.domain.conceptual.VaccinationCenter;

public class RegisterNewEmployeeController {

    public Employee createEmployee(EmployeeDTO employee) {
        return Company.geteMapper().toModel(employee);
    }

    public void saveEmployee(Employee employee) {
        App.getInstance().getAuthFacade().addUserWithRole(employee.getName(), employee.getEmail(), employee.getPassword(), employee.getIdRole());
        Company.geteStore().saveEmployee(employee);
    }

}
