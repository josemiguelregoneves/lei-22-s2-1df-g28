package app.controller.register;

import app.domain.conceptual.VaccineSchedule;
import app.domain.dto.VaccineScheduleDTO;
import app.domain.model.Company;

public class VaccineScheduleController {

    /**
     * method to create a new vaccine schedule model
     *
     * @param vcSchedule vaccine schedule DTO
     * @return vcSchedule
     */
    public VaccineSchedule createVaccineSchedule(VaccineScheduleDTO vcSchedule) {
        return Company.getVcScheduleMapper().toModel(vcSchedule);
    }

    /**
     * method to save a vaccine schedule
     *
     * @param vcSchedule
     */
    public void saveVaccineSchedule(VaccineSchedule vcSchedule) {
        Company.getScheduleStore().saveSchedule(vcSchedule);
    }
}
