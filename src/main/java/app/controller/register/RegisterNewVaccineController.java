package app.controller.register;

import app.domain.conceptual.Employee;
import app.domain.conceptual.Vaccine;
import app.domain.conceptual.VaccineType;
import app.domain.dto.EmployeeDTO;
import app.domain.dto.VaccineDTO;
import app.domain.model.*;

import java.util.ArrayList;

public class RegisterNewVaccineController {

    public Vaccine createVaccine(VaccineDTO vaccine) {
        return Company.getVcMapper().toModel(vaccine);
    }

    public void saveVaccine(Vaccine vaccine) {
        Company.getVcStore().saveVaccine(vaccine);
    }

}

