package app.controller.register;

import app.domain.model.RegisterCSVWithHeader;
import app.domain.model.RegisterCSVWithoutHeader;
import app.domain.interfaces.CSVVariations;

import java.io.*;
import java.util.Scanner;

public class UserRegistrationViaCSVController {
    /**
     * Decide whether or not the file has a header
     *
     * @param file File loaded to the program
     * @throws FileNotFoundException to prevent possible errors (File not found or existant)
     */
    public void chooseCsvReader(File file) throws FileNotFoundException {
        CSVVariations[] arrCsvVariations = {new RegisterCSVWithHeader(), new RegisterCSVWithoutHeader()};
        if (verifyFileHeader(file)) {
            readCsvFile(arrCsvVariations[0], file);
        } else {
            readCsvFile(arrCsvVariations[1], file);
        }
    }

    /**
     * method to verify if exists header in csv file
     *
     * @param file File loaded to the program
     * @return "true" if the file has header, "false" if the file doesn't have header
     * @throws FileNotFoundException to prevent possible errors (File not found or existant)
     */
    private boolean verifyFileHeader(File file) throws FileNotFoundException {
        Scanner sc = new Scanner(file);
        boolean validate = sc.nextLine().contains(";");
        sc.close();
        return validate;
    }

    /**
     * method that confirms polimorphism
     *
     * @param csvVariations
     * @param file
     * @throws FileNotFoundException
     */
    private void readCsvFile(CSVVariations csvVariations, File file) throws FileNotFoundException {
        csvVariations.readFile(file);
    }
}
