package app.controller.register;

import app.controller.App;
import app.domain.conceptual.Client;
import app.domain.conceptual.Employee;
import app.domain.dto.ClientDTO;
import app.domain.dto.EmployeeDTO;
import app.domain.model.Company;
import app.domain.shared.Constants;

import java.util.Date;

public class RegisterClientController {

    public Client createClient(ClientDTO snsUser) {
        return Company.getSnsUserMapper().toModel(snsUser);
    }

    public void saveClient(Client snsUser) {
        App.getInstance().getAuthFacade().addUserWithRole(snsUser.getName(), snsUser.getEmail(), snsUser.getPassword(), Constants.ROLE_SNS_USER);
        Company.getClientStore().saveClient(snsUser);
    }

}