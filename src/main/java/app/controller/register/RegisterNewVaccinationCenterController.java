package app.controller.register;

import app.domain.conceptual.Employee;
import app.domain.dto.EmployeeDTO;
import app.domain.dto.VaccinationCenterDTO;
import app.domain.model.Company;
import app.domain.conceptual.VaccinationCenter;

import java.sql.Time;


public class RegisterNewVaccinationCenterController {

    public VaccinationCenter createCenter(VaccinationCenterDTO center) {
        return Company.getCenterMapper().toModel(center);
    }

    public void saveCenter(VaccinationCenter center) {
        Company.getCenterStore().saveCenter(center);
    }

}
