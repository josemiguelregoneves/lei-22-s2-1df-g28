package app.controller.register;

import app.domain.conceptual.Vaccine;
import app.domain.conceptual.VaccineType;
import app.domain.dto.VaccineDTO;
import app.domain.dto.VaccineTypeDTO;
import app.domain.model.*;

public class RegisterVaccineTypeController {

    public VaccineType createVaccineType(VaccineTypeDTO vcType) {
        return Company.getVcTypeMapper().toModel(vcType);
    }

    public void saveVaccineType(VaccineType vcType) {
        Company.getVcTypeStore().saveVaccineType(vcType);
    }

}
