package app.controller.us007;

import app.domain.conceptual.AdverseReactions;
import app.domain.conceptual.Client;
import app.domain.conceptual.VaccineSchedule;
import app.domain.model.Company;
import app.domain.shared.Alert;
import app.domain.shared.Validator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class RecordAdverseReactionsController {

    /**
     * method to record adverse reactions to a list contained in a store
     *
     * @param client
     * @param reaction
     */
    public void recordAdverseReaction(Client client, String reaction) {
        AdverseReactions adverseReaction = new AdverseReactions(client, reaction);
        Company.getReactionsStore().getReactionsList().add(adverseReaction);
    }

}
