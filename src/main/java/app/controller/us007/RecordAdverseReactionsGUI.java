package app.controller.us007;

import app.domain.conceptual.Client;
import app.domain.model.Company;
import app.domain.shared.Validator;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

public class RecordAdverseReactionsGUI implements Initializable {

    /**
     * text fiels for sns number
     */
    @FXML
    private TextField txtSnsNumber;

    /**
     * text area for adverse reactions
     */
    @FXML
    private TextArea txtAdverseReaction;

    /**
     * button to record the adverse reactions
     */
    @FXML
    private Button btnRecord;

    /**
     * client
     */
    private Client client;

    /**
     * reaction
     */
    private String reaction;

    /**
     * controller to record adverse reactions
     */
    private static RecordAdverseReactionsController ctrl;

    /**
     * method to record and save adverse reactions
     * @param actionEvent
     */
    public void recordAdverseReaction(ActionEvent actionEvent) {

        /**
         * get the snsNumber
         */
        int snsNumber = Integer.parseInt(txtSnsNumber.getText());

        /**
         * get the client
         */
        client = Validator.getClientBySnsNumber(snsNumber);

        /**
         * get the description of the reactions
         */
        reaction = txtAdverseReaction.getText();

        Alert a;
        if (client != null && reaction != null && Validator.clientVaccinated(snsNumber)) {
            /**
             * record the adverse reaction
             */
            ctrl.recordAdverseReaction(client, reaction);

            a = new Alert(Alert.AlertType.CONFIRMATION);
            a.setTitle("Adverse Reaction Registered!");
            a.setContentText(String.format("Client: %s\nReaction: %s", client, reaction));
            a.show();
        } else {
            a = new Alert(Alert.AlertType.ERROR);
            a.setTitle("Cannot Register Adverse Reaction!");
            a.setContentText(String.format("Client: %s\nReaction: %s", client, reaction));
            a.show();
        }

        /**
         * clear the text field
         */
        txtSnsNumber.clear();

        /**
         * clear the text field
         */
        txtAdverseReaction.clear();
    }

    /**
     * initializable method
     *
     * @param url
     * @param resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        ctrl = new RecordAdverseReactionsController();
        client = null;
    }
}
