package app.controller.us016;

import app.domain.conceptual.VaccinationCenter;
import app.domain.conceptual.VaccineSchedule;
import app.domain.model.Company;
import app.domain.shared.Constants;
import app.domain.shared.Validator;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

import java.net.URL;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.ResourceBundle;

public class EfficiencyCenterGUI implements Initializable {

    /**
     * choice box for interval (minutes)
     */
    @FXML
    private ChoiceBox<Integer> boxInterval;

    /**
     * text fiels to date
     */
    @FXML
    private TextField txtDate;

    /**
     * button to get the max sublist
     */
    @FXML
    private Button btnSublist;

    /**
     * label to show the max sublist
     */
    @FXML
    private TextArea txtSublist;

    /**
     * label to show the sum of the elements of the max sublist
     */
    @FXML
    private Label lblMaxSum;

    /**
     * label to show the maximum hour
     */
    @FXML
    private Label lblMaxHour;

    /**
     * label to show the minimum hour
     */
    @FXML
    private Label lblMinHour;

    /**
     * controller to get the maximum sublist
     */
    private static EfficiencyCenterController ctrl;

    /**
     * array list of the completed schedules
     */
    private ArrayList<VaccineSchedule> completedOfDate;

    /**
     * date
     */
    private Date date;

    /**
     * vaccination center
     */
    private static VaccinationCenter center;

    /**
     * method to get the vaccination center value
     *
     * @return center
     */
    public static VaccinationCenter getCenter() {
        return center;
    }

    /**
     * method to set the vaccination center value
     *
     * @param center
     */
    public static void setCenter(VaccinationCenter center) {
        EfficiencyCenterGUI.center = center;
    }

    /**
     * method to get the max sublis
     *
     * @param actionEvent
     */
    public void getMaxSublist(ActionEvent actionEvent) {
        /**
         * interval (minutes)
         */
        int interval = boxInterval.getValue();

        /**
         * string of date
         */
        String strDate = txtDate.getText();

        /**
         * date
         */
        date = Validator.convertStringToDate(strDate);

        /**
         * completed schedules of the selected date
         */
        completedOfDate = Company.getScheduleStore().getCompletedSchedulesByDate(date);

        if (boxInterval.getValue() != null && date != null && !completedOfDate.isEmpty() && completedOfDate != null) {
            /**
             * diff
             */
            int[] diffArray = ctrl.getDiffArray(interval, date);
            int[] maxSublist = ctrl.getMaxSublist(diffArray);
            txtSublist.setText(Arrays.toString(maxSublist));

            int maxSum = ctrl.getMaxSumSublist(maxSublist);
            lblMaxSum.setText(String.valueOf(maxSum));

            Time[] timeInterval = ctrl.getMinMaxHour(maxSublist, diffArray, interval);
            lblMinHour.setText(String.valueOf(timeInterval[0]));
            lblMaxHour.setText(String.valueOf(timeInterval[1]));
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("Invalid Date/Interval!");
            alert.show();
        }
    }

    /**
     * initializable methods
     *
     * @param url
     * @param resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        ctrl = new EfficiencyCenterController();

        /**
         * create an array list with all the available options of intervals in minutes
         */
        ArrayList<Integer> intervals = new ArrayList<>();
        intervals.add(1);
        intervals.add(5);
        intervals.add(10);
        intervals.add(20);
        intervals.add(30);

        /**
         * add that list to the choice box
         */
        boxInterval.getItems().addAll(intervals);
    }
}
