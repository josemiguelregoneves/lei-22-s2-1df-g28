package app.controller.us016;

import app.domain.conceptual.VaccineSchedule;
import app.domain.model.Company;
import app.domain.shared.Constants;
import app.domain.shared.GetConfigProperties;
import app.domain.shared.Validator;
import app.domain.us016.HighestSublist;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class EfficiencyCenterController {

    /**
     * method to get a matrice where the elements of the first collumn are the number of arrival times between the specified interval
     * the second collumn are the number of the leaving time between the specified interval
     *
     * @param interval
     * @param date
     *
     * @return matriceArrivalLeave
     */
    private int[][] getMatriceArrivalLeave(int interval, Date date) {
        ArrayList<VaccineSchedule> completedList = Company.getScheduleStore().getCompletedSchedulesByDate(date);
        int size = 720 / interval;
        int intervalSec = interval * 60;
        int[][] diffMatrice = new int[size][2];
        Time currentTime = new Time(8,0,0);
        int current = Validator.convertTimeSeconds(currentTime);
        for (int i = 0; i < size; i++) {
            int totalArrival = 0;
            int totalLeave = 0;
            for (int j = 0; j < completedList.size(); j++) {
                if (Validator.convertTimeSeconds(completedList.get(j).getArrivalTime()) >= current && Validator.convertTimeSeconds(completedList.get(j).getArrivalTime()) < current + intervalSec) {
                    totalArrival++;
                }
                if (Validator.convertTimeSeconds(completedList.get(j).getLeaveTime()) >= current && Validator.convertTimeSeconds(completedList.get(j).getLeaveTime()) < current + intervalSec) {
                    totalLeave++;
                }
                diffMatrice[i][0] = totalArrival;
                diffMatrice[i][1] = totalLeave;
            }
            current += intervalSec;
        }
        return diffMatrice;
    }

    /**
     * method to get the array with the difference between the number of arrivals and leavings, for each interval
     *
     * @param interval
     * @param date
     *
     * @return diffArray
     */
    public int[] getDiffArray(int interval, Date date) {
        int[][] diffMatrice = getMatriceArrivalLeave(interval, date);
        int size = 720 / interval;
        int[] diffArray = new int[size];
        for (int i = 0; i < size; i++) {
            diffArray[i] = diffMatrice[i][0] - diffMatrice[i][1];
        }
        return diffArray;
    }

    /**
     * method get the max sublist of the diffArray
     * this method use one of the following algorithms: Bench Mark Algorithm (provides by the teachers) and Highest Sublist Algorithm (created by the team)
     *
     * @param diffArray
     *
     * @return maxSublist
     */
    public int[] getMaxSublist(int[] diffArray) {
        HighestSublist maxSublist = new HighestSublist();
        String algorithm = GetConfigProperties.getConfigProperties(Constants.HIGHEST_SUBLIST_ALGORITHM);
        if (algorithm.equalsIgnoreCase(Constants.BENCH_MARK_ALGORITHM)) {
            long time1 = System.nanoTime();
            int[] sublist = maxSublist.getSublistBenchMarkAlgorithm(diffArray);
            long time2 = System.nanoTime();
            System.out.println(time2 - time1);
            return sublist;
        } else if (algorithm.equalsIgnoreCase(Constants.BRUTE_FORCE_ALGORITHM)) {
            long time1 = System.nanoTime();
            int[] sublist = maxSublist.getHighestSublist(diffArray);
            long time2 = System.nanoTime();
            System.out.println(time2 - time1);
            return sublist;
        }
        return null;
    }

    /**
     * get the sum of all the elements of the max sublist
     *
     * @param maxSubList
     *
     * @return sum
     */
    public int getMaxSumSublist(int[] maxSubList) {
        int maxSum = 0;
        for (Integer i : maxSubList) {
            maxSum += i;
        }
        return maxSum;
    }

    /**
     * get the index of the min hour and max hour, of the max sublist
     *
     * @param maxSubList
     * @param diffArray
     *
     * @return minMaxHourIndexArray
     */
    private int[] getMinMaxHourIndex(int[] maxSubList, int[] diffArray) {
        int[] minMaxHours = new int[2];

        int x = 0, count = 0;
        int i;

        for (i = 0; count != maxSubList.length; i++) {
            if (maxSubList[x] == diffArray[i]) {
                count++;
            } else {
                count = 0;
            }
            x++;
        }

        minMaxHours[0] = i - maxSubList.length;
        minMaxHours[1] = i;

        return minMaxHours;
    }

    /**
     * method to get an array with to times: min hour and max hour of the max sublist
     *
     * @param maxSubList
     * @param diffArray
     * @param interval
     *
     * @return minMaxHours
     */
    public Time[] getMinMaxHour(int[] maxSubList, int[] diffArray, int interval) {
        int size = 720 / interval;
        int intervalSec = interval * 60;
        Time[] timeInterval = new Time[size];
        Time opHour = new Time(8,0,0);
        for (int i = 0; i < size; i++) {
            timeInterval[i] = opHour;
            int opHourAux = Validator.convertTimeSeconds(opHour);
            opHourAux += intervalSec;
            opHour = Validator.convertSecondsToTime(opHourAux);
        }

        int[] minMaxHoursIndex = getMinMaxHourIndex(maxSubList, diffArray);
        Time[] minMaxHours = {timeInterval[minMaxHoursIndex[0]], timeInterval[minMaxHoursIndex[1]]};
        return minMaxHours;
    }

}
