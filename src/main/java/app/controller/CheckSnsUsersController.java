package app.controller;

import app.domain.model.Company;
import pt.isep.lei.esoft.auth.AuthFacade;

public class CheckSnsUsersController {
    /**
     * company
     */
    private Company company;

    /**
     * authFacade
     */
    private AuthFacade authFacade;

    /**
     * constructor with parameters
     *
     * @param designation company designation
     * @param authFacade  used AuthFacade
     */
    public CheckSnsUsersController(String designation, AuthFacade authFacade) {
        this.company = new Company(designation);
        this.authFacade = authFacade;
    }

    /**
     * constructor with parameters
     *
     * @param authFacade
     */
    public CheckSnsUsersController(AuthFacade authFacade) {
        this.authFacade = authFacade;
    }

    /**
     * constructor with parameters
     *
     * @param company
     * @param authFacade
     */
    public CheckSnsUsersController(Company company, AuthFacade authFacade) {
        this.company = company;
        this.authFacade = authFacade;
    }

    // public ArrayList<Client> getSnsUsersWaitingList() {
    //      return Company.getSnsUsersWaitingList();
    // }
}
