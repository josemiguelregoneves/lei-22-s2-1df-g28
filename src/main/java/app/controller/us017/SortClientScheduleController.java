package app.controller.us017;

import app.domain.conceptual.VaccinationCenter;
import app.domain.conceptual.VaccineSchedule;
import app.domain.interfaces.BubbleSort;
import app.domain.interfaces.SelectionSort;
import app.domain.model.ReadClientSchedulesFromFile;
import app.domain.shared.Constants;
import app.domain.us017.SortArrivalTime;
import app.domain.us017.SortLeavingTime;

import java.io.File;
import java.io.FileNotFoundException;

public class SortClientScheduleController {

    /**
     * method to read schedules from file
     *
     * @param center
     * @param file
     *
     * @throws FileNotFoundException
     */
    public void readSchedulesFromFile(VaccinationCenter center, File file) throws FileNotFoundException {
        ReadClientSchedulesFromFile.readClientsFromFile(center, file);
    }

    /**
     * method to sort the array of schedules
     *
     * @param algorithm
     * @param parameter
     * @param order
     * @param arrCompleted
     *
     * @throws FileNotFoundException
     */
    public void sortArrayListByArrivalLeave(String algorithm, String parameter, String order, VaccineSchedule[] arrCompleted) throws FileNotFoundException {
        if (algorithm != null && order != null && parameter != null) {
            switch (algorithm) {
                case Constants.STR_BUBBLE:
                    BubbleSort[] arrBubble = {new SortArrivalTime(), new SortLeavingTime()};
                    switch (parameter) {
                        case Constants.STR_ARRIVAL:
                            if (order.equals(Constants.STR_ASCENDING)) {
                                arrBubble[0].ascendingBubbleSort(arrCompleted);
                            } else if (order.equals(Constants.STR_DESCENDING)) {
                                arrBubble[0].descendingBubbleSort(arrCompleted);
                            }
                            break;
                        case Constants.STR_LEAVING:
                            if (order.equals(Constants.STR_ASCENDING)) {
                                arrBubble[1].ascendingBubbleSort(arrCompleted);
                            } else if (order.equals(Constants.STR_DESCENDING)) {
                                arrBubble[1].descendingBubbleSort(arrCompleted);
                            }
                            break;
                    }
                    break;
                case Constants.STR_SELECTION:
                    SelectionSort[] arrSelection = {new SortArrivalTime(), new SortLeavingTime()};
                    switch (parameter) {
                        case Constants.STR_ARRIVAL:
                            if (order.equals(Constants.STR_ASCENDING)) {
                                arrSelection[0].ascendingSelectionSort(arrCompleted);
                            } else if (order.equals(Constants.STR_DESCENDING)) {
                                arrSelection[0].descendingSelectionSort(arrCompleted);
                            }
                            break;
                        case Constants.STR_LEAVING:
                            if (order.equals(Constants.STR_ASCENDING)) {
                                arrSelection[1].ascendingSelectionSort(arrCompleted);
                            } else if (order.equals(Constants.STR_DESCENDING)) {
                                arrSelection[1].descendingSelectionSort(arrCompleted);
                            }
                            break;
                    }
                    break;
            }
        }
    }

    /**
     * method to show the sorted list
     *
     * @param arrCompleted
     */
    public void showSortedList(VaccineSchedule[] arrCompleted) {
        for (VaccineSchedule schedule : arrCompleted) {
            System.out.println(schedule);
        }
    }
}
