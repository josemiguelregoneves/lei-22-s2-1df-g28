package app.controller.us017;

import app.domain.conceptual.VaccinationCenter;
import app.domain.conceptual.VaccineSchedule;
import app.domain.shared.Constants;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class SortClientScheduleGUI implements Initializable {

    /**
     * box of available algorithms (bubble & selection)
     */
    @FXML
    private ChoiceBox<String> boxAlgorithm;

    /**
     * box of available parameters (arrival & leaving)
     */
    @FXML
    private ChoiceBox<String> boxParameter;

    /**
     * box of available parameters (ascending & dscending)
     */
    @FXML
    private ChoiceBox<String> boxOrder;

    /**
     * button to reset the data
     */
    @FXML
    private Button btnReset;

    /**
     * button to sort the array
     */
    @FXML
    private Button btnSort;

    /**
     * list view to show the sorted list
     */
    @FXML
    private ListView<VaccineSchedule> listViewStatus;

    /**
     * algorithm
     */
    private String algorithm;

    /**
     * order
     */
    private String order;

    /**
     * parameter
     */
    private String parameter;

    /**
     * completed schedules
     */
    private ArrayList<VaccineSchedule> completedSchedules;

    /**
     * center
     */
    private static VaccinationCenter center;

    /**
     * controller to sort the array
     */
    private static final SortClientScheduleController ctrl = new SortClientScheduleController();

    /**
     * method to get the vaccination center
     *
     * @return center
     */
    public static VaccinationCenter getCenter() {
        return center;
    }

    /**
     * method to set the vaccination center
     *
     * @param center
     */
    public static void setCenter(VaccinationCenter center) {
        SortClientScheduleGUI.center = center;
    }

    /**
     * initializable method
     *
     * @param url
     * @param resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        completedSchedules = center.getCompletedSchedulesList();
        boxAlgorithm.getItems().addAll(Constants.STR_BUBBLE, Constants.STR_SELECTION);
        boxOrder.getItems().addAll(Constants.STR_ASCENDING, Constants.STR_DESCENDING);
        boxParameter.getItems().addAll(Constants.STR_ARRIVAL, Constants.STR_LEAVING);
    }

    /**
     * method to reset the data
     *
     * @param actionEvent
     */
    public void resetData(ActionEvent actionEvent) {
        listViewStatus.getItems().clear();
    }

    /**
     * method to sort the list
     *
     * @param actionEvent
     *
     * @throws FileNotFoundException
     */
    public void sortList(ActionEvent actionEvent) throws FileNotFoundException {
        /**
         * algorithm value
         */
        algorithm = boxAlgorithm.getValue();

        /**
         * order value
         */
        order = boxOrder.getValue();

        /**
         * parameter value
         */
        parameter = boxParameter.getValue();

        if (algorithm != null && order != null && parameter != null && !completedSchedules.isEmpty()) {
            /**
             * getting the array
             */
            VaccineSchedule[] arrCompleted = completedSchedules.toArray(new VaccineSchedule[0]);

            /**
             * sorting the array
             */
            ctrl.sortArrayListByArrivalLeave(algorithm, parameter, order, arrCompleted);

            /**
             * printing the array
             */
            listViewStatus.getItems().addAll(arrCompleted);
        } else {
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setTitle("Cannot Sort!");
            a.setContentText("Invalid Algorithm/Order/Parameter!");
            a.show();
        }
    }
}
