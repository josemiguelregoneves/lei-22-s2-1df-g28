package app.controller.list;

import java.util.ArrayList;

import app.domain.conceptual.VaccineSchedule;
import app.domain.model.Company;
import app.domain.conceptual.VaccinationCenter;

public class ShowCentersListController {
    public ArrayList<VaccinationCenter> getCenterList() {
        return Company.getCenterStore().getCentersList();
    }

    public ArrayList<VaccinationCenter> getCenterListWithType(String type) {
        ArrayList<VaccinationCenter> centerListByType = new ArrayList<>();
        for (VaccinationCenter center : Company.getCenterStore().getCentersList()) {
            if (center.getIdType().equals(type)) {
                centerListByType.add(center);
            }
        }
        return centerListByType;
    }


    /**
     * method to print the data of each center of the list
     *
     * @param centerList
     */
    public void showCenterList(ArrayList<VaccinationCenter> centerList) {
        for (VaccinationCenter vc : centerList) {
            System.out.println("ID Type: " + vc.getIdType());
            System.out.println("Name: " + vc.getName());
            System.out.println("Address" + vc.getAddress());
            System.out.println("Phone Number: " + vc.getPhoneNumber());
            System.out.println("Fax Number: " + vc.getFaxNumber());
            System.out.println("Email: " + vc.getEmail());
            System.out.println("Web Site: " + vc.getWebsite());
            System.out.println("Slot Duration: " + vc.getSlotDuration());
            System.out.println("Maximum of Vaccines per Slot: " + vc.getMaxVaccines());
            System.out.println("Opening Hour: " + vc.getOpeningHour());
            System.out.println("Closing Hour: " + vc.getClosingHour());

            System.out.println("SCHEDULES");
            for (VaccineSchedule schedule : vc.getSchedulesList()) {
                System.out.println(schedule);
            }

            System.out.println("WAITING ROOM");
            for (VaccineSchedule schedule : vc.getSchedulesWaitingRoom()) {
                System.out.println(schedule);
            }

            System.out.println("COMPLETED SCHEDULES");
            for (VaccineSchedule schedule : vc.getCompletedSchedulesList()) {
                System.out.println(schedule);
            }

            System.out.println();
        }
    }
}