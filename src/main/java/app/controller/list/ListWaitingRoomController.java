package app.controller.list;

import app.domain.conceptual.Client;
import app.domain.conceptual.VaccinationCenter;
import app.domain.dto.ClientDTO;
import app.domain.dto.VaccinationCenterDTO;
import app.domain.dto.VaccineScheduleDTO;
import app.domain.mapper.ClientMapper;
import app.domain.model.Company;
import app.domain.conceptual.VaccineSchedule;
import app.domain.shared.Alert;
import app.domain.shared.Validator;

import java.util.ArrayList;

public class ListWaitingRoomController {

    public ArrayList<VaccinationCenterDTO> getCentersList() {
        ArrayList<VaccinationCenter> centerList = Company.getCenterStore().getCentersList();
        return Company.getListCenterMapper().toDto(centerList);
    }

    public ArrayList<VaccineScheduleDTO> getSchedulesWaiting(VaccinationCenter center) {
        return Company.getListSchedulesMapper().toDto(center.getSchedulesWaitingRoom());
    }

    public ArrayList<ClientDTO> getWaitingRoom(ArrayList<VaccineSchedule> schedulesList) {
        ClientMapper clientMap = Company.getSnsUserMapper();
        ArrayList<ClientDTO> waitingRoomDto = new ArrayList<>();
        for (VaccineSchedule schedule : schedulesList) {
            if (!waitingRoomDto.contains(Validator.getClientBySnsNumber(schedule.getSnsNumber()))) {
                waitingRoomDto.add(clientMap.toDto(Validator.getClientBySnsNumber(schedule.getSnsNumber())));
            }
        }
        return waitingRoomDto;
    }
}