package app.controller.list;

import java.util.ArrayList;

import app.domain.model.Company;
import app.domain.conceptual.Employee;

public class ShowEmployeesListController {
    public ArrayList<Employee> getEmployeeList() {
        return Company.geteStore().getEmployeeList();
    }

    public ArrayList<Employee> getEmployeeListWithRole(String idRole) {
        ArrayList<Employee> eListByRole = new ArrayList<>();
        for (Employee e : Company.geteStore().getEmployeeList()) {
            if (e.getIdRole().equals(idRole)) {
                eListByRole.add(e);
            }
        }
        return eListByRole;
    }

    /**
     * method to print the data of each employee of the list
     *
     * @param employeeList
     */
    public void showEmployeeList(ArrayList<Employee> employeeList) {
        for (Employee e : employeeList) {
            System.out.println("ID: " + e.getId());
            System.out.println("ID Role: " + e.getIdRole());
            System.out.println("Name: " + e.getName());
            System.out.println("Email: " + e.getEmail());
            System.out.println("Address: " + e.getAddress());
            System.out.println("Citizen Card Number: " + e.getCcNumber());
            System.out.println("Phone Number: " + e.getPhoneNumber());
            System.out.println("Password: " + e.getPassword());
            System.out.println("Vaccination Center: " + e.getCenter().getName());
            System.out.println();
        }
    }
}
