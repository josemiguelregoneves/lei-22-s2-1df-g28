package app.controller.list;

import java.util.ArrayList;

import app.domain.conceptual.Vaccine;
import app.domain.conceptual.VaccineType;
import app.domain.model.*;

public class ShowVaccinesListController {
    public ArrayList<Vaccine> getVaccineList() {
        return Company.getVcStore().getVaccineList();
    }

    public ArrayList<Vaccine> getVaccineListByVcType(VaccineType vcType) {
        ArrayList<Vaccine> vcListByType = new ArrayList<>();
        for (Vaccine vc : Company.getVcStore().getVaccineList()) {
            if (vc.getType().equals(vcType)) {
                vcListByType.add(vc);
            }
        }
        return vcListByType;
    }

    public void showVaccinesList(ArrayList<Vaccine> vcList) {
        for (Vaccine vc : vcList) {
            System.out.println("Vaccine Type Name: " + vc.getType().getName());
            System.out.println("Min Age: " + vc.getAdministration().getRange().getMinAge());
            System.out.println("Max Age: " + vc.getAdministration().getRange().getMaxAge());
            System.out.println("Dosage (ml): " + vc.getAdministration().getDosage());
            System.out.println("Doses: " + vc.getAdministration().getDoses());
            System.out.println("Vaccine Name: " + vc.getName());
            System.out.println();
        }
    }
}
