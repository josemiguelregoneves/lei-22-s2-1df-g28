package app.controller.us008;

import app.domain.conceptual.Client;
import app.domain.conceptual.VaccinationCenter;
import app.domain.conceptual.Vaccine;
import app.domain.conceptual.VaccineSchedule;
import app.domain.dto.ClientDTO;
import app.domain.dto.VaccineDTO;
import app.domain.dto.VaccineScheduleDTO;
import app.domain.model.Company;
import app.domain.shared.Constants;
import app.domain.shared.Validator;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;

import java.io.FileNotFoundException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;

public class NurseAdminVaccineGUI implements Initializable {

    /**
     * choice box of clients
     */
    @FXML
    private ChoiceBox<Client> boxClients;

    /**
     * choice box of schedules
     */
    @FXML
    private ChoiceBox<VaccineSchedule> boxSchedules;

    /**
     * choice box of vaccines
     */
    @FXML
    private ChoiceBox<Vaccine> boxVaccines;

    /**
     * button to get the schedules
     */
    @FXML
    private Button btnSchedules;

    /**
     * button to get the vaccines
     */
    @FXML
    private Button btnVaccines;

    /**
     * button to record the administration data
     */
    @FXML
    private Button btnRecord;

    /**
     * button to send an sms
     */
    @FXML
    private Button btnSms;

    /**
     * button to repeat the process
     */
    @FXML
    private Button btnAgain;

    /**
     * client
     */
    private Client client;

    /**
     * schedule
     */
    private VaccineSchedule schedule;

    /**
     * vaccine
     */
    private Vaccine vaccine;

    /**
     * controller for nurse to record the administration of a vaccine
     */
    private static NurseAdminVaccineController ctrl;

    /**
     * vaccination center
     */
    private static VaccinationCenter center;

    /**
     * method to get the vaccination center value
     *
     * @return center
     */
    public static VaccinationCenter getCenter() {
        return center;
    }

    /**
     * method to set the vaccination center value
     *
     * @param center
     */
    public static void setCenter(VaccinationCenter center) {
        NurseAdminVaccineGUI.center = center;
    }

    /**
     * method to get the available schedules
     *
     * @param actionEvent
     */
    public void getSchedules(ActionEvent actionEvent) {
        /**
         * get the value of the clients box
         */
        client = boxClients.getValue();

        /**
         * get the value of the schedules box
         */
        schedule = boxSchedules.getValue();

        /**
         * get the value of the vaccines box
         */
        vaccine = boxVaccines.getValue();

        if (client != null) {
            /**
             * get the list of available schedules
             */
            ArrayList<VaccineScheduleDTO> schedulesListDto = ctrl.getSchedulesOfClient(client);
            ArrayList<VaccineSchedule> schedulesList = Company.getListSchedulesMapper().toModel(schedulesListDto);

            /**
             * clear the choice box
             */
            boxSchedules.getItems().clear();

            /**
             * fill the schedules choice box with that list
             */
            boxSchedules.getItems().addAll(schedulesList);
        } else {
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setTitle("Invalid SNS User/Schedule");
            a.setContentText("Invalid SNS User");
            a.show();
        }
    }

    /**
     * method to get the available vaccines
     *
     * @param actionEvent
     */
    public void getVaccines(ActionEvent actionEvent) {
        /**
         * get the value of the clients box
         */
        client = boxClients.getValue();

        /**
         * get the value of the schedules box
         */
        schedule = boxSchedules.getValue();

        /**
         * get the value of the vaccines box
         */
        vaccine = boxVaccines.getValue();

        if (client != null && schedule != null) {
            /**
             * get the list of available vaccines
             */
            ArrayList<VaccineDTO> vaccinesListDto = ctrl.getAvailableVaccinesForSchedule(schedule);
            ArrayList<Vaccine> vaccinesList = Company.getListVaccineMapper().toModel(vaccinesListDto);

            /**
             * clear the vaccines box
             */
            boxVaccines.getItems().clear();

            /**
             * fill the vaccines choice box with that list
             */
            boxVaccines.getItems().addAll(vaccinesList);
        } else {
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setTitle("Invalid SNS User/Schedule");
            a.setContentText("Invalid SNS User/Schedule");
            a.show();
        }
    }

    /**
     * method to record the vaccine administration data
     *
     * @param actionEvent
     */
    public void recordVaccineAdministration(ActionEvent actionEvent) {
        /**
         * get the value of the clients box
         */
        client = boxClients.getValue();

        /**
         * get the value of the schedules box
         */
        schedule = boxSchedules.getValue();

        /**
         * get the value of the vaccines box
         */
        vaccine = boxVaccines.getValue();

        Alert a;
        if (client != null && schedule != null && vaccine != null) {
            if (Validator.validateNumberOfDoses(schedule) && Validator.validateTimeSinceLastVaccine(schedule)) {
                /**
                 * record the vaccine administration data
                 */
                ctrl.recordVaccineAdmin(schedule, center, vaccine);

                /**
                 * send an sms when the recovery time ends
                 */
                ctrl.sendLeavingSms(schedule);

                a = new Alert(Alert.AlertType.CONFIRMATION);
                a.setTitle("Vaccine Administration Complete!");
                a.setContentText(client.getName() + "\n" + client.getSnsNumber() + "\n");
                a.show();
            } else {
                a = new Alert(Alert.AlertType.ERROR);
                a.setTitle("Cannot do this Vaccine Administration");
                a.setContentText("Cannot Receive more doses/Invalid Time since last Vaccine!");
                a.show();
            }

        } else {
            a = new Alert(Alert.AlertType.ERROR);
            a.setTitle("Cannot do this Vaccine Administration");
            a.setContentText("Invalid SNS User/Schedule/Vaccine");
            a.show();
        }
    }

    /**
     * method to send an sms when the administration process and record data ends
     *
     * @param actionEvent
     *
     * @throws FileNotFoundException
     */
    public void sendSms(ActionEvent actionEvent) throws FileNotFoundException {
        /**
         * get the value of the clients box
         */
        client = boxClients.getValue();

        /**
         * get the value of the schedules box
         */
        schedule = boxSchedules.getValue();

        /**
         * get the value of the vaccines box
         */
        vaccine = boxVaccines.getValue();

        Alert a;
        if (client != null && schedule != null && vaccine != null) {
            /**
             * send an sms when the administration is complete
             */
            ctrl.sendAdminSms(schedule);

            a = new Alert(Alert.AlertType.CONFIRMATION);
            a.setTitle("Send SMS Complete!");
            a.setContentText(client.getName() + "\n" + client.getSnsNumber() + "\n");
            a.show();
        } else {
            a = new Alert(Alert.AlertType.ERROR);
            a.setTitle("Cannot Send SMS");
            a.setContentText("Invalid SNS User/Schedule/Vaccine");
            a.show();
        }
    }

    /**
     * method to repeat the record administration process
     *
     * @param actionEvent
     */
    public void repeatProcess(ActionEvent actionEvent) {
        /**
         * clear the choice box
         */
        boxClients.getItems().clear();

        /**
         * clear the choice box
         */
        boxSchedules.getItems().clear();

        /**
         * clear the choice box
         */
        boxVaccines.getItems().clear();

        /**
         * get the list of available clients
         */
        ArrayList<ClientDTO> clientsListDto = ctrl.getClientsList(center);
        ArrayList<Client> clientsList = Company.getListClientMapper().toModel(clientsListDto);

        /**
         * fill the clients choice box with that list
         */
        boxClients.getItems().addAll(clientsList);
    }

    /**
     * initializable method
     *
     * @param url
     * @param resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        ctrl = new NurseAdminVaccineController();

        /**
         * get the list of available clients
         */
        ArrayList<ClientDTO> clientsListDto = ctrl.getClientsList(center);
        ArrayList<Client> clientsList = Company.getListClientMapper().toModel(clientsListDto);

        /**
         * fill the clients choice box with that list
         */
        boxClients.getItems().addAll(clientsList);

        /**
         * get the value of the clients box
         */
        client = boxClients.getValue();

        /**
         * get the value of the schedules box
         */
        schedule = boxSchedules.getValue();

        /**
         * get the value of the vaccines box
         */
        vaccine = boxVaccines.getValue();
    }

}


