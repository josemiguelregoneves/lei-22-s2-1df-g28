package app.controller.us008;

import app.domain.conceptual.Client;
import app.domain.conceptual.VaccinationCenter;
import app.domain.conceptual.Vaccine;
import app.domain.conceptual.VaccineSchedule;
import app.domain.dto.ClientDTO;
import app.domain.dto.VaccineDTO;
import app.domain.dto.VaccineScheduleDTO;
import app.domain.mapper.*;
import app.domain.model.Company;
import app.domain.shared.Alert;
import app.domain.shared.Constants;
import app.domain.shared.SMS;
import app.domain.shared.Validator;

import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.*;

public class NurseAdminVaccineController {

    /**
     * method to get the list of available clienst no center waiting room
     *
     * @param center
     *
     * @return clientsListDto
     */
    public ArrayList<ClientDTO> getClientsList(VaccinationCenter center) {
        ArrayList<Client> clientList = center.getClientsWaitingRoom();
        return Company.getListClientMapper().toDto(clientList);
    }

    /**
     * method to get the selected client
     *
     * @param clients
     * @param clientOption
     *
     * @return clientDto
     */
    public ClientDTO selectClient(ArrayList<Client> clients, int clientOption) {
        ClientMapper cMap = Company.getSnsUserMapper();
        return cMap.toDto(clients.get(clientOption));
    }


    /**
     * method to get the list of available schedules of the selected client
     *
     * @param snsUser
     *
     * @return schedulesListDto
     */
    public ArrayList<VaccineScheduleDTO> getSchedulesOfClient(Client snsUser) {
        ListSchedulesMapper listSchedulesMapper = Company.getListSchedulesMapper();
        return listSchedulesMapper.toDto(snsUser.getTodaySchedules());
    }

    /**
     * method to get the selected schedule
     *
     * @param schedules
     * @param scheduleOption
     *
     * @return scheduleDto
     */
    public VaccineScheduleDTO selectSchedule(ArrayList<VaccineSchedule> schedules, int scheduleOption) {
        VaccineScheduleMapper scheduleMap = Company.getVcScheduleMapper();
        return scheduleMap.toDto(schedules.get(scheduleOption));
    }

    /**
     * method to get the available vaccines of the selected schedule
     *
     * @param schedule
     *
     * @return schedulesListDto
     */
    public ArrayList<VaccineDTO> getAvailableVaccinesForSchedule(VaccineSchedule schedule) {
        ListVaccinesMapper listVcMap = Company.getListVaccineMapper();
        return listVcMap.toDto(Company.getVcStore().getAvailableVaccinesForSchedule(schedule));
    }

    /**
     * method to get the selected vaccine
     *
     * @param vaccines
     * @param vaccineOption
     *
     * @return vaccineDto
     */
    public VaccineDTO selectVaccine(ArrayList<Vaccine> vaccines, int vaccineOption) {
        VaccineMapper vcMap = Company.getVcMapper();
        return vcMap.toDto(vaccines.get(vaccineOption));
    }

    /**
     * method to send an sms after vaccine admin
     *
     * @param schedule
     *
     * @throws FileNotFoundException
     */
    public void sendAdminSms(VaccineSchedule schedule) throws FileNotFoundException {
        SMS.sendSmsVcComplete(schedule);
    }

    /**
     * method to send an sms when the recovery period ends
     *
     * @param schedule
     */
    public void sendLeavingSms(VaccineSchedule schedule) {
        Timer timer = new Timer();

        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(schedule.getAdminTime());

        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
        sdf.format(gc.getTime());

        gc.add(Calendar.MINUTE,30);

        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                schedule.setLeaveTime(Validator.getHourOfDate(gc));
                try {
                    SMS.sendSmsRecoveryComplete(schedule);
                } catch (FileNotFoundException | InterruptedException e) {
                    throw new RuntimeException(e);
                }
                timer.cancel();
            }
        };

        timer.schedule(task, gc.getTime());
    }

    /**
     * method to record the data of the administration vaccine process
     *
     * mudanca de estados de um objeto (esoft)
     *
     * @param schedule
     * @param center
     * @param vc
     */
    public void recordVaccineAdmin(VaccineSchedule schedule, VaccinationCenter center, Vaccine vc) {

        Client client = Validator.getClientBySnsNumber(schedule.getSnsNumber());

        schedule.setAdminTime(Validator.getCurrentHour());
        schedule.setVc(vc.getName());
        schedule.setLotNumber(Constants.VACCINE_LOT);
        client.getCompletedList().add(schedule);
        center.getCompletedSchedulesList().add(schedule);
        Company.getScheduleStore().getCompletedSchedulesList().add(schedule);
        int doses = Validator.getNumberOfDoses(schedule);
        schedule.setDose(Validator.convertIntDoseToString(doses));
    }
}
