package app.controller;

import app.domain.conceptual.Client;
import app.domain.conceptual.VaccineSchedule;
import app.domain.model.Company;
import app.domain.conceptual.Employee;
import app.domain.conceptual.VaccinationCenter;
import app.domain.shared.Alert;
import app.domain.shared.Validator;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class ReceptionistChecksArrivalController {


    public ArrayList<VaccineSchedule> getCenterClients(VaccinationCenter center) {
        return center.getSchedulesList();
    }

    public VaccineSchedule getScheduleBySnsNumber(int snsNumber) {
        VaccineSchedule vcSchedule = null;
        for (VaccineSchedule schedule : Company.getScheduleStore().getVcScheduleList()) {
            if (schedule.getSnsNumber() == snsNumber) {
                vcSchedule = schedule;
            }
        }
        return vcSchedule;
    }

    public boolean addScheduleToWaitingRoom(int snsNumber, VaccinationCenter center) {
        Client snsUser = Validator.getClientBySnsNumber(snsNumber);
        for (VaccineSchedule schedule : getCenterClients(center)) {
            if (schedule.getSnsNumber() == snsNumber) {
                int currentDate = Calendar.getInstance().getTime().getDate();
                Date date = schedule.getScheduleDate();
                if (date.getDate() == currentDate) {
                    center.getSchedulesWaitingRoom().add(schedule);
                    return true;
                }
            }
        }
        return false;
    }
}
