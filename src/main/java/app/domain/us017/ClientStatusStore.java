package app.domain.us017;

import app.domain.model.Company;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class ClientStatusStore {

    /**
     * list of all client status
     */
    private ArrayList<ClientStatus> clientStatusList;

    /**
     * constructor
     */
    public ClientStatusStore() {
        this.clientStatusList = new ArrayList<>();
    }

    /**
     * method to get the client status list
     *
     * @return clientStatusList
     */
    public ArrayList<ClientStatus> getClientStatusList() {
        return clientStatusList;
    }


    /**
     * method to set the client status list
     *
     * @param clientStatusList
     */
    public void setClientStatusList(ArrayList<ClientStatus> clientStatusList) {
        this.clientStatusList = clientStatusList;
    }

    /**
     * method to create a client status
     *
     * @param snsNumber
     * @param vcName
     * @param dose
     * @param lot
     * @param date
     * @param scheduleTime
     * @param arrivalTime
     * @param adminTime
     * @param leaveTime
     * @return clientStatus
     */
    public ClientStatus createClientStatus(int snsNumber, String vcName, String dose, String lot, Date date, Time scheduleTime, Time arrivalTime, Time adminTime, Time leaveTime) {
        return new ClientStatus(snsNumber, vcName, dose, lot, date, scheduleTime, arrivalTime, adminTime, leaveTime);
    }

    /**
     * method to save a clientStatus on the list
     *
     * @param clientStatus
     */
    public void saveClientStatus(ClientStatus clientStatus) {
        this.clientStatusList.add(clientStatus);
    }

    public void readClientsFromFile(File file) throws FileNotFoundException {
        ClientStatusStore clientStatusStore = Company.getStatusStore();
        Scanner read = new Scanner(file);
        read.nextLine();
        while (read.hasNextLine()) {
            String[] arr = read.nextLine().split(";");

            int snsNumber = Integer.parseInt(arr[0]);
            String vcName = arr[1];
            String dose = arr[2];
            String lot = arr[3];

            String[] arrSchedule = arr[4].split(" ");
            String[] arrDate = arrSchedule[0].split("/");
            int month = Integer.parseInt(arrDate[0]);
            int day = Integer.parseInt(arrDate[1]);
            int year = Integer.parseInt(arrDate[2]);
            Date date = new Date(year, month, day);
            String[] arrTimeSchedule = arrSchedule[1].split(":");
            int hourSchedule = Integer.parseInt(arrTimeSchedule[0]);
            int minuteSchedule = Integer.parseInt(arrTimeSchedule[1]);
            int secondSchedule = 0;
            String strTimeSchedule = String.format("%d:%d:%d", hourSchedule, minuteSchedule, secondSchedule);
            Time scheduleTime = Time.valueOf(strTimeSchedule);

            String[] arrArrival = arr[5].split(" ");
            String[] arrTimeArrival = arrArrival[1].split(":");
            int hourArrival = Integer.parseInt(arrTimeArrival[0]);
            int minuteArrival = Integer.parseInt(arrTimeArrival[1]);
            int secondArrival = 0;
            String strTimeArrival = String.format("%d:%d:%d", hourArrival, minuteArrival, secondArrival);
            Time arrivalTime = Time.valueOf(strTimeArrival);


            String[] arrAdmin = arr[6].split(" ");
            String[] arrTimeAdmin = arrAdmin[1].split(":");
            int hourAdmin = Integer.parseInt(arrTimeAdmin[0]);
            int minuteAdmin = Integer.parseInt(arrTimeAdmin[1]);
            int secondAdmin = 0;
            String strTimeAdmin = String.format("%d:%d:%d", hourAdmin, minuteAdmin, secondAdmin);
            Time adminTime = Time.valueOf(strTimeAdmin);

            String[] arrLeave = arr[7].split(" ");
            String[] arrTimeLeave = arrLeave[1].split(":");
            int hourLeave = Integer.parseInt(arrTimeLeave[0]);
            int minuteLeave = Integer.parseInt(arrTimeLeave[1]);
            int secondLeave = 0;
            String strTimeLeave = String.format("%d:%d:%d", hourLeave, minuteLeave, secondLeave);
            Time leaveTime = Time.valueOf(strTimeLeave);

            ClientStatus clientStatus = clientStatusStore.createClientStatus(snsNumber, vcName, dose, lot, date, scheduleTime, arrivalTime, adminTime, leaveTime);
            clientStatusStore.saveClientStatus(clientStatus);
        }
    }
}

