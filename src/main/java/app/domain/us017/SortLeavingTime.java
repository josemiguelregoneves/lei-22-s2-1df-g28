package app.domain.us017;

import app.domain.conceptual.VaccineSchedule;
import app.domain.interfaces.BubbleSort;
import app.domain.interfaces.SelectionSort;

import java.io.FileNotFoundException;
import java.util.ArrayList;

public class SortLeavingTime implements BubbleSort, SelectionSort {

    /**
     * bubble sort method to sort an array by ascending order of the leaving time
     *
     * @param arrSchedules
     *
     * @throws FileNotFoundException
     */
    @Override
    public void ascendingBubbleSort(VaccineSchedule[] arrSchedules) {
        int size = arrSchedules.length;

        for (int i = 0; i < size - 1; i++) {
            for (int j = 0; j < size - i - 1; j++) {
                if (arrSchedules[j].getLeaveTime().after(arrSchedules[j + 1].getLeaveTime())) {
                    VaccineSchedule aux = arrSchedules[j];
                    arrSchedules[j] = arrSchedules[j + 1];
                    arrSchedules[j + 1] = aux;
                }
            }
        }
    }

    /**
     * bubble sort method to sort an array by descending order of the leaving time
     *
     * @param arrSchedules
     *
     */
    @Override
    public void descendingBubbleSort(VaccineSchedule[] arrSchedules) {
        int size = arrSchedules.length;

        for (int i = 0; i < size - 1; i++) {
            for (int j = 0; j < size - i - 1; j++) {
                if (arrSchedules[j].getLeaveTime().before(arrSchedules[j + 1].getLeaveTime())) {
                    VaccineSchedule aux = arrSchedules[j];
                    arrSchedules[j] = arrSchedules[j + 1];
                    arrSchedules[j + 1] = aux;
                }
            }
        }
    }

    /**
     * selection sort method to sort an array by ascending order of the leaving time
     *
     * @param arrSchedules
     *
     */
    @Override
    public void ascendingSelectionSort(VaccineSchedule[] arrSchedules) {
        int size = arrSchedules.length;

        for (int step = 0; step < size - 1; step++) {
            int min_idx = step;

            for (int i = step + 1; i < size; i++) {

                // To sort in descending order, change > to < in this line.
                // Select the minimum element in each loop.
                if (arrSchedules[i].getLeaveTime().before(arrSchedules[min_idx].getLeaveTime())) {
                    min_idx = i;
                }
            }

            // put min at the correct position
            VaccineSchedule aux = arrSchedules[step];
            arrSchedules[step] = arrSchedules[min_idx];
            arrSchedules[min_idx] = aux;
        }
    }

    /**
     * selection sort method to sort an array by descending order of the leaving time
     *
     * @param arrSchedules
     *
     */
    @Override
    public void descendingSelectionSort(VaccineSchedule[] arrSchedules) {
        int size = arrSchedules.length;

        for (int step = 0; step < size - 1; step++) {
            int min_idx = step;

            for (int i = step + 1; i < size; i++) {

                // To sort in descending order, change > to < in this line.
                // Select the minimum element in each loop.
                if (arrSchedules[i].getLeaveTime().after(arrSchedules[min_idx].getLeaveTime())) {
                    min_idx = i;
                }
            }

            // put min at the correct position
            VaccineSchedule aux = arrSchedules[step];
            arrSchedules[step] = arrSchedules[min_idx];
            arrSchedules[min_idx] = aux;
        }
    }

    /**
     * method to get the time execution of bubble sort algorithm
     *
     * @param arrSchedules
     *
     * @return nanoseconds
     */
    public long leavingBubbleSortExecutionTime(VaccineSchedule[] arrSchedules) throws FileNotFoundException {
        long time1 = System.currentTimeMillis();
        ascendingBubbleSort(arrSchedules);
        long time2 = System.currentTimeMillis();
        return time2 - time1;
    }

    /**
     * method to get the time execution of selection sort algorithm
     *
     * @param arrSchedules
     *
     * @return nanoseconds
     */
    public long leavingSelectionSortExecutionTime(VaccineSchedule[] arrSchedules) throws FileNotFoundException {
        long time1 = System.currentTimeMillis();
        ascendingSelectionSort(arrSchedules);
        long time2 = System.currentTimeMillis();
        return time2 - time1;
    }
}
