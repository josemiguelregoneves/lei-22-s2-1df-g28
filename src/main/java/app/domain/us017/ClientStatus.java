package app.domain.us017;

import java.sql.Time;
import java.util.Comparator;
import java.util.Date;

public class ClientStatus {

    /**
     * sns number
     */
    private int snsNumber;

    /**
     * vaccine name
     */
    private String vcName;

    /**
     * dose
     */
    private String dose;

    /**
     * lot number
     */
    private String lot;

    /**
     * date
     */
    private Date date;

    /**
     * schedule time
     */
    private Time scheduleTime;

    /**
     * arrival time
     */
    private Time arrivalTime;

    /**
     * admin time
     */
    private Time adminTime;

    /**
     * leaving time
     */
    private Time leaveTime;

    /**
     * constructor with parameters
     *
     * @param snsNumber
     * @param vcName
     * @param dose
     * @param lot
     * @param date
     * @param scheduleTime
     * @param arrivalTime
     * @param adminTime
     * @param leaveTime
     */
    public ClientStatus(int snsNumber, String vcName, String dose, String lot, Date date, Time scheduleTime, Time arrivalTime, Time adminTime, Time leaveTime) {
        this.snsNumber = snsNumber;
        this.vcName = vcName;
        this.dose = dose;
        this.lot = lot;
        this.date = date;
        this.scheduleTime = scheduleTime;
        this.arrivalTime = arrivalTime;
        this.adminTime = adminTime;
        this.leaveTime = leaveTime;
    }

    public int getSnsNumber() {
        return snsNumber;
    }

    public void setSnsNumber(int snsNumber) {
        this.snsNumber = snsNumber;
    }

    public String getVcName() {
        return vcName;
    }

    public void setVcName(String vcName) {
        this.vcName = vcName;
    }

    public String getDose() {
        return dose;
    }

    public void setDose(String dose) {
        this.dose = dose;
    }

    public String getLot() {
        return lot;
    }

    public void setLot(String lot) {
        this.lot = lot;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Time getScheduleTime() {
        return scheduleTime;
    }

    public void setScheduleTime(Time scheduleTime) {
        this.scheduleTime = scheduleTime;
    }

    public Time getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(Time arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public Time getAdminTime() {
        return adminTime;
    }

    public void setAdminTime(Time adminTime) {
        this.adminTime = adminTime;
    }

    public Time getLeaveTime() {
        return leaveTime;
    }

    public void setLeaveTime(Time leaveTime) {
        this.leaveTime = leaveTime;
    }

    @Override
    public String toString() {
        return "ClientStatus{" +
                "snsNumber=" + snsNumber +
                ", arrivalTime=" + arrivalTime +
                ", leaveTime=" + leaveTime +
                '}';
    }
}

