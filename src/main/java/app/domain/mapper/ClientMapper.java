package app.domain.mapper;

import app.domain.conceptual.Client;
import app.domain.conceptual.VaccineSchedule;
import app.domain.dto.ClientDTO;
import app.domain.dto.VaccineScheduleDTO;
import app.domain.model.*;

import java.util.ArrayList;
import java.util.Date;

public class ClientMapper {

    /**
     * method to convert a client DTO to model
     *
     * @param snsUser
     * @return client
     */
    public Client toModel(ClientDTO snsUser) {
        int ccNumber = snsUser.ccNumber;
        String name = snsUser.name;
        String address = snsUser.address;
        String sex = snsUser.sex;
        Date birthDate = snsUser.birthDate;
        int phoneNumber = snsUser.phoneNumber;
        String email = snsUser.email;
        int snsNumber = snsUser.snsNumber;
        String password = snsUser.password;
        ArrayList<VaccineSchedule> schedules = snsUser.schedulesList;
        ArrayList<VaccineSchedule> completed = snsUser.completed;

        Client c = Company.getClientStore().createClient(ccNumber, name, address, sex, birthDate, phoneNumber, email, snsNumber);
        c.setSchedules(schedules);
        c.setCompletedList(completed);

        return c;
    }

    /**
     * method to convert a client model to DTO
     *
     * @param snsUser
     * @return clientDto
     */
    public ClientDTO toDto(Client snsUser) {
        int ccNumber = snsUser.getCcNumber();
        String name = snsUser.getName();
        String address = snsUser.getAddress();
        String sex = snsUser.getSex();
        Date birthDate = snsUser.getBirthDate();
        int phoneNumber = snsUser.getPhoneNumber();
        String email = snsUser.getEmail();
        int snsNumber = snsUser.getSnsNumber();
        String password = snsUser.getPassword();
        ArrayList<VaccineSchedule> schedules = snsUser.getSchedules();
        ArrayList<VaccineSchedule> completed = snsUser.getCompletedList();

        ClientDTO cDto = new ClientDTO(ccNumber, name, address, sex, birthDate, phoneNumber, email, snsNumber);
        cDto.schedulesList = schedules;
        cDto.completed = completed;
        return cDto;
    }

}
