package app.domain.mapper;

import app.domain.conceptual.Client;
import app.domain.conceptual.VaccinationCenter;
import app.domain.dto.ClientDTO;
import app.domain.dto.VaccinationCenterDTO;
import app.domain.model.Company;

import java.util.ArrayList;

public class ListClientMapper {

    /**
     * method to convert a List of Clients model to a DTO List
     *
     * @param cList
     * @return cListDto
     */
    public ArrayList<ClientDTO> toDto(ArrayList<Client> cList) {
        ClientMapper snsUserMapper = Company.getSnsUserMapper();
        ArrayList<ClientDTO> cListDto = new ArrayList<>();
        for (Client snsUser : cList) {
            cListDto.add(snsUserMapper.toDto(snsUser));
        }
        return cListDto;
    }

    /**
     * method to convert a List of Clients DTO to a model List
     *
     * @param cListDto
     * @return cList
     */
    public ArrayList<Client> toModel(ArrayList<ClientDTO> cListDto) {
        ClientMapper snsUserMapper = Company.getSnsUserMapper();
        ArrayList<Client> cList = new ArrayList<>();
        for (ClientDTO snsUserDto : cListDto) {
            cList.add(snsUserMapper.toModel(snsUserDto));
        }
        return cList;
    }
}
