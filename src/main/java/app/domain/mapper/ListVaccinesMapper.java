package app.domain.mapper;

import app.domain.conceptual.Client;
import app.domain.conceptual.Vaccine;
import app.domain.dto.ClientDTO;
import app.domain.dto.VaccineDTO;
import app.domain.model.Company;

import java.util.ArrayList;

public class ListVaccinesMapper {

    /**
     * method to convert a list of vaccines in model to DTO
     *
     * @param vcList
     *
     * @return vaccinesListDto
     */
    public ArrayList<VaccineDTO> toDto(ArrayList<Vaccine> vcList) {
        VaccineMapper vcMapper = Company.getVcMapper();
        ArrayList<VaccineDTO> vcListDto = new ArrayList<>();
        for (Vaccine vc : vcList) {
            vcListDto.add(vcMapper.toDto(vc));
        }
        return vcListDto;
    }

    /**
     * method to convert a list of vaccines in DTO to model
     *
     * @param vcListDto
     *
     * @return vaccinesList
     */
    public ArrayList<Vaccine> toModel(ArrayList<VaccineDTO> vcListDto) {
        VaccineMapper vcMapper = Company.getVcMapper();
        ArrayList<Vaccine> vcList = new ArrayList<>();
        for (VaccineDTO vcDto : vcListDto) {
            vcList.add(vcMapper.toModel(vcDto));
        }
        return vcList;
    }

}
