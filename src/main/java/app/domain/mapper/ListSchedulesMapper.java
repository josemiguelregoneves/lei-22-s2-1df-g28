package app.domain.mapper;

import app.domain.conceptual.Client;
import app.domain.conceptual.VaccineSchedule;
import app.domain.dto.ClientDTO;
import app.domain.dto.VaccineScheduleDTO;
import app.domain.model.Company;

import java.util.ArrayList;

public class ListSchedulesMapper {

    /**
     * method to convert a list of schedules in model to DTO
     *
     * @param scheduleList
     *
     * @return schedulesListDto
     */
    public ArrayList<VaccineScheduleDTO> toDto(ArrayList<VaccineSchedule> scheduleList) {
        VaccineScheduleMapper scheduleMap = Company.getVcScheduleMapper();
        ArrayList<VaccineScheduleDTO> scheduleListDto = new ArrayList<>();
        for (VaccineSchedule schedule : scheduleList) {
            scheduleListDto.add(scheduleMap.toDto(schedule));
        }
        return scheduleListDto;
    }

    /**
     * method to convert a list of schedules in DTO to model
     *
     * @param scheduleListDto
     *
     * @return schedulesList
     */
    public ArrayList<VaccineSchedule> toModel(ArrayList<VaccineScheduleDTO> scheduleListDto) {
        VaccineScheduleMapper scheduleMap = Company.getVcScheduleMapper();
        ArrayList<VaccineSchedule> scheduleList = new ArrayList<>();
        for (VaccineScheduleDTO scheduleDto : scheduleListDto) {
            scheduleList.add(scheduleMap.toModel(scheduleDto));
        }
        return scheduleList;
    }
}
