package app.domain.mapper;

import app.domain.conceptual.Employee;
import app.domain.conceptual.VaccinationCenter;
import app.domain.dto.EmployeeDTO;
import app.domain.model.*;

public class EmployeeMapper {

    /**
     * method to convert an employee DTO to model
     *
     * @param employee
     * @return employee
     */
    public Employee toModel(EmployeeDTO employee) {
        int id = employee.id;
        String name = employee.name;
        String address = employee.address;
        int phoneNumber = employee.phoneNumber;
        String email = employee.email;
        int ccNumber = employee.ccNumber;
        String password = employee.password;
        String idRole = employee.idRole;
        VaccinationCenter center = employee.center;

        return Company.geteStore().createEmployee(name, address, phoneNumber, email, ccNumber, idRole, center);
    }

    /**
     * method to convert an employee model to DTO
     *
     * @param employee
     * @return employeeDto
     */
    public EmployeeDTO toDto(Employee employee) {
        int id = employee.getId();
        String name = employee.getName();
        String address = employee.getAddress();
        int phoneNumber = employee.getPhoneNumber();
        String email = employee.getEmail();
        int ccNumber = employee.getCcNumber();
        String password = employee.getPassword();
        String idRole = employee.getIdRole();
        VaccinationCenter center = employee.getCenter();

        return new EmployeeDTO(name, address, phoneNumber, email, ccNumber, idRole, center);
    }
}
