package app.domain.mapper;

import app.domain.conceptual.AdministrationProcess;
import app.domain.conceptual.Vaccine;
import app.domain.conceptual.VaccineType;
import app.domain.dto.VaccineDTO;
import app.domain.model.*;

public class VaccineMapper {

    /**
     * method to convert a vaccine in model
     *
     * @param vc
     *
     * @return vaccine
     */
    public Vaccine toModel(VaccineDTO vc) {
        VaccineType type = vc.type;
        AdministrationProcess administration = vc.administration;
        String name = vc.name;

        return Company.getVcStore().createVaccine(type, administration, name);
    }

    /**
     * method to convert a vaccine in DTO
     *
     * @param vc
     *
     * @return vaccineDTO
     */
    public VaccineDTO toDto(Vaccine vc) {
        VaccineType type = vc.getType();
        AdministrationProcess administration = vc.getAdministration();
        String name = vc.getName();

        return new VaccineDTO(type, administration, name);
    }

}
