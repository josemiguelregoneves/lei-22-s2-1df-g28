package app.domain.mapper;

import app.domain.conceptual.*;
import app.domain.dto.VaccineScheduleDTO;
import app.domain.model.Company;

import java.sql.Time;
import java.util.Date;

public class VaccineScheduleMapper {

    /**
     * method to convert a vaccine schedule DTO to a model
     *
     * @param vcScheduleDto
     * @return vcSchedule
     */
    public VaccineSchedule toModel(VaccineScheduleDTO vcScheduleDto) {
        int snsNumber = vcScheduleDto.snsNumber;
        VaccineType vcType = vcScheduleDto.vcType;
        Date scheduleDate = vcScheduleDto.scheduleDate;
        Time scheduleTime = vcScheduleDto.scheduleTime;
        Time arrivalTime = vcScheduleDto.arrivalTime;
        Time adminTime = vcScheduleDto.adminTime;
        Time leavingTime = vcScheduleDto.leaveTime;

        VaccineSchedule schedule = Company.getScheduleStore().createSchedule(snsNumber, vcType, scheduleDate, scheduleTime);
        schedule.setArrivalTime(arrivalTime);
        schedule.setAdminTime(adminTime);
        schedule.setLeaveTime(leavingTime);

        return schedule;
    }

    /**
     * method to convert a vaccine schedule model to a DTO
     *
     * @param vcSchedule
     * @return vcScheduleDto
     */
    public VaccineScheduleDTO toDto(VaccineSchedule vcSchedule) {
        int snsNumber = vcSchedule.getSnsNumber();
        VaccineType vcType = vcSchedule.getVcType();
        Date scheduleDate = vcSchedule.getScheduleDate();
        Time scheduleTime = vcSchedule.getScheduleTime();
        Time arrivalTime = vcSchedule.getArrivalTime();
        Time adminTime = vcSchedule.getAdminTime();
        Time leavingTime = vcSchedule.getLeaveTime();

        VaccineScheduleDTO scheduleDto = new VaccineScheduleDTO(snsNumber, vcType, scheduleDate, scheduleTime);
        scheduleDto.arrivalTime = arrivalTime;
        scheduleDto.adminTime = adminTime;
        scheduleDto.leaveTime = leavingTime;

        return scheduleDto;
    }
}
