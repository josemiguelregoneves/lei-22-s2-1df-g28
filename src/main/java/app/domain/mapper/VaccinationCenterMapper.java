package app.domain.mapper;

import app.domain.conceptual.VaccineSchedule;
import app.domain.dto.VaccinationCenterDTO;
import app.domain.conceptual.Client;
import app.domain.model.Company;
import app.domain.conceptual.VaccinationCenter;

import java.sql.Time;
import java.util.ArrayList;

public class VaccinationCenterMapper {

    /**
     * method to convert a center DTO to model
     *
     * @param center
     * @return center
     */
    public VaccinationCenter toModel(VaccinationCenterDTO center) {
        String name = center.name;
        String address = center.address;
        int phoneNumber = center.phoneNumber;
        String email = center.email;
        int faxNumber = center.faxNumber;
        String website = center.website;
        Time slotDuration = center.slotDuration;
        int maxVaccines = center.maxVaccines;
        Time openingHour = center.openingHour;
        Time closingHour = center.closingHour;
        String idType = center.idType;
        ArrayList<VaccineSchedule> schedulesWaitingRoom = center.schedulesWaitingRoom;
        ArrayList<VaccineSchedule> completedSchedulesList = center.completedSchedulesList;

        return Company.getCenterStore().createVaccinationCenter(name, address, phoneNumber, email, faxNumber, website, slotDuration, maxVaccines, openingHour, closingHour, idType);
    }

    /**
     * method to convert a center model to DTO
     *
     * @param center
     * @return centerDto
     */
    public VaccinationCenterDTO toDto(VaccinationCenter center) {
        String name = center.getName();
        String address = center.getAddress();
        int phoneNumber = center.getPhoneNumber();
        String email = center.getEmail();
        int faxNumber = center.getFaxNumber();
        String website = center.getWebsite();
        Time slotDuration = center.getSlotDuration();
        int maxVaccines = center.getMaxVaccines();
        Time openingHour = center.getOpeningHour();
        Time closingHour = center.getClosingHour();
        String idType = center.getIdType();
        ArrayList<VaccineSchedule> schedulesWaitingRoom = center.getSchedulesWaitingRoom();
        ArrayList<VaccineSchedule> completedSchedulesList = center.getCompletedSchedulesList();

        return new VaccinationCenterDTO(name, address, phoneNumber, email, faxNumber, website, slotDuration, maxVaccines, openingHour, closingHour, idType);
    }

}
