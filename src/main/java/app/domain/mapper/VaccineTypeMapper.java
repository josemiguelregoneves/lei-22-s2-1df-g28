package app.domain.mapper;

import app.domain.dto.VaccineTypeDTO;
import app.domain.model.Company;
import app.domain.conceptual.VaccineType;

public class VaccineTypeMapper {

    /**
     * method to convert a vaccine type to model
     *
     * @param vcType
     *
     * @return vaccineType
     */
    public VaccineType toModel(VaccineTypeDTO vcType) {
        String name = vcType.name;

        return Company.getVcTypeStore().createVaccineType(name);
    }

    /**
     * method to convert a vaccine type to DTO
     *
     * @param vcType
     *
     * @return vaccineTypeDto
     */
    public VaccineTypeDTO toDto(VaccineType vcType) {
        String name = vcType.getName();

        return new VaccineTypeDTO(name);
    }

}
