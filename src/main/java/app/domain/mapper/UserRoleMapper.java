package app.domain.mapper;

import app.domain.dto.UserRoleDTO;
import pt.isep.lei.esoft.auth.domain.model.UserRole;

public class UserRoleMapper {

    /**
     * method to convert a user rolein model to DTO
     *
     * @param role
     *
     * @return userRoleDto
     */
    public UserRoleDTO toDTO(UserRole role) {
        String id = role.getId();
        String description = role.getDescription();

        return new UserRoleDTO(id, description);
    }

}
