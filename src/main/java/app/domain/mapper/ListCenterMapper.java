package app.domain.mapper;

import app.domain.conceptual.Client;
import app.domain.conceptual.VaccinationCenter;
import app.domain.dto.ClientDTO;
import app.domain.dto.VaccinationCenterDTO;
import app.domain.model.Company;

import java.util.ArrayList;
import java.util.List;

public class ListCenterMapper {

    /**
     * method to convert a List of Vaccination Centers model to a DTO List
     *
     * @param centerList
     * @return centerListDto
     */
    public ArrayList<VaccinationCenterDTO> toDto(ArrayList<VaccinationCenter> centerList) {
        VaccinationCenterMapper centerMapper = Company.getCenterMapper();
        ArrayList<VaccinationCenterDTO> centerListDto = new ArrayList<>();
        for (VaccinationCenter center : centerList) {
            centerListDto.add(centerMapper.toDto(center));
        }
        return centerListDto;
    }

    /**
     * method to convert a List of Vaccination Centers DTOs to a model List
     *
     * @param centerListDto
     * @return centerList
     */
    public ArrayList<VaccinationCenter> toModel(ArrayList<VaccinationCenterDTO> centerListDto) {
        VaccinationCenterMapper centerMapper = Company.getCenterMapper();
        ArrayList<VaccinationCenter> centerList = new ArrayList<>();
        for (VaccinationCenterDTO centerDto : centerListDto) {
            centerList.add(centerMapper.toModel(centerDto));
        }
        return centerList;
    }

}
