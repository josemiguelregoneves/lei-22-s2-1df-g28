package app.domain.store;

import app.controller.App;
import app.domain.conceptual.UserRole;
import app.domain.model.Company;
import app.domain.shared.Constants;
import pt.isep.lei.esoft.auth.AuthFacade;

import java.io.Serializable;
import java.util.ArrayList;

public class UserRoleStore implements Serializable {

    /**
     * rolesList
     */
    private ArrayList<UserRole> rolesList;

    /**
     * constructor without parameters
     */
    public UserRoleStore() {
        this.rolesList = new ArrayList<>();
        for (String role : Constants.USER_ROLES) {
            addRole(role, role);
            Company.getAuthFacade().addUserRole(role, role);
        }
    }

    /**
     * method to get the user roles list
     *
     * @return rolesList
     */
    public ArrayList<UserRole> getRolesList() {
        return rolesList;
    }

    /**
     * method to add a new role
     *
     * @param id
     * @param description
     */
    public void addRole(String id, String description) {
        UserRole role = new UserRole(id, description);
        if (!rolesList.contains(role)) {
            rolesList.add(role);
        }
    }
}
