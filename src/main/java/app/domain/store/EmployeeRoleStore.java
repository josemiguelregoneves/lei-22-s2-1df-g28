package app.domain.store;

import java.util.ArrayList;
import java.util.List;

import pt.isep.lei.esoft.auth.AuthFacade;
import pt.isep.lei.esoft.auth.domain.model.UserRole;

public class EmployeeRoleStore {
    /**
     * list of employee roles
     */
    private ArrayList<UserRole> rolesList;

    /**
     * authFacade
     */
    private AuthFacade authFacade;

    public EmployeeRoleStore() {
        this.rolesList = new ArrayList<>();
    }

    /**
     * method to get the roles list
     *
     * @return rolesList
     */
    public ArrayList<UserRole> getRolesList() {
        return rolesList;
    }

    /**
     * method to set the roles list
     *
     * @param rolesList
     */
    public void setRolesList(ArrayList<UserRole> rolesList) {
        this.rolesList = rolesList;
    }

    /**
     * method to add a role (UserRole) to the rolesList
     *
     * @param roleId
     * @param description
     */
    public void addRole(String roleId, String description) {
        UserRole userRole = new UserRole(roleId, description);
        rolesList.add(userRole);
        this.authFacade.addUserRole(roleId, description);
    }
}
