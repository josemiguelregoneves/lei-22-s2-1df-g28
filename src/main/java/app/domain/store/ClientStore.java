package app.domain.store;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import app.domain.conceptual.Client;
import app.domain.shared.PasswordGenerator;
import pt.isep.lei.esoft.auth.AuthFacade;

public class ClientStore implements Serializable {

    /**
     * list of all clients
     */
    private ArrayList<Client> clientList;

    /**
     * authFacade
     */
    private AuthFacade authFacade;

    public ClientStore() {
        this.clientList = new ArrayList<>();
    }

    /**
     * method to get the client list
     *
     * @return clientList
     */
    public ArrayList<Client> getClientList() {
        return clientList;
    }

    /**
     * method to set the client list
     *
     * @param clientList
     */
    public void setClientList(ArrayList<Client> clientList) {
        this.clientList = clientList;
    }

    /**
     * method to get the authFacade
     *
     * @return authFacade
     */
    public AuthFacade getAuthFacade() {
        return authFacade;
    }

    /**
     * method to set the authFacade
     *
     * @param authFacade
     */
    public void setAuthFacade(AuthFacade authFacade) {
        this.authFacade = authFacade;
    }

    /**
     * method to create a new client
     *
     * @param ccNumber    client
     * @param name        client
     * @param sex         client
     * @param birthDate   client
     * @param phoneNumber client
     * @param email       client
     * @param snsNumber   client
     * @return new Client()
     */
    public Client createClient(int ccNumber, String name, String address, String sex, Date birthDate, int phoneNumber, String email, int snsNumber) {
        String password = PasswordGenerator.generatePassword();
        return new Client(ccNumber, name, address, sex, birthDate, phoneNumber, email, snsNumber, password);
    }

    /**
     * method to validate if client doesnt exists in the list
     *
     * @param c client
     * @return true/false
     */
    public boolean validateClient(Client c) {
        if (c == null) {
            return false;
        } else {
            return !this.clientList.contains(c);
        }
    }

    /**
     * method to add the created client to the client list
     *
     * @param snsUser client
     * @return true/false
     */
    public boolean saveClient(Client snsUser) {
        if (!ccNumberExists(snsUser) && !emailExists(snsUser) && !phoneNumberExists(snsUser) && !snsNumberExists(snsUser)) {
            clientList.add(snsUser);
            return true;
        } else {
            return false;
        }
    }

    /**
     * method to verify if exists any client with that email
     *
     * @param snsUser
     *
     * @return true/false
     */
    private boolean emailExists(Client snsUser) {
        boolean exists = false;
        int index = 0;
        String email = snsUser.getEmail();
        while (!exists && index != clientList.size()) {
            if (clientList.get(index).getEmail().equals(email)) {
                exists = true;
            }
            index++;
        }
        return exists;
    }

    /**
     * method to verify if exists any client with that phone number
     *
     * @param snsUser
     *
     * @return true/false
     */
    private boolean phoneNumberExists(Client snsUser) {
        boolean exists = false;
        int index = 0;
        int phoneNumber = snsUser.getPhoneNumber();
        while (!exists && index != clientList.size()) {
            if (clientList.get(index).getPhoneNumber() == phoneNumber) {
                exists = true;
            }
            index++;
        }
        return exists;
    }

    /**
     * method to verify if exists any client with that ccNumber
     *
     * @param snsUser
     *
     * @return true/false
     */
    private boolean ccNumberExists(Client snsUser) {
        boolean exists = false;
        int index = 0;
        int ccNumber = snsUser.getCcNumber();
        while (!exists && index != clientList.size()) {
            if (clientList.get(index).getCcNumber() == ccNumber) {
                exists = true;
            }
            index++;
        }
        return exists;
    }

    /**
     * method to verify if exists any client with that snsNumber
     *
     * @param snsUser
     *
     * @return true/false
     */
    private boolean snsNumberExists(Client snsUser) {
        boolean exists = false;
        int index = 0;
        int snsNumber = snsUser.getSnsNumber();
        while (!exists && index != clientList.size()) {
            if (clientList.get(index).getCcNumber() == snsNumber) {
                exists = true;
            }
            index++;
        }
        return exists;
    }
}