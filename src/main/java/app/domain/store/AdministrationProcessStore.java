package app.domain.store;

import app.domain.conceptual.AdministrationProcess;
import app.domain.conceptual.Vaccine;
import app.domain.model.Company;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;

public class AdministrationProcessStore implements Serializable {

    /**
     * list of administration processes
     */
    private ArrayList<AdministrationProcess> adminProcessList;

    /**
     * constructor without parameters
     */
    public AdministrationProcessStore() {
        this.adminProcessList = new ArrayList<>();
    }

    /**
     * method to get the administration processes list
     *
     * @return administrationProcessesList
     */
    public ArrayList<AdministrationProcess> getAdminProcessList() {
        return adminProcessList;
    }

    /**
     * method to set the administration processes list
     *
     * @param adminProcessList
     */
    public void setAdminProcessList(ArrayList<AdministrationProcess> adminProcessList) {
        this.adminProcessList = adminProcessList;
    }
}
