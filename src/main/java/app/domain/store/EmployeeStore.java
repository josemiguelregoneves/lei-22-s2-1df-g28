package app.domain.store;

import java.io.Serializable;
import java.util.ArrayList;

import app.domain.conceptual.Employee;
import app.domain.shared.PasswordGenerator;
import app.domain.conceptual.VaccinationCenter;
import pt.isep.lei.esoft.auth.AuthFacade;

public class EmployeeStore implements Serializable {
    /**
     * list of all employees
     */
    private ArrayList<Employee> employeeList;

    /**
     * authFacade
     */
    private AuthFacade authFacade;

    /**
     * constructor without parameters
     */
    public EmployeeStore() {
        this.employeeList = new ArrayList<>();
    }

    /**
     * method to get the employees list
     *
     * @return employeeList
     */
    public ArrayList<Employee> getEmployeeList() {
        return employeeList;
    }

    /**
     * method to set the employees list
     *
     * @param employeeList
     */
    public void setEmployeeList(ArrayList<Employee> employeeList) {
        this.employeeList = employeeList;
    }

    /**
     * method to get the authFacade
     *
     * @return authFacade
     */
    public AuthFacade getAuthFacade() {
        return authFacade;
    }

    /**
     * method to set the authFacade
     *
     * @param authFacade
     */
    public void setAuthFacade(AuthFacade authFacade) {
        this.authFacade = authFacade;
    }

    /**
     * method to create a new employee
     *
     * @param name        employee
     * @param address     employee
     * @param phoneNumber employee
     * @param email       employee
     * @param ccNumber    employee
     * @param idRole      employee
     * @return new Employee()
     */
    public Employee createEmployee(String name, String address, int phoneNumber, String email, int ccNumber, String idRole, VaccinationCenter center) {
        String password = PasswordGenerator.generatePassword();
        int id = employeeList.size();
        return new Employee(id, name, address, phoneNumber, email, ccNumber, password, idRole, center);
    }

    /**
     * method to validate if employee doesnt exists in the list
     *
     * @param e employee
     * @return true/false
     */
    public boolean validateEmployee(Employee e) {
        if (e == null) {
            return false;
        } else {
            return !this.employeeList.contains(e);
        }
    }

    /**
     * method to add the created employee to the employees list
     *
     * @param e employee
     * @return true/false
     */
    public boolean saveEmployee(Employee e) {
        if (!emailExists(e) && !phoneNumberExists(e) && !ccNumberExists(e)) {
            employeeList.add(e);
            return true;
        } else {
            return false;
        }
    }

    /**
     * method to remove some employee for the employees list
     *
     * @param e employee
     * @return true/false
     */
    public boolean removeEmployee(Employee e) {
        if (employeeList.contains(e)) {
            employeeList.remove(e);
            return true;
        } else {
            return false;
        }
    }

    /**
     * method to send an email to some employee
     *
     * @param e     employee
     * @param email employee
     */
    public void sendEmail(Employee e, String email) {
        if (employeeList.contains(e) && e.getEmail().equals(email)) {
            System.out.println("Email");
        } else {
            System.out.println("Invalid email or employee");
        }
    }

    /**
     * verify if email exists in the list
     *
     * @param e
     * @return true/false
     */
    private boolean emailExists(Employee e) {
        boolean exists = false;
        int index = 0;
        String email = e.getEmail();
        while (!exists && index != employeeList.size()) {
            if (employeeList.get(index).getEmail().equals(email)) {
                exists = true;
            }
            index++;
        }
        return exists;
    }

    /**
     * verify if id exists in the list
     *
     * @param e
     * @return true/false
     */
    private boolean idExists(Employee e) {
        boolean exists = false;
        int index = 0;
        int id = e.getId();
        while (!exists && index != employeeList.size()) {
            if (employeeList.get(index).getId() == id) {
                exists = true;
            }
            index++;
        }
        return exists;
    }

    /**
     * verify if phoneNumber exists in the list
     *
     * @param e
     * @return true/false
     */
    private boolean phoneNumberExists(Employee e) {
        boolean exists = false;
        int index = 0;
        int phoneNumber = e.getPhoneNumber();
        while (!exists && index != employeeList.size()) {
            if (employeeList.get(index).getPhoneNumber() == phoneNumber) {
                exists = true;
            }
            index++;
        }
        return exists;
    }

    /**
     * verify if ccNumber exists in the list
     *
     * @param e
     * @return true/false
     */
    private boolean ccNumberExists(Employee e) {
        boolean exists = false;
        int index = 0;
        int ccNumber = e.getCcNumber();
        while (!exists && index != employeeList.size()) {
            if (employeeList.get(index).getCcNumber() == ccNumber) {
                exists = true;
            }
            index++;
        }
        return exists;
    }
}
