package app.domain.store;

import app.domain.conceptual.*;
import app.domain.dto.VaccineScheduleDTO;

import java.io.Serializable;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;

public class VaccineScheduleStore implements Serializable {

    /**
     * list of all vaccine schedules
     */
    private ArrayList<VaccineSchedule> vcScheduleList;

    /**
     * list of all vaccines schedules in waiting state
     */
    private ArrayList<VaccineSchedule> waitingSchedulesList;

    /**
     * list of all completed vaccine schedules
     */
    private ArrayList<VaccineSchedule> completedSchedulesList;

    /**
     * list of all full vaccinated
     */
    private ArrayList<VaccineSchedule> fullVaccinated;

    /**
     * constructor without parameters
     */
    public VaccineScheduleStore() {
        this.vcScheduleList = new ArrayList<>();
        this.completedSchedulesList = new ArrayList<>();
        this.fullVaccinated = new ArrayList<>();
        this.waitingSchedulesList = new ArrayList<>();
    }

    /**
     * method to get the vaccine schedules list
     *
     * @return vcScheduleList
     */
    public ArrayList<VaccineSchedule> getVcScheduleList() {
        return vcScheduleList;
    }

    /**
     * method to set the vaccine schedules list
     *
     * @param vcScheduleList
     */
    public void setVcScheduleList(ArrayList<VaccineSchedule> vcScheduleList) {
        this.vcScheduleList = vcScheduleList;
    }

    /**
     * method to get the completed vaccine schedules list
     *
     * @return completedSchedules
     */
    public ArrayList<VaccineSchedule> getCompletedSchedulesList() {
        return completedSchedulesList;
    }

    /**
     * method to set the completed vaccine schedules list
     *
     * @param completedSchedulesList
     */
    public void setCompletedSchedulesList(ArrayList<VaccineSchedule> completedSchedulesList) {
        this.completedSchedulesList = completedSchedulesList;
    }

    /**
     * method to get the full vaccine schedules list
     *
     * @return full vaccinated
     */
    public ArrayList<VaccineSchedule> getFullVaccinated() {
        return fullVaccinated;
    }

    /**
     * method to set the full vaccine schedules list
     *
     * @param fullVaccinated
     */
    public void setFullVaccinated(ArrayList<VaccineSchedule> fullVaccinated) {
        this.fullVaccinated = fullVaccinated;
    }

    /**
     * method to get the waiting schedules
     *
     * @return waitingSchedules
     */
    public ArrayList<VaccineSchedule> getWaitingSchedulesList() {
        return waitingSchedulesList;
    }

    /**
     * method to set the waiting schedules
     *
     * @param waitingSchedulesList
     */
    public void setWaitingSchedulesList(ArrayList<VaccineSchedule> waitingSchedulesList) {
        this.waitingSchedulesList = waitingSchedulesList;
    }

    /**
     * method to get the completed schedules of a specific date
     *
     * @param date
     *
     * @return competedSchedulesList
     */
    public ArrayList<VaccineSchedule> getCompletedSchedulesByDate(Date date) {
        ArrayList<VaccineSchedule> completed = new ArrayList<>();
        for (VaccineSchedule schedule : completedSchedulesList) {
            if (schedule.getScheduleDate().equals(date)) {
                completed.add(schedule);
            }
        }
        return completed;
    }

    /**
     * method to get the full vaccinated schedules by date
     *
     * @param date
     *
     * @return fullVaccinatedList
     */
    public ArrayList<VaccineSchedule> getFullVaccinatedByDate(Date date) {
        ArrayList<VaccineSchedule> full = new ArrayList<>();
        for (VaccineSchedule schedule : fullVaccinated) {
            if (schedule.getScheduleDate().equals(date)) {
                full.add(schedule);
            }
        }
        return full;
    }

    /**
     * method to create a new vaccine schedule from a DTO
     *
     * @param vcScheduleDto
     *
     * @return new VaccineSchedule
     */
    public VaccineSchedule createSchedule(VaccineScheduleDTO vcScheduleDto) {
        int snsNumber = vcScheduleDto.snsNumber;
        VaccineType vcType = vcScheduleDto.vcType;
        Date scheduleDate = vcScheduleDto.scheduleDate;
        Time scheduleTime = vcScheduleDto.scheduleTime;

        return new VaccineSchedule(snsNumber, vcType, scheduleDate, scheduleTime);
    }

    /**
     * method to create a vaccine schedule
     *
     * @param snsNumber
     * @param vcType
     * @param scheduleDate
     * @param scheduleTime
     *
     * @return new VaccineSchedule
     */
    public VaccineSchedule createSchedule(int snsNumber, VaccineType vcType, Date scheduleDate, Time scheduleTime) {
        return new VaccineSchedule(snsNumber, vcType, scheduleDate, scheduleTime);
    }

    /**
     * method to add a vaccine schedule to the schedule list
     *
     * @param vcSchedule
     */
    public void saveSchedule(VaccineSchedule vcSchedule) {
        vcScheduleList.add(vcSchedule);
    }

}
