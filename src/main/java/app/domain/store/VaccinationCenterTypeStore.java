package app.domain.store;

import app.domain.model.VaccinationCenterType;
import pt.isep.lei.esoft.auth.AuthFacade;

import java.util.ArrayList;

public class VaccinationCenterTypeStore {
    /**
     * list of vaccination centers types
     */
    private ArrayList<VaccinationCenterType> typesList;

    /**
     * authFacade
     */
    private AuthFacade authFacade;

    public VaccinationCenterTypeStore() {
        this.typesList = new ArrayList<>();
    }

    /**
     * method to get typesList
     *
     * @return typesList
     */
    public ArrayList<VaccinationCenterType> getTypesList() {
        return typesList;
    }

    /**
     * method to set the typesList
     *
     * @param typesList
     */
    public void setTypesList(ArrayList<VaccinationCenterType> typesList) {
        this.typesList = typesList;
    }

    /**
     * method to get the authFacade
     *
     * @return authFacade
     */
    public AuthFacade getAuthFacade() {
        return authFacade;
    }

    /**
     * method to set the authFacade
     *
     * @param authFacade
     */
    public void setAuthFacade(AuthFacade authFacade) {
        this.authFacade = authFacade;
    }

    /**
     * method to add a type (VaccinationCenterType) to the typesList
     *
     * @param typeId
     * @param description
     */
    public void addType(String typeId, String description) {
        VaccinationCenterType centerType = new VaccinationCenterType(typeId, description);
        typesList.add(centerType);
    }
}
