package app.domain.store;

import app.domain.conceptual.VaccineType;
import pt.isep.lei.esoft.auth.AuthFacade;

import java.io.Serializable;
import java.util.ArrayList;

public class VaccineTypeStore implements Serializable {
    /**
     * list of all vaccines types
     */
    private ArrayList<VaccineType> vaccineTypeList;

    /**
     * authFacade
     */
    private AuthFacade authFacade;

    /**
     * constructor without parameters
     */
    public VaccineTypeStore() {
        this.vaccineTypeList = new ArrayList<>();
    }

    /**
     * method to get the list of vaccines
     *
     * @return vaccinesList
     */
    public ArrayList<VaccineType> getVaccineTypeList() {
        return vaccineTypeList;
    }

    /**
     * method to set the list of vaccines
     *
     * @param vaccineTypeList
     */
    public void setVaccineTypeList(ArrayList<VaccineType> vaccineTypeList) {
        this.vaccineTypeList = vaccineTypeList;
    }

    /**
     * method to get the authfacade
     *
     * @return authfacade
     */
    public AuthFacade getAuthFacade() {
        return authFacade;
    }

    /**
     * method to set the authfacade
     *
     * @param authFacade
     */
    public void setAuthFacade(AuthFacade authFacade) {
        this.authFacade = authFacade;
    }

    /**
     * method to create a new vaccine type
     *
     * @param name
     * @return vaccine type
     */
    public VaccineType createVaccineType(String name) {
        return new VaccineType(name);
    }

    /**
     * method to validate if vaccine type exists in the list
     *
     * @param vcType vaccine
     * @return true/false
     */
    public boolean validateVaccineType(VaccineType vcType) {
        if (vcType == null) {
            return false;
        } else {
            return !this.vaccineTypeList.contains(vcType);
        }
    }

    /**
     * method to save the vaccine type
     *
     * @param vcType
     *
     * @return true/false
     */
    public boolean saveVaccineType(VaccineType vcType) {
        if (!vcTypeExists(vcType.getName())) {
            vaccineTypeList.add(vcType);
            return true;
        } else {
            return false;
        }
    }

    /**
     * method to remove the vaccines type
     *
     * @param vcType
     *
     * @return true/false
     */
    public boolean removeVaccineType(VaccineType vcType) {
        if (vaccineTypeList.contains(vcType)) {
            vaccineTypeList.remove(vcType);
            return true;
        } else {
            return false;
        }
    }

    /**
     * verify if vaccine type exists in the list
     *
     * @param name
     * @return true/false
     */
    private boolean vcTypeExists(String name) {
        boolean exists = false;
        int index = 0;
        while (!exists && index != vaccineTypeList.size()) {
            if (vaccineTypeList.get(index).getName().equals(name)) {
                exists = true;
            }
            index++;
        }
        return exists;
    }
}
