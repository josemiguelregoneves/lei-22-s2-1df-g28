package app.domain.store;

import java.io.Serializable;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import app.domain.conceptual.Client;
import app.domain.conceptual.Employee;
import app.domain.conceptual.VaccinationCenter;
import app.domain.shared.Alert;
import pt.isep.lei.esoft.auth.AuthFacade;

public class VaccinationCenterStore implements Serializable {
    /**
     * list of all centers
     */
    private ArrayList<VaccinationCenter> centersList;

    /**
     * authFacade
     */
    private AuthFacade authFacade;

    /**
     * constructor without parameters
     */
    public VaccinationCenterStore() {
        this.centersList = new ArrayList<>();
    }

    /**
     * method to get centersList
     *
     * @return centersList
     */
    public ArrayList<VaccinationCenter> getCentersList() {
        return centersList;
    }

    /**
     * method to set centersList value
     *
     * @param centersList
     */
    public void setCentersList(ArrayList<VaccinationCenter> centersList) {
        this.centersList = centersList;
    }

    /**
     * method to get authFacade value
     *
     * @return authFacade
     */
    public AuthFacade getAuthFacade() {
        return authFacade;
    }

    /**
     * method to set authFacade value
     *
     * @param authFacade
     */
    public void setAuthFacade(AuthFacade authFacade) {
        this.authFacade = authFacade;
    }

    /**
     * method to create a new vaccination center
     *
     * @param name         vaccination center
     * @param address      vaccination center
     * @param phoneNumber  vaccination center
     * @param email        vaccination center
     * @param faxNumber    vaccination center
     * @param website      vaccination center
     * @param slotDuration vaccination center
     * @param maxVaccines  vaccination center
     * @param idType       vaccination center
     * @return new VaccinationCenter()
     */
    public VaccinationCenter createVaccinationCenter(String name, String address, int phoneNumber, String email, int faxNumber, String website, Time slotDuration, int maxVaccines, Time openingHour, Time closingHour, String idType) {
        return new VaccinationCenter(name, address, phoneNumber, email, faxNumber, website, slotDuration, maxVaccines, openingHour, closingHour, idType);
    }

    /**
     * method to validate if there is a vaccination center with the same name and if the vaccination center is valid
     *
     * @param vc
     * @return true/false
     */
    public boolean validateCenter(VaccinationCenter vc) {
        for (VaccinationCenter vaccinationCenter : centersList) {
            if (vc == null) {
                return false;
            } else if (vaccinationCenter.getName().equals(vc.getName())) {
                return false;
            }
        }
        return true;
    }

    /**
     * method to save a vaccination center in centersList if there isn't already one with the same variable name
     *
     * @param vc
     * @return true/false
     */
    public boolean saveCenter(VaccinationCenter vc) {
        if (!phoneNumberExists(vc) && !websiteExists(vc) && !faxNumberExists(vc) && !emailExists(vc)) {
            centersList.add(vc);
            return true;
        } else {
            return false;
        }
    }

    /**
     * method to remove a vaccination center from centersList
     *
     * @param vc
     * @return true/false
     */
    public boolean removeCenter(VaccinationCenter vc) {
        if (centersList.contains(vc)) {
            centersList.remove(vc);
            return true;
        } else {
            return false;
        }
    }

    /**
     * method to send an email
     *
     * @param name
     * @param email
     * @param idType
     */
    public void sendEmail(String name, String email, String idType) {
        System.out.println("Send email");
    }

    /**
     * verify if email exists in the list
     *
     * @param vc
     * @return true/false
     */
    private boolean emailExists(VaccinationCenter vc) {
        boolean exists = false;
        int index = 0;
        String email = vc.getEmail();
        while (!exists && index != centersList.size()) {
            if (centersList.get(index).getEmail().equals(email)) {
                exists = true;
            }
            index++;
        }
        return exists;
    }

    /**
     * verify if faxNumber exists in the list
     *
     * @param vc
     * @return true/false
     */
    private boolean faxNumberExists(VaccinationCenter vc) {
        boolean exists = false;
        int index = 0;
        int fax = vc.getFaxNumber();
        while (!exists && index != centersList.size()) {
            if (centersList.get(index).getFaxNumber() == fax) {
                exists = true;
            }
            index++;
        }
        return exists;
    }

    /**
     * verify if phoneNumber exists in the list
     *
     * @param vc
     * @return true/false
     */
    private boolean phoneNumberExists(VaccinationCenter vc) {
        boolean exists = false;
        int index = 0;
        int phoneNumber = vc.getPhoneNumber();
        while (!exists && index != centersList.size()) {
            if (centersList.get(index).getPhoneNumber() == phoneNumber) {
                exists = true;
            }
            index++;
        }
        return exists;
    }

    /**
     * verify if email exists in the list
     *
     * @param vc
     * @return true/false
     */
    private boolean websiteExists(VaccinationCenter vc) {
        boolean exists = false;
        int index = 0;
        String web = vc.getWebsite();
        while (!exists && index != centersList.size()) {
            if (centersList.get(index).getWebsite().equals(web)) {
                exists = true;
            }
            index++;
        }
        return exists;
    }
}
