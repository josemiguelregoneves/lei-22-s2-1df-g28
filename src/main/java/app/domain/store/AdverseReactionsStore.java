package app.domain.store;

import app.domain.conceptual.AdverseReactions;

import java.io.Serializable;
import java.util.ArrayList;

public class AdverseReactionsStore implements Serializable {

    /**
     * list of adverse reactions
     */
    private ArrayList<AdverseReactions> reactionsList;

    /**
     * constructor without parameters
     */
    public AdverseReactionsStore() {
        this.reactionsList = new ArrayList<>();
    }

    /**
     * method to get the list of adverse reactions
     *
     * @return reactions list
     */
    public ArrayList<AdverseReactions> getReactionsList() {
        return reactionsList;
    }

    /**
     * method to set the list of adverse reactions
     *
     * @param reactionsList
     */
    public void setReactionsList(ArrayList<AdverseReactions> reactionsList) {
        this.reactionsList = reactionsList;
    }
}
