package app.domain.store;

import app.domain.conceptual.AdministrationProcess;
import app.domain.conceptual.AgeRange;
import app.domain.conceptual.Vaccine;
import app.domain.model.Company;

import java.io.Serializable;
import java.util.ArrayList;

public class AgeRangeStore implements Serializable {

    /**
     * age range list
     */
    private ArrayList<AgeRange> ageRangeList;

    /**
     * constructor without parameters
     */
    public AgeRangeStore() {
        this.ageRangeList = new ArrayList<>();
    }

    /**
     * method to get the age range list
     *
     * @return age range list
     */
    public ArrayList<AgeRange> getAgeRangeList() {
        return ageRangeList;
    }

    /**
     * method to set the age range list
     *
     * @param ageRangeList
     */
    public void setAgeRangeList(ArrayList<AgeRange> ageRangeList) {
        this.ageRangeList = ageRangeList;
    }
}
