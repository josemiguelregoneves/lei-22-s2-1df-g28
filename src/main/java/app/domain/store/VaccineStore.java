package app.domain.store;

import app.domain.conceptual.Client;
import app.domain.conceptual.VaccineSchedule;
import app.domain.conceptual.AdministrationProcess;
import app.domain.conceptual.Vaccine;
import app.domain.conceptual.VaccineType;
import app.domain.model.Company;
import app.domain.shared.Validator;
import pt.isep.lei.esoft.auth.AuthFacade;

import java.io.Serializable;
import java.util.ArrayList;

public class VaccineStore implements Serializable {
    /**
     * list of all vaccines
     */
    private ArrayList<Vaccine> vaccineList;

    /**
     * authFacade
     */
    private AuthFacade authFacade;

    /**
     * constructor without parameters
     */
    public VaccineStore() {
        this.vaccineList = new ArrayList<>();
    }

    /**
     * method to get the list of all vaccines
     *
     * @return vaccinesList
     */
    public ArrayList<Vaccine> getVaccineList() {
        return vaccineList;
    }

    /**
     * method to set the vaccinesList
     *
     * @param vaccineList
     */
    public void setVaccineList(ArrayList<Vaccine> vaccineList) {
        this.vaccineList = vaccineList;
    }

    /**
     * method to get the authfacade
     *
     * @return authfacade
     */
    public AuthFacade getAuthFacade() {
        return authFacade;
    }

    /**
     * method to set the authfacade
     *
     * @param authFacade
     */
    public void setAuthFacade(AuthFacade authFacade) {
        this.authFacade = authFacade;
    }

    /**
     * method to create a new vaccine
     *
     * @param vcType
     * @param adminProcess
     * @param name
     * @return vaccine
     */
    public Vaccine createVaccine(VaccineType vcType, AdministrationProcess adminProcess, String name) {
        return new Vaccine(vcType, adminProcess, name);
    }

    /**
     * method to validate if vaccine exists in the list
     *
     * @param vc vaccine
     * @return true/false
     */
    public boolean validateVaccine(Vaccine vc) {
        if (vc == null) {
            return false;
        } else {
            return !this.vaccineList.contains(vc);
        }
    }

    /**
     * method to save the created vaccines
     *
     * @param vc
     *
     * @return true/false
     */
    public boolean saveVaccine(Vaccine vc) {
        vaccineList.add(vc);
        Company.getAdminProcessStore().getAdminProcessList().add(vc.getAdministration());
        Company.getAgeRangeStore().getAgeRangeList().add(vc.getAdministration().getRange());
        return true;
    }

    /**
     * method to get the list of vaccines of a specified type
     *
     * @param schedule
     *
     * @return vaccinesList
     */
    public ArrayList<Vaccine> getVaccinesByType(VaccineSchedule schedule) {
        VaccineType vcType = schedule.getVcType();
        ArrayList<Vaccine> vcList = new ArrayList<>();
        for (Vaccine vc : vaccineList) {
            if (vc.getType().equals(vcType)) {
                vcList.add(vc);
            }
        }
        return vcList;
    }

    /**
     * method to get the first vaccine of a specific client and vcType
     *
     * @param schedule
     *
     * @return vaccine
     */
    public Vaccine getFirstVaccineOfType(VaccineSchedule schedule) {
        Client snsUser = Validator.getClientBySnsNumber(schedule.getSnsNumber());
        VaccineType vcType = schedule.getVcType();

        Vaccine vc = null;
        for (VaccineSchedule complete : snsUser.getCompletedList()) {
            if (Validator.getVaccineByName(complete.getVc()).getType().equals(vcType)) {
                vc = Validator.getVaccineByName(complete.getVc());
                break;
            }
        }

        return vc;
    }

    /**
     * method to remove a vaccine from the list
     *
     * @param vc
     *
     * @return true/false
     */
    public boolean removeVaccine(Vaccine vc) {
        if (vaccineList.contains(vc)) {
            vaccineList.remove(vc);
            return true;
        } else {
            return false;
        }
    }

    /**
     * method to get the available vaccines for a specific schedule
     *
     * @param schedule
     *
     * @return vaccinesList
     */
    public ArrayList<Vaccine> getAvailableVaccinesForSchedule(VaccineSchedule schedule) {
        ArrayList<Vaccine> vcList = new ArrayList<>();
        Vaccine vc = Company.getVcStore().getFirstVaccineOfType(schedule);
        if (vc == null) {
            vcList = Company.getVcStore().getVaccinesByType(schedule);
        } else {
            vcList.add(vc);
        }
        return vcList;
    }

    /**
     * verify if vaccine type exists in the list
     *
     * @param vcType
     * @return true/false
     */
    private boolean vcTypeExists(VaccineType vcType) {
        boolean exists = false;
        int index = 0;
        String name = vcType.getName();
        while (!exists && index != vaccineList.size()) {
            if (vaccineList.get(index).getType().getName().equals(name)) {
                exists = true;
            }
            index++;
        }
        return exists;
    }
}
