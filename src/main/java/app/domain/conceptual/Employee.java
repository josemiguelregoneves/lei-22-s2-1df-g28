package app.domain.conceptual;

import java.io.Serializable;
import java.util.Objects;

public class Employee implements Serializable {
    /**
     * id
     */
    private int id;

    /**
     * name
     */
    private String name;

    /**
     * address
     */
    private String address;

    /**
     * phone number
     */
    private int phoneNumber;

    /**
     * email
     */
    private String email;

    /**
     * ccNumber
     */
    private int ccNumber;

    /**
     * password
     */
    private String password;

    /**
     * idRole
     */
    private String idRole;

    /**
     * center
     */
    private VaccinationCenter center;

    /**
     * constructor with parameters
     *
     * @param id
     * @param name
     * @param address
     * @param phoneNumber
     * @param email
     * @param ccNumber
     * @param password
     * @param idRole
     * @param center
     */
    public Employee(int id, String name, String address, int phoneNumber, String email, int ccNumber, String password, String idRole, VaccinationCenter center) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.ccNumber = ccNumber;
        this.password = password;
        this.idRole = idRole;
        this.center = center;
    }

    /**
     * method to get the id
     *
     * @return id
     */
    public int getId() {
        return id;
    }

    /**
     * method to get the name
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * method to get the address
     *
     * @return address
     */
    public String getAddress() {
        return address;
    }

    /**
     * method to get the phoneNumber
     *
     * @return phoneNumber
     */
    public int getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * method to get the email
     *
     * @return email
     */
    public String getEmail() {
        return email;
    }

    /**
     * method to get the ccNumber
     *
     * @return ccNumber
     */
    public int getCcNumber() {
        return ccNumber;
    }

    /**
     * method to get the password
     *
     * @return password
     */
    public String getPassword() {
        return password;
    }

    /**
     * method to get the idRole
     *
     * @return idRole
     */
    public String getIdRole() {
        return idRole;
    }

    /**
     * method to get the center
     *
     * @return center
     */
    public VaccinationCenter getCenter() {
        return center;
    }

    /**
     * method to set the id
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * method to set the name
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * method to set the address
     *
     * @param address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * method to set the phoneNumber
     *
     * @param phoneNumber
     */
    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * method to set the email
     *
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * method to set the ccNumber
     *
     * @param ccNumber
     */
    public void setCcNumber(int ccNumber) {
        this.ccNumber = ccNumber;
    }

    /**
     * method to set the password
     *
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * method to set the idRole
     *
     * @param idRole
     */
    public void setIdRole(String idRole) {
        this.idRole = idRole;
    }

    /**
     * method to set the center
     *
     * @param center
     */
    public void setCenter(VaccinationCenter center) {
        this.center = center;
    }

    /**
     * to string mthod
     *
     * @return formated string
     */
    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", phoneNumber=" + phoneNumber +
                ", email='" + email + '\'' +
                ", ccNumber=" + ccNumber +
                ", password='" + password + '\'' +
                ", idRole='" + idRole + '\'' +
                ", center=" + center +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return phoneNumber == employee.phoneNumber && ccNumber == employee.ccNumber && email.equals(employee.email) && password.equals(employee.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(phoneNumber, email, ccNumber, password);
    }
}
