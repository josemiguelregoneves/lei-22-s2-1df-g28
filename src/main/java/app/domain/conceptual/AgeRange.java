package app.domain.conceptual;

import java.io.Serializable;

public class AgeRange implements Serializable {
    /**
     * min age
     */
    private int minAge;

    /**
     * max age
     */
    private int maxAge;

    /**
     * constructor with parameters
     *
     * @param minAge
     * @param maxAge
     */
    public AgeRange(int minAge, int maxAge) {
        this.minAge = minAge;
        this.maxAge = maxAge;
    }

    /**
     * method to get the min age
     *
     * @return min age
     */
    public int getMinAge() {
        return minAge;
    }

    /**
     * method to set the min age
     *
     * @param minAge
     */
    public void setMinAge(int minAge) {
        this.minAge = minAge;
    }

    /**
     * method to get the max age
     *
     * @return max age
     */
    public int getMaxAge() {
        return maxAge;
    }

    /**
     * method to set the max age
     *
     * @param maxAge
     */
    public void setMaxAge(int maxAge) {
        this.maxAge = maxAge;
    }

    /**
     * method to check if age is in range
     *
     * @param age
     *
     * @return true/false
     */
    public boolean isInRange(int age) {
        return age >= minAge && age <= maxAge;
    }

    /**
     * to string method
     *
     * @return formated string
     */
    @Override
    public String toString() {
        return "AgeRange{" +
                "minAge=" + minAge +
                ", maxAge=" + maxAge +
                '}';
    }
}