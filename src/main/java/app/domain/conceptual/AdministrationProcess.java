package app.domain.conceptual;

import java.io.Serializable;

public class AdministrationProcess implements Serializable {
    /**
     * range
     */
    private AgeRange range;

    /**
     * dosage
     */
    private int dosage;

    /**
     * doses
     */
    private int doses;

    /**
     * constructor with parameters
     *
     * @param range
     * @param dosage
     * @param doses
     */
    public AdministrationProcess(AgeRange range, int dosage, int doses) {
        this.range = range;
        this.dosage = dosage;
        this.doses = doses;
    }

    /**
     * method to get the range
     *
     * @return range
     */
    public AgeRange getRange() {
        return range;
    }

    /**
     * method to set the range
     *
     * @param range
     */
    public void setRange(AgeRange range) {
        this.range = range;
    }

    /**
     * method to get the dosage
     *
     * @return dosage
     */
    public int getDosage() {
        return dosage;
    }

    /**
     * method to set the dosage
     *
     * @param dosage
     */
    public void setDosage(int dosage) {
        this.dosage = dosage;
    }

    /**
     * method to get the doses
     *
     * @return doses
     */
    public int getDoses() {
        return doses;
    }

    /**
     * method to set the doses
     *
     * @param doses
     */
    public void setDoses(int doses) {
        this.doses = doses;
    }

    /**
     * to string method
     *
     * @return formated string
     */
    @Override
    public String toString() {
        return "AdministrationProcess{" +
                "range=" + range +
                ", dosage=" + dosage +
                ", doses=" + doses +
                '}';
    }
}

