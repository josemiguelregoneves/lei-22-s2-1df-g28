package app.domain.conceptual;

import java.io.Serializable;

public class UserRole implements Serializable {

    private String id;

    private String description;

    public UserRole(String id, String description) {
        this.id = id;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "UserRole{" +
                "id='" + id + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

}
