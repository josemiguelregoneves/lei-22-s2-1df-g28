package app.domain.conceptual;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

public class Client implements Serializable {

    /**
     * cc number
     */
    private final int ccNumber;

    /**
     * name
     */
    private final String name;

    /**
     * address
     */
    private final String address;

    /**
     * sex
     */
    private final String sex;

    /**
     * birthDate
     */
    private final Date birthDate;


    /**
     * phoneNumber
     */
    private final int phoneNumber;


    /**
     * email
     */
    private final String email;

    /**
     * snsNumber
     */
    private final int snsNumber;

    /**
     * password
     */
    private String password;

    /**
     * schedulesList
     */
    private ArrayList<VaccineSchedule> schedules;

    /**
     * completed schedules list
     */
    private ArrayList<VaccineSchedule> completedList;

    /**
     * constructor with parameters
     *
     * @param ccNumber
     * @param name
     * @param address
     * @param sex
     * @param birthDate
     * @param phoneNumber
     * @param email
     * @param snsNumber
     * @param password
     */
    public Client(int ccNumber, String name, String address, String sex, Date birthDate, int phoneNumber, String email, int snsNumber, String password) {
        this.ccNumber = ccNumber;
        this.name = name;
        this.address = address;
        this.sex = sex;
        this.birthDate = birthDate;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.snsNumber = snsNumber;
        this.password = password;
        this.schedules = new ArrayList<>();
        this.completedList = new ArrayList<>();
    }

    /**
     * method to get the ccNumber
     *
     * @return ccNumber
     */
    public int getCcNumber() {
        return ccNumber;
    }

    /**
     * method to get the name
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * method to get the address
     *
     * @return address
     */
    public String getAddress() {
        return address;
    }

    /**
     * method to get the sex
     *
     * @return sex
     */
    public String getSex() {
        return sex;
    }

    /**
     * method to get the birthDate
     *
     * @return birthDate
     */
    public Date getBirthDate() {
        return birthDate;
    }

    /**
     * method to get the phoneNumber
     *
     * @return phoneNumber
     */
    public int getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * method to get the email
     *
     * @return email
     */
    public String getEmail() {
        return email;
    }

    /**
     * method to get the snsNumber
     *
     * @return snsNumber
     */
    public int getSnsNumber() {
        return snsNumber;
    }

    /**
     * method to get the password
     *
     * @return password
     */
    public String getPassword() {
        return password;
    }

    /**
     * method to get the schedules
     *
     * @return schedules
     */
    public ArrayList<VaccineSchedule> getSchedules() {
        return schedules;
    }

    /**
     * method to get the completedList
     *
     * @return completedList
     */
    public ArrayList<VaccineSchedule> getCompletedList() {
        return completedList;
    }

    /**
     * method to ser the password value
     *
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * method to ser the schedulesList value
     *
     * @param schedules
     */
    public void setSchedules(ArrayList<VaccineSchedule> schedules) {
        this.schedules = schedules;
    }

    /**
     * method to ser the completed schedules list value
     *
     * @param completedList
     */
    public void setCompletedList(ArrayList<VaccineSchedule> completedList) {
        this.completedList = completedList;
    }

    /**
     * method to get the today schedules of the sns user
     *
     * @return todaySchedules
     */
    public ArrayList<VaccineSchedule> getTodaySchedules() {
        ArrayList<VaccineSchedule> schedulesList = new ArrayList<>();
        for (VaccineSchedule schedule : schedules) {
            if (schedule.getScheduleDate().getDate() == Calendar.getInstance().getTime().getDate() && schedule.getArrivalTime() != null) {
                schedulesList.add(schedule);
            }
        }
        return schedulesList;
    }

    /**
     * to string method
     *
     * @return formated string
     */
    @Override
    public String toString() {
        return "Client{" +
                "name='" + name + '\'' +
                ", snsNumber=" + snsNumber +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return ccNumber == client.ccNumber && phoneNumber == client.phoneNumber && snsNumber == client.snsNumber && email.equals(client.email) && password.equals(client.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ccNumber, phoneNumber, email, snsNumber, password);
    }


}