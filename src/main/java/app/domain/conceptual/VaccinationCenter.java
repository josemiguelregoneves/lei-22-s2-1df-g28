package app.domain.conceptual;

import app.domain.shared.Validator;

import java.io.Serializable;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;
import java.util.zip.DataFormatException;

public class VaccinationCenter implements Serializable {
  /**
   * name
   */
  private String name;

  /**
   * address
   */
  private String address;

  /**
   * phoneNumber
   */
  private int phoneNumber;

  /**
   * email
   */
  private String email;

  /**
   * faxNumber
   */
  private int faxNumber;

  /**
   * website
   */
  private String website;

  /**
   * slotDuration
   */
  private Time slotDuration;

  /**
   * maxVaccines
   */
  private int maxVaccines;

  /**
   * openingHour
   */
  private Time openingHour;

  /**
   * closingHour
   */
  private Time closingHour;

  /**
   * idType
   */
  private String idType;

  /**
   * completed schedules
   */
  private int completedSchedules;

  /**
   * schedules list
   */
  private ArrayList<VaccineSchedule> schedulesList;

  /**
   * schedules waiting room
   */
  private ArrayList<VaccineSchedule> schedulesWaitingRoom;

  /**
   * completed schedules list
   */
  private ArrayList<VaccineSchedule> completedSchedulesList;

  /**
   * full vaccinated list
   */
  private ArrayList<VaccineSchedule> fullVaccinatedList;

  /**
   * constructor with parameters
   *
   * @param name
   * @param address
   * @param phoneNumber
   * @param email
   * @param faxNumber
   * @param website
   * @param slotDuration
   * @param maxVaccines
   * @param openingHour
   * @param closingHour
   * @param idType
   */
  public VaccinationCenter(String name, String address, int phoneNumber, String email, int faxNumber, String website,
      Time slotDuration, int maxVaccines, Time openingHour, Time closingHour, String idType) {
    this.name = name;
    this.address = address;
    this.phoneNumber = phoneNumber;
    this.email = email;
    this.faxNumber = faxNumber;
    this.website = website;
    this.slotDuration = slotDuration;
    this.maxVaccines = maxVaccines;
    this.openingHour = openingHour;
    this.closingHour = closingHour;
    this.idType = idType;
    this.schedulesList = new ArrayList<>();
    this.schedulesWaitingRoom = new ArrayList<>();
    this.completedSchedulesList = new ArrayList<>();
    this.fullVaccinatedList = new ArrayList<>();
    this.completedSchedules = 0;
  }

  /**
   * method to get the name
   *
   * @return name
   */
  public String getName() {
    return name;
  }

  /**
   * method to set the name
   *
   * @param name
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * method to get the address
   *
   * @return address
   */
  public String getAddress() {
    return address;
  }

  /**
   * method to set the address
   *
   * @param address
   */
  public void setAddress(String address) {
    this.address = address;
  }

  /**
   * method to get the phoneNumber
   *
   * @return phoneNumber
   */
  public int getPhoneNumber() {
    return phoneNumber;
  }

  /**
   * method to set the phoneNumber
   *
   * @param phoneNumber
   */
  public void setPhoneNumber(int phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  /**
   * method to get the email
   *
   * @return email
   */
  public String getEmail() {
    return email;
  }

  /**
   * method to set the email
   *
   * @param email
   */
  public void setEmail(String email) {
    this.email = email;
  }

  /**
   * method to get the faxNumber
   *
   * @return faxNumber
   */
  public int getFaxNumber() {
    return faxNumber;
  }

  /**
   * method to set the faxNumber
   *
   * @param faxNumber
   */
  public void setFaxNumber(int faxNumber) {
    this.faxNumber = faxNumber;
  }

  /**
   * method to get the website
   *
   * @return website
   */
  public String getWebsite() {
    return website;
  }

  /**
   * method to set the website
   *
   * @param website
   */
  public void setWebsite(String website) {
    this.website = website;
  }

  /**
   * method to get the slotDuration
   *
   * @return slotDuration
   */
  public Time getSlotDuration() {
    return slotDuration;
  }

  /**
   * method to set the slotDuration
   *
   * @param slotDuration
   */
  public void setSlotDuration(Time slotDuration) {
    this.slotDuration = slotDuration;
  }

  /**
   * method to get the maxVaccines
   *
   * @return maxVaccines
   */
  public int getMaxVaccines() {
    return maxVaccines;
  }

  /**
   * method to set the maxVaccines
   *
   * @param maxVaccines
   */
  public void setMaxVaccines(int maxVaccines) {
    this.maxVaccines = maxVaccines;
  }

  /**
   * method to get the openingHour
   *
   * @return openingHour
   */
  public Time getOpeningHour() {
    return openingHour;
  }

  /**
   * method to set the openingHour
   *
   * @param openingHour
   */
  public void setOpeningHour(Time openingHour) {
    this.openingHour = openingHour;
  }

  /**
   * method to get the closingHour
   *
   * @return closingHour
   */
  public Time getClosingHour() {
    return closingHour;
  }

  /**
   * method to set the closingHour
   *
   * @param closingHour
   */
  public void setClosingHour(Time closingHour) {
    this.closingHour = closingHour;
  }

  /**
   * method to get the idType
   *
   * @return idType
   */
  public String getIdType() {
    return idType;
  }

  /**
   * method to set the idType
   *
   * @param idType
   */
  public void setIdType(String idType) {
    this.idType = idType;
  }

  /**
   * method to get the completedSchedules
   *
   * @return completedSchedules
   */
  public int getCompletedSchedules() {
    return completedSchedules;
  }

  /**
   * method to set the completedSchedules
   *
   * @param completedSchedules
   */
  public void setCompletedSchedules(int completedSchedules) {
    this.completedSchedules = completedSchedules;
  }

  /**
   * method to get the schedulesList
   *
   * @return schedulesList
   */
  public ArrayList<VaccineSchedule> getSchedulesList() {
    return schedulesList;
  }

  /**
   * method to set the schedulesList
   *
   * @param schedulesList
   */
  public void setSchedulesList(ArrayList<VaccineSchedule> schedulesList) {
    this.schedulesList = schedulesList;
  }

  /**
   * method to get the schedulesWaitingRoom
   *
   * @return schedulesWaitingRoom
   */
  public ArrayList<VaccineSchedule> getSchedulesWaitingRoom() {
    return schedulesWaitingRoom;
  }

  /**
   * method to set the schedulesWaitingRoom
   *
   * @param schedulesWaitingRoom
   */
  public void setSchedulesWaitingRoom(ArrayList<VaccineSchedule> schedulesWaitingRoom) {
    this.schedulesWaitingRoom = schedulesWaitingRoom;
  }

  /**
   * method to get the completedSchedulesList
   *
   * @return completedSchedulesList
   */
  public ArrayList<VaccineSchedule> getCompletedSchedulesList() {
    return completedSchedulesList;
  }

  /**
   * method to set the completedSchedulesList
   *
   * @param completedSchedulesList
   */
  public void setCompletedSchedulesList(ArrayList<VaccineSchedule> completedSchedulesList) {
    this.completedSchedulesList = completedSchedulesList;
  }

  /**
   * method to get the fullVaccinatedList
   *
   * @return fullVaccinatedList
   */
  public ArrayList<VaccineSchedule> getFullVaccinatedList() {
    return fullVaccinatedList;
  }

  /**
   * method to set the fullVaccinatedList
   *
   * @param fullVaccinatedList
   */
  public void setFullVaccinatedList(ArrayList<VaccineSchedule> fullVaccinatedList) {
    this.fullVaccinatedList = fullVaccinatedList;
  }

  /**
   * method to get the full vaccinated by date
   *
   * @param date
   *
   * @return full completed schedules
   */
  public ArrayList<VaccineSchedule> getFullVaccinatedByDate(Date date) {
    ArrayList<VaccineSchedule> fulLVaccinated = new ArrayList<>();
    for (VaccineSchedule schedule : fullVaccinatedList) {
      if (schedule.getScheduleDate().equals(date)) {
        fulLVaccinated.add(schedule);
      }
    }
    return fulLVaccinated;
  }

  /**
   * method to increment a completed schedule
   */
  public void addCompletedSchedules() {
    this.completedSchedules++;
  }

  /**
   * reset the schedules variable
   */
  public void resetCompletedSchedules() {
    this.completedSchedules = 0;
  }

  /**
   * method to get the schedules in waiting state, for a specific client
   *
   * @param client
   *
   * @return schedulesWaiting
   */
  public ArrayList<VaccineSchedule> getSchedulesWaitingForClient(Client client) {
    ArrayList<VaccineSchedule> schedules = new ArrayList<>();
    for (VaccineSchedule schedule : schedulesWaitingRoom) {
      if (Validator.getClientBySnsNumber(schedule.getSnsNumber()).equals(client)) {
        schedules.add(schedule);
      }
    }
    return schedules;
  }

  /**
   * method to get the clients in the waiting room
   *
   * @return clientsList
   */
  public ArrayList<Client> getClientsWaitingRoom() {
    ArrayList<Client> clientList = new ArrayList<>();
    for (VaccineSchedule schedule : schedulesWaitingRoom) {
      Client c = Validator.getClientBySnsNumber(schedule.getSnsNumber());
      if (!clientList.contains(c)) {
        clientList.add(c);
      }
    }
    return clientList;
  }

  /**
   * toString method
   *
   * @return string
   */
  @Override
  public String toString() {
    return "VaccinationCenter{" +
            "name='" + name + '\'' +
            ", address='" + address + '\'' +
            ", phoneNumber=" + phoneNumber +
            ", email='" + email + '\'' +
            ", faxNumber=" + faxNumber +
            ", website='" + website + '\'' +
            ", slotDuration=" + slotDuration +
            ", maxVaccines=" + maxVaccines +
            ", openingHour=" + openingHour +
            ", closingHour=" + closingHour +
            ", idType='" + idType + '\'' +
            ", completedSchedules=" + completedSchedules +
            ", schedulesList=" + schedulesList +
            ", schedulesWaitingRoom=" + schedulesWaitingRoom +
            ", completedSchedulesList=" + completedSchedulesList +
            '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    VaccinationCenter that = (VaccinationCenter) o;
    return Objects.equals(name, that.name) && Objects.equals(address, that.address);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, address);
  }
}
