package app.domain.conceptual;

import java.io.Serializable;
import java.util.Objects;

public class Vaccine implements Serializable {

    /**
     * vaccine type
     */
    private final VaccineType type;

    /**
     * administration process
     */
    private final AdministrationProcess administration;

    /**
     * name
     */
    private final String name;

    /**
     * constructor with parameters
     *
     * @param type
     * @param administration
     * @param name
     */
    public Vaccine(VaccineType type, AdministrationProcess administration, String name) {
        this.type = type;
        this.administration = administration;
        this.name = name;
    }

    /**
     * method to get the vaccine type
     *
     * @return vcType
     */
    public VaccineType getType() {
        return type;
    }

    /**
     * method to get the vaccine administration
     *
     * @return administration
     */
    public AdministrationProcess getAdministration() {
        return administration;
    }

    /**
     * method to get the vaccine name
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Vaccine vacine = (Vaccine) o;
        return Objects.equals(type, vacine.type) && Objects.equals(name, vacine.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, name);
    }

    @Override
    public String toString() {
        return "Vaccine{" +
                "type=" + type +
                ", administration=" + administration +
                ", name='" + name + '\'' +
                '}';
    }
}
