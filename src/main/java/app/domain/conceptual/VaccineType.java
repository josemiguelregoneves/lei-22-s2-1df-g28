package app.domain.conceptual;

import java.io.Serializable;
import java.util.Objects;

public class VaccineType implements Serializable {

    /**
     * name
     */
    private final String name;

    /**
     * constructor with parameters
     *
     * @param name
     */
    public VaccineType(String name) {
        this.name = name;
    }

    /**
     * method to get the name
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * to string method
     *
     * @return string
     */
    @Override
    public String toString() {
        return "VaccineType{" +
                "name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        VaccineType that = (VaccineType) o;
        return Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
