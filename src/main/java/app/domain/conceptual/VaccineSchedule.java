package app.domain.conceptual;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;
import java.util.Objects;

public class VaccineSchedule implements Serializable {

    /**
     * snsNumber
     */
    private int snsNumber;

    /**
     * vaccine type
     */
    private VaccineType vcType;

    /**
     * vaccine name
     */
    private String vc;

    /**
     * dose
     */
    private String dose;

    /**
     * lot number
     */
    private String lotNumber;

    /**
     * schedule date
     */
    private Date scheduleDate;

    /**
     * schedule time
     */
    private Time scheduleTime;

    /**
     * arrival time
     */
    private Time arrivalTime;

    /**
     * admin time
     */
    private Time adminTime;

    /**
     * leave time
     */
    private Time leaveTime;

    /**
     * constructor with parameters
     *
     * @param snsNumber
     * @param vcType
     * @param scheduleDate
     * @param scheduleTime
     */
    public VaccineSchedule(int snsNumber, VaccineType vcType, Date scheduleDate, Time scheduleTime) {
        this.snsNumber = snsNumber;
        this.vcType = vcType;
        this.scheduleDate = scheduleDate;
        this.scheduleTime = scheduleTime;
    }

    /**
     * constructor with parameters
     *
     * @param snsNumber
     * @param vcType
     * @param scheduleDate
     * @param scheduleTime
     * @param arrivalTime
     */
    public VaccineSchedule(int snsNumber, VaccineType vcType, Date scheduleDate, Time scheduleTime, Time arrivalTime) {
        this.snsNumber = snsNumber;
        this.vcType = vcType;
        this.scheduleDate = scheduleDate;
        this.scheduleTime = scheduleTime;
        this.arrivalTime = arrivalTime;
    }

    /**
     * constructor with parameters
     *
     * @param snsNumber
     * @param vc
     * @param dose
     * @param lotNumber
     * @param scheduleDate
     * @param scheduleTime
     * @param arrivalTime
     * @param adminTime
     * @param leaveTime
     */
    public VaccineSchedule(int snsNumber, String vc, String dose, String lotNumber, Date scheduleDate, Time scheduleTime, Time arrivalTime, Time adminTime, Time leaveTime) {
        this.snsNumber = snsNumber;
        this.vc = vc;
        this.dose = dose;
        this.lotNumber = lotNumber;
        this.scheduleDate = scheduleDate;
        this.scheduleTime = scheduleTime;
        this.arrivalTime = arrivalTime;
        this.adminTime = adminTime;
        this.leaveTime = leaveTime;
    }

    /**
     * method to get the snsNumber
     *
     * @return snsNumber
     */
    public int getSnsNumber() {
        return snsNumber;
    }

    /**
     * method to get the vcType
     *
     * @return vcType
     */
    public VaccineType getVcType() {
        return vcType;
    }

    /**
     * method to get the vc
     *
     * @return vc
     */
    public String getVc() {
        return vc;
    }

    /**
     * method to get the dose
     *
     * @return dose
     */
    public String getDose() {
        return dose;
    }

    /**
     * method to get the lotNumber
     *
     * @return lotNumber
     */
    public String getLotNumber() {
        return lotNumber;
    }

    /**
     * method to get the scheduleDate
     *
     * @return scheduleDate
     */
    public Date getScheduleDate() {
        return scheduleDate;
    }

    /**
     * method to get the scheduleTime
     *
     * @return scheduleTime
     */
    public Time getScheduleTime() {
        return scheduleTime;
    }

    /**
     * method to get the arrivalTime
     *
     * @return arrivalTime
     */
    public Time getArrivalTime() {
        return arrivalTime;
    }

    /**
     * method to get the adminTime
     *
     * @return adminTime
     */
    public Time getAdminTime() {
        return adminTime;
    }

    /**
     * method to get the leaveTime
     *
     * @return leaveTime
     */
    public Time getLeaveTime() {
        return leaveTime;
    }

    /**
     * method to set the snsNumber value
     *
     * @param snsNumber
     */
    public void setSnsNumber(int snsNumber) {
        this.snsNumber = snsNumber;
    }

    /**
     * method to set the vcType value
     *
     * @param vcType
     */
    public void setVcType(VaccineType vcType) {
        this.vcType = vcType;
    }

    /**
     * method to set the vc value
     *
     * @param vc
     */
    public void setVc(String vc) {
        this.vc = vc;
    }

    /**
     * method to set the dose value
     *
     * @param dose
     */
    public void setDose(String dose) {
        this.dose = dose;
    }

    /**
     * method to set the lotNumber value
     *
     * @param lotNumber
     */
    public void setLotNumber(String lotNumber) {
        this.lotNumber = lotNumber;
    }

    /**
     * method to set the scheduleDate value
     *
     * @param scheduleDate
     */
    public void setScheduleDate(Date scheduleDate) {
        this.scheduleDate = scheduleDate;
    }

    /**
     * method to set the scheduleTime value
     *
     * @param scheduleTime
     */
    public void setScheduleTime(Time scheduleTime) {
        this.scheduleTime = scheduleTime;
    }

    /**
     * method to set the arrivalTime value
     *
     * @param arrivalTime
     */
    public void setArrivalTime(Time arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    /**
     * method to set the adminTime value
     *
     * @param adminTime
     */
    public void setAdminTime(Time adminTime) {
        this.adminTime = adminTime;
    }

    /**
     * method to set the leaveTime value
     *
     * @param leaveTime
     */
    public void setLeaveTime(Time leaveTime) {
        this.leaveTime = leaveTime;
    }

    /**
     * method to return a formated string with all atributtes
     *
     * @return formated String
     */
    @Override
    public String toString() {
        return "VaccineSchedule{" +
                "snsNumber=" + snsNumber +
                ", vcType=" + vcType +
                ", vc=" + vc +
                ", dose='" + dose + '\'' +
                ", lotNumber='" + lotNumber + '\'' +
                ", scheduleDate=" + scheduleDate +
                ", scheduleTime=" + scheduleTime +
                ", arrivalTime=" + arrivalTime +
                ", adminTime=" + adminTime +
                ", leaveTime=" + leaveTime +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VaccineSchedule that = (VaccineSchedule) o;
        return snsNumber == that.snsNumber && Objects.equals(vcType, that.vcType) && Objects.equals(vc, that.vc) && Objects.equals(dose, that.dose) && Objects.equals(lotNumber, that.lotNumber) && Objects.equals(scheduleDate, that.scheduleDate) && Objects.equals(scheduleTime, that.scheduleTime) && Objects.equals(arrivalTime, that.arrivalTime) && Objects.equals(adminTime, that.adminTime) && Objects.equals(leaveTime, that.leaveTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(snsNumber, vcType, vc, dose, lotNumber, scheduleDate, scheduleTime, arrivalTime, adminTime, leaveTime);
    }
}
