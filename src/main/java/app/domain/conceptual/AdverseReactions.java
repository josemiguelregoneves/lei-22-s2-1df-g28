package app.domain.conceptual;

import java.io.Serializable;

public class AdverseReactions implements Serializable {

    /**
     * client
     */
    private Client client;

    /**
     * reaction description
     */
    private String reaction;

    /**
     * constructor with parameters
     *
     * @param client
     * @param reaction
     */
    public AdverseReactions(Client client, String reaction) {
        this.client = client;
        this.reaction = reaction;
    }

    /**
     * method to get the client
     *
     * @return client
     */
    public Client getClient() {
        return client;
    }

    /**
     * method to set the client
     *
     * @param client
     */
    public void setClient(Client client) {
        this.client = client;
    }

    /**
     * method to get the reaction
     *
     * @return reaction
     */
    public String getReaction() {
        return reaction;
    }

    /**
     * method to set the reaction
     *
     * @param reaction
     */
    public void setReaction(String reaction) {
        this.reaction = reaction;
    }

    /**
     * to string method
     *
     * @return formated string
     */
    @Override
    public String toString() {
        return "AdverseReactions{" +
                "client=" + client +
                ", reaction='" + reaction + '\'' +
                '}';
    }
}
