package app.domain.tests;

import app.controller.us015.FullyVaccinatedUsersController;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;

import static org.junit.Assert.*;

public class FullyVaccinatedUsersControllerTest {

    /**
     * test the method to get the days between to another dates
     */
    @Test
    public void datesBetweenInterval() {

        FullyVaccinatedUsersController ctrl = new FullyVaccinatedUsersController();

        Date date1 = new Date(2020, 10, 10);
        Date date2 = new Date(2020, 10, 11);
        Date date3 = new Date(2020, 10, 12);
        Date date4 = new Date(2020, 10, 13);
        Date date5 = new Date(2020, 10, 14);

        ArrayList<Date> expected = new ArrayList<>();
        expected.add(date1);
        expected.add(date2);
        expected.add(date3);
        expected.add(date4);
        expected.add(date5);

        ArrayList<Date> actual = ctrl.datesBetweenInterval(date1, date5);

        Assert.assertEquals(expected, actual);
    }
}