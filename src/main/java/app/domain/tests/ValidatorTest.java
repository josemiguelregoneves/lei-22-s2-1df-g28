package app.domain.tests;

import app.domain.conceptual.*;
import app.domain.model.Company;
import app.domain.shared.Constants;
import app.domain.shared.Validator;
import app.domain.store.ClientStore;
import app.domain.store.VaccineScheduleStore;
import app.domain.store.VaccineStore;
import org.junit.Assert;
import org.junit.Test;

import java.sql.Time;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.junit.Assert.*;

public class ValidatorTest {

    private static final int PHONE_NUMBER = 123456789;

    private static final int FAX_NUMBER = 123456789;

    private static final int SNS_NUMBER = 123456789;

    private static final int CC_NUMBER = 12345678;

    private static final String EMAIL = "email@gmail.com";

    private static final String WEBSITE = "website.pt";

    private static final String STR_TIME = "14:30:30";
    private static final Time TIME = new Time(14, 30, 30);

    private static final String STR_OP_HOUR = "10:30:30";
    private static final Time OPENING_HOUR = new Time(10, 30, 30);

    private static final String STR_CL_HOUR = "20:30:30";
    private static final Time CLOSING_HOUR = new Time(20, 30, 30);

    private static VaccinationCenter CENTER = new VaccinationCenter("C1", "A1", 123456789, "c1@gmail.com", 123456789, "web1.pt", new Time(1, 0, 0), 100, OPENING_HOUR, CLOSING_HOUR, Constants.TYPE_HEALTH_CARE_CENTER);

    /**
     * test the method to validate the phone number
     */
    @Test
    public void validatePhoneNumber() {
        boolean expected = true;
        boolean actual = Validator.validatePhoneNumber(PHONE_NUMBER);
        Assert.assertEquals(expected, actual);
    }

    /**
     * test the method to validate the fax number
     */
    @Test
    public void validateFaxNumber() {
        boolean expected = true;
        boolean actual = Validator.validateFaxNumber(FAX_NUMBER);
        Assert.assertEquals(expected, actual);
    }

    /**
     * test the method to validate the sns number
     */
    @Test
    public void validateSnsNumber() {
        boolean expected = true;
        boolean actual = Validator.validateSnsNumber(SNS_NUMBER);
        Assert.assertEquals(expected, actual);
    }

    /**
     * test the method to validate the cc number
     */
    @Test
    public void validateCcNumber() {
        boolean expected = true;
        boolean actual = Validator.validateCcNumber(CC_NUMBER);
        Assert.assertEquals(expected, actual);
    }

    /**
     * test the method to validate the email
     */
    @Test
    public void validateEmail() {
        boolean expected = true;
        boolean actual = Validator.validateEmail(EMAIL);
        Assert.assertEquals(expected, actual);
    }

    /**
     * test the method to validate the website
     */
    @Test
    public void validateWebsite() {
        boolean expected = true;
        boolean actual = Validator.validateWebsite(WEBSITE);
        Assert.assertEquals(expected, actual);
    }

    /**
     * test the method to validate the time
     */
    @Test
    public void validateTime() {
        boolean expected = true;
        boolean actual = Validator.validateTime(STR_TIME);
        Assert.assertEquals(expected, actual);
    }

    /**
     * test the method to validate the time schedule
     */
    @Test
    public void validateTimeSchedule() {
        boolean expected = true;
        boolean actual = Validator.validateTimeSchedule(TIME, CENTER);
        Assert.assertEquals(expected, actual);
    }

    /**
     * test the method to validate the closing hour
     */
    @Test
    public void validateClosingHour() {
        boolean expected = true;
        boolean actual = Validator.validateClosingHour(STR_OP_HOUR, STR_CL_HOUR);
        Assert.assertEquals(expected, actual);
    }

    /**
     * test the method to validate the opening hour
     */
    @Test
    public void validateOpeningHour() {
        boolean expected = true;
        boolean actual = Validator.validateOpeningHour(STR_OP_HOUR);
        Assert.assertEquals(expected, actual);
    }

    /**
     * test the method to validate the time since the last vaccine
     */
    @Test
    public void validateTimeSinceLastVaccine() {
        Client c = new Client(CC_NUMBER, "Name1", "Address1", Constants.SEX_MALE, new Date(2002, 10, 10), PHONE_NUMBER, EMAIL, SNS_NUMBER, "newPass");
        ClientStore cStore = new ClientStore();
        cStore.getClientList().add(c);
        Company.setClientStore(cStore);

        VaccineSchedule schedule = new VaccineSchedule(SNS_NUMBER, "Salva-3", "Primeira", Constants.VACCINE_LOT, new Date(2020, 10, 10), new Time(17,0,0), new Time(16,0,0), new Time(18,0,0), new Time(18,30,0));
        VaccineScheduleStore schedulesStore = new VaccineScheduleStore();
        schedulesStore.getCompletedSchedulesList().add(schedule);
        Company.setScheduleStore(schedulesStore);

        c.getCompletedList().add(schedule);

        VaccineSchedule newSchedule = new VaccineSchedule(SNS_NUMBER, "Salva-3", "Primeira", Constants.VACCINE_LOT, new Date(2022, 10, 10), new Time(17,0,0), new Time(16,0,0), new Time(18,0,0), new Time(18,30,0));

        boolean expected = true;
        boolean actual = Validator.validateTimeSinceLastVaccine(newSchedule);
        Assert.assertEquals(expected, actual);
    }

    /**
     * test the method to convert the time into seconds
     */
    @Test
    public void convertTimeSeconds() {
        Time time = new Time(1,0,0);
        int expected = 3600;
        int actual = Validator.convertTimeSeconds(time);
        Assert.assertEquals(expected, actual);
    }

    /**
     * test the method to get the client giving an sns number
     */
    @Test
    public void getClientBySnsNumber() {
        Client expected = new Client(CC_NUMBER, "Name1", "Address1", Constants.SEX_MALE, new Date(2002, 10, 10), PHONE_NUMBER, EMAIL, SNS_NUMBER, "newPass");
        ClientStore cStore = new ClientStore();
        cStore.getClientList().add(expected);
        Company.setClientStore(cStore);
        Client actual = Validator.getClientBySnsNumber(SNS_NUMBER);
        Assert.assertEquals(expected, actual);
    }

    /**
     * test the method to get the current hour
     */
    @Test
    public void getCurrentHour() {
        Calendar currentDate = Calendar.getInstance();
        int hour = currentDate.get(Calendar.HOUR_OF_DAY);
        int minute = currentDate.get(Calendar.MINUTE);
        int second = currentDate.get(Calendar.SECOND);
        String strTime = String.format("%d:%d:%d", hour, minute, second);
        Time expected = Time.valueOf(strTime);
        Time actual = Validator.getCurrentHour();
        Assert.assertEquals(expected, actual);
    }

    /**
     * test the method to validate if exists any client with a specific sns number
     */
    @Test
    public void existsClientWithSnsNumber() {
        Client c = new Client(CC_NUMBER, "Name1", "Address1", Constants.SEX_MALE, new Date(2002, 10, 10), PHONE_NUMBER, EMAIL, SNS_NUMBER, "newPass");
        ClientStore cStore = new ClientStore();
        cStore.getClientList().add(c);
        Company.setClientStore(cStore);
        boolean expected = true;
        boolean actual = Validator.existsClientWithSnsNumber(SNS_NUMBER);
        Assert.assertEquals(expected, actual);
    }

    /**
     * test the method to verify if exists any vaccine with specific name
     */
    @Test
    public void existsVaccineName() {
        Vaccine vc = new Vaccine(new VaccineType("Covid"), new AdministrationProcess(new AgeRange(10, 100),10,2), "Salva-1");
        VaccineStore vcStore = new VaccineStore();
        vcStore.getVaccineList().add(vc);
        Company.setVcStore(vcStore);
        boolean expected = true;
        boolean actual = Validator.existsVaccineName("Salva-1");
        Assert.assertEquals(expected, actual);
    }

    /**
     * test the method to get the vacccine giving the name
     */
    @Test
    public void getVaccineByName() {
        Vaccine expected = new Vaccine(new VaccineType("Covid"), new AdministrationProcess(new AgeRange(10, 100),10,2), "Salva-1");
        VaccineStore vcStore = new VaccineStore();
        vcStore.getVaccineList().add(expected);
        Company.setVcStore(vcStore);
        Vaccine actual = Validator.getVaccineByName("Salva-1");
        Assert.assertEquals(expected, actual);
    }

    /**
     * test the method to convert the dose in string into integer
     */
    @Test
    public void convertStringDoseToInt() {
        int expected = 1;
        int actual = Validator.convertStringDoseToInt("Primeira");
        Assert.assertEquals(expected, actual);
    }

    /**
     * test the method to get the hour of a specific date
     */
    @Test
    public void getHourOfDate() {
        GregorianCalendar gc = new GregorianCalendar(2022,10,10,15,0,0);
        Time expected = new Time(15,0,0);
        Time actual = Validator.getHourOfDate(gc);
        Assert.assertEquals(expected, actual);
    }

    /**
     * test the method to convert a formated string to date
     */
    @Test
    public void convertStringToDate() {
        String strDate = "12/10/2020";
        Date expected = new Date(2020,10,12);
        Date actual = Validator.convertStringToDate(strDate);
        Assert.assertEquals(expected, actual);
    }

    /**
     * test the method to convert a date to string
     */
    @Test
    public void convertDateToString() {
        String expected = "12/10/2020";
        Date date = new Date(2020,10,12);
        String actual = Validator.convertDateToString(date);
        Assert.assertEquals(expected, actual);
    }

    /**
     * test the method to verify if the client has any vaccine schedule complete
     */
    @Test
    public void clientVaccinated() {
        Client c = new Client(CC_NUMBER, "Name1", "Address1", Constants.SEX_MALE, new Date(2002, 10, 10), PHONE_NUMBER, EMAIL, SNS_NUMBER, "newPass");
        ClientStore cStore = new ClientStore();
        cStore.getClientList().add(c);
        Company.setClientStore(cStore);
        VaccineSchedule schedule = new VaccineSchedule(SNS_NUMBER, "Salva-3", "Primeira", Constants.VACCINE_LOT, new Date(2020, 10, 10), new Time(17,0,0), new Time(16,0,0), new Time(18,0,0), new Time(18,30,0));
        VaccineScheduleStore schedulesStore = new VaccineScheduleStore();
        schedulesStore.getCompletedSchedulesList().add(schedule);
        Company.setScheduleStore(schedulesStore);
        boolean expected = true;
        boolean actual = Validator.clientVaccinated(SNS_NUMBER);
        Assert.assertEquals(expected, actual);
    }

    /**
     * test the method to convert the time into seconds
     */
    @Test
    public void convertTimeToSeconds() {
        int expected = 3600;
        Time time = new Time(1,0,0);
        int actual = Validator.convertTimeToSeconds(time);
        Assert.assertEquals(expected, actual);
    }

    /**
     * test the method to convert the time to seconds
     */
    @Test
    public void convertSecondsToTime() {
        int seconds = 3600;
        Time expected = new Time(1,0,0);
        Time actual = Validator.convertSecondsToTime(seconds);
        Assert.assertEquals(expected, actual);
    }

    /**
     * test the method to convert an integer dose to string
     */
    @Test
    public void convertIntDoseToString() {
        int dose = 1;
        String expected = "Primeira";
        String actual = Validator.convertIntDoseToString(dose);
        Assert.assertEquals(expected, actual);
    }

    /**
     * test the method to get the number of available doses
     */
    @Test
    public void getNumberOfDoses() {
        Client c = new Client(CC_NUMBER, "Name1", "Address1", Constants.SEX_MALE, new Date(2002, 10, 10), PHONE_NUMBER, EMAIL, SNS_NUMBER, "newPass");
        ClientStore cStore = new ClientStore();
        cStore.getClientList().add(c);
        Company.setClientStore(cStore);

        VaccineSchedule schedule = new VaccineSchedule(SNS_NUMBER, "Salva-3", "Primeira", Constants.VACCINE_LOT, new Date(2020, 10, 10), new Time(17,0,0), new Time(16,0,0), new Time(18,0,0), new Time(18,30,0));
        VaccineScheduleStore schedulesStore = new VaccineScheduleStore();
        schedulesStore.getCompletedSchedulesList().add(schedule);
        Company.setScheduleStore(schedulesStore);

        c.getCompletedList().add(schedule);

        int expected = 1;
        int actual = Validator.getNumberOfDoses(schedule);
        Assert.assertEquals(expected, actual);
    }
}