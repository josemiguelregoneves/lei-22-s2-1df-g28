package app.domain.tests;

import app.domain.conceptual.*;
import app.domain.model.Company;
import app.domain.shared.Constants;
import app.domain.store.ClientStore;
import app.domain.store.VaccineScheduleStore;
import app.domain.store.VaccineStore;
import org.junit.Assert;
import org.junit.Test;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.*;

public class VaccineStoreTest {

    /**
     * test the method of get available vaccines of a specific type
     */
    @Test
    public void getVaccinesByType() {
        final VaccineType TYPE1 = new VaccineType("Covid");
        final Vaccine VC1 = new Vaccine(TYPE1, new AdministrationProcess(new AgeRange(10, 100),10,2), "Salva-1");

        final VaccineType TYPE2 = new VaccineType("Sida");
        final Vaccine VC2 = new Vaccine(TYPE2, new AdministrationProcess(new AgeRange(10, 100),10,2), "Salva-2");


        final int SNS_NUMBER = 111111111;

        VaccineStore vcStore = new VaccineStore();
        vcStore.getVaccineList().add(VC1);
        vcStore.getVaccineList().add(VC2);

        Client c = new Client(11111111, "Name1", "Address1", Constants.SEX_MALE, new Date(2002, 10, 10), 111111111, "name1@gmail.com", SNS_NUMBER, "newPass1");
        ClientStore cStore = new ClientStore();
        cStore.getClientList().add(c);
        Company.setClientStore(cStore);

        VaccineSchedule schedule = new VaccineSchedule(SNS_NUMBER, TYPE1, Calendar.getInstance().getTime(), new Time(18,0,0), new Time(19,0,0));
        VaccineScheduleStore schedulesStore = new VaccineScheduleStore();
        schedulesStore.getCompletedSchedulesList().add(schedule);
        Company.setScheduleStore(schedulesStore);

        ArrayList<Vaccine> expected = new ArrayList<>();
        expected.add(VC1);

        ArrayList<Vaccine> actual = vcStore.getVaccinesByType(schedule);
        Assert.assertEquals(expected, actual);
    }
}