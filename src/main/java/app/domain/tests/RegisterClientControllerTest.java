package app.domain.tests;

import app.controller.register.RegisterClientController;
import app.domain.conceptual.Client;
import app.domain.dto.ClientDTO;
import app.domain.mapper.ClientMapper;
import app.domain.model.Company;
import app.domain.shared.Constants;
import app.domain.store.ClientStore;
import org.junit.Assert;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.*;

public class RegisterClientControllerTest {

    private static RegisterClientController clientCtrl = new RegisterClientController();
    private static ClientDTO CLIENT_DTO;
    private static ClientMapper CLIENT_MAPPER;
    private static ClientStore CLIENT_STORE;

    /**
     * test the method of create a new client
     */
    @Test
    public void createClient() {
        getData();
        Client expected = CLIENT_MAPPER.toModel(CLIENT_DTO);
        Client actual = clientCtrl.createClient(CLIENT_DTO);
        Assert.assertEquals(expected.getName(), actual.getName());
        Assert.assertEquals(expected.getAddress(), actual.getAddress());
        Assert.assertEquals(expected.getSnsNumber(), actual.getSnsNumber());
        Assert.assertEquals(expected.getEmail(), actual.getEmail());
        Assert.assertEquals(expected.getCcNumber(), actual.getCcNumber());
        Assert.assertEquals(expected.getSex(), actual.getSex());
        Assert.assertEquals(expected.getBirthDate(), actual.getBirthDate());
    }

    /**
     * method to get the data
     */
    private void getData() {

        CLIENT_DTO = new ClientDTO(11111111, "u1", "a1", Constants.SEX_MALE, new Date(2002, 01, 01), 111111111, "u1@gmail.com", 111111111);
        CLIENT_MAPPER = new ClientMapper();
        CLIENT_STORE = new ClientStore();

        Company.setSnsUserMapper(CLIENT_MAPPER);
        Company.setClientStore(CLIENT_STORE);
    }
}