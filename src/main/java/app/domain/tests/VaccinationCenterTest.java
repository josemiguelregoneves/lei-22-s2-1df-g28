package app.domain.tests;

import app.domain.conceptual.Client;
import app.domain.conceptual.VaccinationCenter;
import app.domain.conceptual.VaccineSchedule;
import app.domain.conceptual.VaccineType;
import app.domain.model.Company;
import app.domain.shared.Constants;
import app.domain.store.ClientStore;
import app.domain.store.VaccineScheduleStore;
import org.junit.Assert;
import org.junit.Test;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.*;

public class VaccinationCenterTest {

    /**
     * test the method of get the clients in the waiting room
     */
    @Test
    public void getClientsWaitingRoom() {
        final String STR_OP_HOUR = "10:30:30";
        final Time OPENING_HOUR = new Time(10, 30, 30);
        final String STR_CL_HOUR = "20:30:30";
        final Time CLOSING_HOUR = new Time(20, 30, 30);
        VaccinationCenter center = new VaccinationCenter("C1", "A1", 123456789, "c1@gmail.com", 123456789, "web1.pt", new Time(1, 0, 0), 100, OPENING_HOUR, CLOSING_HOUR, Constants.TYPE_HEALTH_CARE_CENTER);

        final int SNS_NUMBER = 111111111;

        Client c = new Client(11111111, "Name1", "Address1", Constants.SEX_MALE, new Date(2002, 10, 10), 111111111, "name1@gmail.com", SNS_NUMBER, "newPass1");
        ClientStore cStore = new ClientStore();
        cStore.getClientList().add(c);
        Company.setClientStore(cStore);

        VaccineSchedule schedule = new VaccineSchedule(SNS_NUMBER, new VaccineType("Covid"), Calendar.getInstance().getTime(), new Time(18,0,0), new Time(19,0,0));
        VaccineScheduleStore schedulesStore = new VaccineScheduleStore();
        schedulesStore.getCompletedSchedulesList().add(schedule);
        Company.setScheduleStore(schedulesStore);

        center.getSchedulesWaitingRoom().add(schedule);

        ArrayList<Client> expected = new ArrayList<>();
        expected.add(c);

        ArrayList<Client> actual = center.getClientsWaitingRoom();

        Assert.assertEquals(expected, actual);
    }
}