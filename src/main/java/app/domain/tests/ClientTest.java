package app.domain.tests;

import app.domain.conceptual.Client;
import app.domain.conceptual.VaccineSchedule;
import app.domain.conceptual.VaccineType;
import app.domain.model.Company;
import app.domain.shared.Constants;
import app.domain.store.ClientStore;
import app.domain.store.VaccineScheduleStore;
import org.junit.Assert;
import org.junit.Test;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.*;

public class ClientTest {

    /**
     * test the method to get the available schedules for today
     */
    @Test
    public void getTodaySchedules() {
        final int SNS_NUMBER = 111111111;

        Client c = new Client(11111111, "Name1", "Address1", Constants.SEX_MALE, new Date(2002, 10, 10), 111111111, "name1@gmail.com", SNS_NUMBER, "newPass1");
        ClientStore cStore = new ClientStore();
        cStore.getClientList().add(c);
        Company.setClientStore(cStore);

        VaccineSchedule schedule = new VaccineSchedule(SNS_NUMBER, new VaccineType("Covid"), Calendar.getInstance().getTime(), new Time(18,0,0), new Time(19,0,0));
        VaccineScheduleStore schedulesStore = new VaccineScheduleStore();
        schedulesStore.getCompletedSchedulesList().add(schedule);
        Company.setScheduleStore(schedulesStore);

        c.getSchedules().add(schedule);

        ArrayList<VaccineSchedule> expected = new ArrayList<>();
        expected.add(schedule);

        ArrayList<VaccineSchedule> actual = c.getTodaySchedules();

        Assert.assertEquals(expected, actual);
    }
}