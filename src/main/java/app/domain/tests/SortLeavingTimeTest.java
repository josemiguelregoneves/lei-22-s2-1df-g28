package app.domain.tests;

import app.domain.conceptual.*;
import app.domain.model.Company;
import app.domain.shared.Constants;
import app.domain.store.ClientStore;
import app.domain.store.VaccineScheduleStore;
import app.domain.us017.SortArrivalTime;
import org.junit.Assert;
import org.junit.Test;

import java.sql.Time;
import java.util.Date;

import static org.junit.Assert.*;

public class SortLeavingTimeTest {

    /**
     * test the method of sorting an array by ascending order, using bubble sort
     */
    @Test
    public void ascendingBubbleSort() {
        SortArrivalTime sort = new SortArrivalTime();

        Vaccine vc = new Vaccine(new VaccineType("Covid"), new AdministrationProcess(new AgeRange(10,100),10,1),"Salva-1");

        Client c = new Client(11111111, "Name1", "Address1", Constants.SEX_MALE, new Date(2002, 10, 10), 111111111, "name1@gmail.com", 111111111, "newPass1");
        ClientStore cStore = new ClientStore();
        cStore.getClientList().add(c);
        Company.setClientStore(cStore);

        VaccineSchedule completed1 = new VaccineSchedule(111111111, vc.getName(), "Primeira", Constants.VACCINE_LOT, new Date(2022,10,10), new Time(16,0,0), new Time(17,0,0), new Time(12,0,0), new Time(18,30,0));
        VaccineSchedule completed2 = new VaccineSchedule(111111111, vc.getName(), "Primeira", Constants.VACCINE_LOT, new Date(2022,10,10), new Time(16,0,0), new Time(17,0,0), new Time(15,0,0), new Time(18,30,0));
        VaccineSchedule completed3 = new VaccineSchedule(111111111, vc.getName(), "Primeira", Constants.VACCINE_LOT, new Date(2022,10,10), new Time(16,0,0), new Time(17,0,0), new Time(18,0,0), new Time(18,30,0));
        VaccineSchedule completed4 = new VaccineSchedule(111111111, vc.getName(), "Primeira", Constants.VACCINE_LOT, new Date(2022,10,10), new Time(16,0,0), new Time(17,0,0), new Time(21,0,0), new Time(18,30,0));
        VaccineScheduleStore schedulesStore = new VaccineScheduleStore();
        schedulesStore.getCompletedSchedulesList().add(completed1);
        schedulesStore.getCompletedSchedulesList().add(completed2);
        schedulesStore.getCompletedSchedulesList().add(completed3);
        schedulesStore.getCompletedSchedulesList().add(completed4);
        Company.setScheduleStore(schedulesStore);

        VaccineSchedule[] completedSchedules = schedulesStore.getCompletedSchedulesList().toArray(new VaccineSchedule[0]);

        VaccineSchedule[] expected = {completed1, completed2, completed3, completed4};

        sort.ascendingBubbleSort(completedSchedules);

        Assert.assertArrayEquals(expected, completedSchedules);
    }

    /**
     * test the method of sorting an array by descending order, using bubble sort
     */
    @Test
    public void descendingBubbleSort() {
        SortArrivalTime sort = new SortArrivalTime();

        Vaccine vc = new Vaccine(new VaccineType("Covid"), new AdministrationProcess(new AgeRange(10,100),10,1),"Salva-1");

        Client c = new Client(11111111, "Name1", "Address1", Constants.SEX_MALE, new Date(2002, 10, 10), 111111111, "name1@gmail.com", 111111111, "newPass1");
        ClientStore cStore = new ClientStore();
        cStore.getClientList().add(c);
        Company.setClientStore(cStore);

        VaccineSchedule completed1 = new VaccineSchedule(111111111, vc.getName(), "Primeira", Constants.VACCINE_LOT, new Date(2022,10,10), new Time(16,0,0), new Time(17,0,0), new Time(12,0,0), new Time(18,30,0));
        VaccineSchedule completed2 = new VaccineSchedule(111111111, vc.getName(), "Primeira", Constants.VACCINE_LOT, new Date(2022,10,10), new Time(16,0,0), new Time(17,0,0), new Time(15,0,0), new Time(18,30,0));
        VaccineSchedule completed3 = new VaccineSchedule(111111111, vc.getName(), "Primeira", Constants.VACCINE_LOT, new Date(2022,10,10), new Time(16,0,0), new Time(17,0,0), new Time(18,0,0), new Time(18,30,0));
        VaccineSchedule completed4 = new VaccineSchedule(111111111, vc.getName(), "Primeira", Constants.VACCINE_LOT, new Date(2022,10,10), new Time(16,0,0), new Time(17,0,0), new Time(21,0,0), new Time(18,30,0));
        VaccineScheduleStore schedulesStore = new VaccineScheduleStore();
        schedulesStore.getCompletedSchedulesList().add(completed1);
        schedulesStore.getCompletedSchedulesList().add(completed2);
        schedulesStore.getCompletedSchedulesList().add(completed3);
        schedulesStore.getCompletedSchedulesList().add(completed4);
        Company.setScheduleStore(schedulesStore);

        VaccineSchedule[] completedSchedules = schedulesStore.getCompletedSchedulesList().toArray(new VaccineSchedule[0]);

        VaccineSchedule[] expected = {completed1, completed2, completed3, completed4};

        sort.descendingBubbleSort(completedSchedules);

        Assert.assertArrayEquals(expected, completedSchedules);
    }

    /**
     * test the method of sorting an array by ascending order, using selection sort
     */
    @Test
    public void ascendingSelectionSort() {
        SortArrivalTime sort = new SortArrivalTime();

        Vaccine vc = new Vaccine(new VaccineType("Covid"), new AdministrationProcess(new AgeRange(10,100),10,1),"Salva-1");

        Client c = new Client(11111111, "Name1", "Address1", Constants.SEX_MALE, new Date(2002, 10, 10), 111111111, "name1@gmail.com", 111111111, "newPass1");
        ClientStore cStore = new ClientStore();
        cStore.getClientList().add(c);
        Company.setClientStore(cStore);

        VaccineSchedule completed1 = new VaccineSchedule(111111111, vc.getName(), "Primeira", Constants.VACCINE_LOT, new Date(2022,10,10), new Time(16,0,0), new Time(17,0,0), new Time(12,0,0), new Time(18,30,0));
        VaccineSchedule completed2 = new VaccineSchedule(111111111, vc.getName(), "Primeira", Constants.VACCINE_LOT, new Date(2022,10,10), new Time(16,0,0), new Time(17,0,0), new Time(15,0,0), new Time(18,30,0));
        VaccineSchedule completed3 = new VaccineSchedule(111111111, vc.getName(), "Primeira", Constants.VACCINE_LOT, new Date(2022,10,10), new Time(16,0,0), new Time(17,0,0), new Time(18,0,0), new Time(18,30,0));
        VaccineSchedule completed4 = new VaccineSchedule(111111111, vc.getName(), "Primeira", Constants.VACCINE_LOT, new Date(2022,10,10), new Time(16,0,0), new Time(17,0,0), new Time(21,0,0), new Time(18,30,0));
        VaccineScheduleStore schedulesStore = new VaccineScheduleStore();
        schedulesStore.getCompletedSchedulesList().add(completed1);
        schedulesStore.getCompletedSchedulesList().add(completed2);
        schedulesStore.getCompletedSchedulesList().add(completed3);
        schedulesStore.getCompletedSchedulesList().add(completed4);
        Company.setScheduleStore(schedulesStore);

        VaccineSchedule[] completedSchedules = schedulesStore.getCompletedSchedulesList().toArray(new VaccineSchedule[0]);

        VaccineSchedule[] expected = {completed1, completed2, completed3, completed4};

        sort.ascendingSelectionSort(completedSchedules);

        Assert.assertArrayEquals(expected, completedSchedules);
    }

    /**
     * test the method of sorting an array by descending order, using selection sort
     */
    @Test
    public void descendingSelectionSort() {
        SortArrivalTime sort = new SortArrivalTime();

        Vaccine vc = new Vaccine(new VaccineType("Covid"), new AdministrationProcess(new AgeRange(10,100),10,1),"Salva-1");

        Client c = new Client(11111111, "Name1", "Address1", Constants.SEX_MALE, new Date(2002, 10, 10), 111111111, "name1@gmail.com", 111111111, "newPass1");
        ClientStore cStore = new ClientStore();
        cStore.getClientList().add(c);
        Company.setClientStore(cStore);

        VaccineSchedule completed1 = new VaccineSchedule(111111111, vc.getName(), "Primeira", Constants.VACCINE_LOT, new Date(2022,10,10), new Time(16,0,0), new Time(17,0,0), new Time(12,0,0), new Time(18,30,0));
        VaccineSchedule completed2 = new VaccineSchedule(111111111, vc.getName(), "Primeira", Constants.VACCINE_LOT, new Date(2022,10,10), new Time(16,0,0), new Time(17,0,0), new Time(15,0,0), new Time(18,30,0));
        VaccineSchedule completed3 = new VaccineSchedule(111111111, vc.getName(), "Primeira", Constants.VACCINE_LOT, new Date(2022,10,10), new Time(16,0,0), new Time(17,0,0), new Time(18,0,0), new Time(18,30,0));
        VaccineSchedule completed4 = new VaccineSchedule(111111111, vc.getName(), "Primeira", Constants.VACCINE_LOT, new Date(2022,10,10), new Time(16,0,0), new Time(17,0,0), new Time(21,0,0), new Time(18,30,0));
        VaccineScheduleStore schedulesStore = new VaccineScheduleStore();
        schedulesStore.getCompletedSchedulesList().add(completed1);
        schedulesStore.getCompletedSchedulesList().add(completed2);
        schedulesStore.getCompletedSchedulesList().add(completed3);
        schedulesStore.getCompletedSchedulesList().add(completed4);
        Company.setScheduleStore(schedulesStore);

        VaccineSchedule[] completedSchedules = schedulesStore.getCompletedSchedulesList().toArray(new VaccineSchedule[0]);

        VaccineSchedule[] expected = {completed1, completed2, completed3, completed4};

        sort.descendingSelectionSort(completedSchedules);

        Assert.assertArrayEquals(expected, completedSchedules);
    }
}