package app.domain.tests;

import app.domain.us016.HighestSublist;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class HighestSublistTest {

    /**
     * test the method to get the max sublist from an array, using the bench mark algorithm
     */
    @Test
    public void getSublistBenchMarkAlgorithm() {
        HighestSublist ctrl = new HighestSublist();
        int[] diffArray = {242, 194, 241, 92, 53, 27, 74, -75, -357, -125, -29, 141, 269, 121, 50, 26, -29, -118, -254, 89, 246, -20, -119, -322};
        int[] expected = {242, 194, 241, 92, 53, 27, 74, -75, -357, -125, -29, 141, 269, 121, 50, 26};
        int[] actual = ctrl.getSublistBenchMarkAlgorithm(diffArray);
        Assert.assertArrayEquals(expected, actual);
    }

    /**
     * test the method to get the highest sublist from an array, using a brute force algorithm
     */
    @Test
    public void getHighestSublist() {
        HighestSublist ctrl = new HighestSublist();
        int[] diffArray = {242, 194, 241, 92, 53, 27, 74, -75, -357, -125, -29, 141, 269, 121, 50, 26, -29, -118, -254, 89, 246, -20, -119, -322};
        int[] expected = {242, 194, 241, 92, 53, 27, 74, -75, -357, -125, -29, 141, 269, 121, 50, 26};
        int[] actual = ctrl.getHighestSublist(diffArray);
        Assert.assertArrayEquals(expected, actual);
    }
}