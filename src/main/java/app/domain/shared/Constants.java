package app.domain.shared;

/**
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Constants {
    /**
     * dgs
     */
    public static final String COMPANY_DESIGNATION = "DGS";

    /**
     * male sex
     */
    public static final String SEX_MALE = "MALE";

    /**
     * female sex
     */
    public static final String SEX_FEMALE = "FEMALE";

    /**
     * administrator
     */
    public static final String ROLE_ADMIN = "ADMINISTRATOR";

    /**
     * nurse
     */
    public static final String ROLE_NURSE = "NURSE";

    /**
     * center coordinator
     */
    public static final String ROLE_COORDINATOR = "CENTER COORDINATOR";

    /**
     * receptionist
     */
    public static final String ROLE_RECEPTIONIST = "RECEPTIONIST";

    /**
     * client/sns user
     */
    public static final String ROLE_SNS_USER = "SNS USER";

    /**
     * user roles array
     */
    public static final String[] USER_ROLES = {ROLE_ADMIN, ROLE_NURSE, ROLE_COORDINATOR, ROLE_RECEPTIONIST, ROLE_SNS_USER};

    /**
     * health care center
     */
    public static final String TYPE_HEALTH_CARE_CENTER = "HEALTH CARE CENTER";

    /**
     * comunity mass center
     */
    public static final String TYPE_COMUNITY_MASS_CENTER = "COMUNITY MASS CENTER";

    /**
     * center types
     */
    public static final String[] CENTER_TYPES = {TYPE_HEALTH_CARE_CENTER, TYPE_COMUNITY_MASS_CENTER};

    /**
     * days per week
     */
    public static final String[] DAYS_WEEK = {"MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY", "SUNDAY"};

    /**
     * days per week length
     */
    public static final int DAYS_PER_WEEK = DAYS_WEEK.length;

    /**
     * recovery time
     */
    public static final long RECOVERY_TIME = 30000;

    /**
     * available doses
     */
    public static final String[] AVAILABLE_DOSES = {"First", "Second", "Third", "Fourth", "Fifth", "Sixth", "Seventh", "Eighth", "Ninth", "Tenth"};

    /**
     *generic  vaccine lot
     */
    public static final String VACCINE_LOT = "LOT_01";

    /**
     * bubble sort algorithm
     */
    public static final String STR_BUBBLE = "Bubble Sort";

    /**
     * selection sort algorithm
     */
    public static final String STR_SELECTION = "Selection Sort";

    /**
     * ascending order
     */
    public static final String STR_ASCENDING = "Ascending";

    /**
     * descending order
     */
    public static final String STR_DESCENDING = "Descending";

    /**
     * arrival time
     */
    public static final String STR_ARRIVAL = "Arrival Time";

    /**
     * leaving time
     */
    public static final String STR_LEAVING = "Leaving Time";

    /**
     * config properties file name
     */
    public static final String PARAMS_FILENAME = "config.properties";

    /**
     * company designation
     */
    public static final String PARAMS_COMPANY_DESIGNATION = "Company.Designation";

    /**
     * bench mark algorithm
     */
    public static final String BENCH_MARK_ALGORITHM = "Bench Mark";

    /**
     * brute force algorithm
     */
    public static final String BRUTE_FORCE_ALGORITHM = "Brute Force";

    /**
     * highest sublist algorithm
     */
    public static final String HIGHEST_SUBLIST_ALGORITHM = "Algorithm.Highest.Sublist";
}
