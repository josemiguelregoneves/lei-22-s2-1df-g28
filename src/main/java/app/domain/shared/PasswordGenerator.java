package app.domain.shared;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class PasswordGenerator {
    /**
     * password
     */
    private String password;

    /**
     * empty constructor
     */
    public PasswordGenerator() {
        this.password = generatePassword();
    }

    /**
     * method to generate the employee password
     *
     * @return password
     */
    public static String generatePassword() {
        final Random RANDOM = new Random();

        ArrayList<String> passwordList = new ArrayList<>();

        final String UPPERS = "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z";
        final String[] UPPERS_LIST = UPPERS.split(",");
        final String LOWERS = "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z";
        final String[] LOWERS_LIST = LOWERS.split(",");
        final String DIGITS = "0,1,2,3,4,5,6,7,8,9";
        final String[] DIGITS_LIST = DIGITS.split(",");

        final int TOTAL_UPPERS = 3;
        final int TOTAL_DIGITS = 2;
        final int TOTAL_LOWERS = 2;

        for (int i = 0; i < TOTAL_UPPERS; i++) {
            passwordList.add(UPPERS_LIST[RANDOM.nextInt(UPPERS_LIST.length)]);
        }

        for (int i = 0; i < TOTAL_DIGITS; i++) {
            passwordList.add(DIGITS_LIST[RANDOM.nextInt(DIGITS_LIST.length)]);
        }

        for (int i = 0; i < TOTAL_LOWERS; i++) {
            passwordList.add(LOWERS_LIST[RANDOM.nextInt(LOWERS_LIST.length)]);
        }

        Collections.shuffle(passwordList);

        StringBuilder password = new StringBuilder();
        for (String s : passwordList) {
            password.append(s);
        }

        return password.toString();
    }
}
