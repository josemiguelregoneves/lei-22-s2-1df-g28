package app.domain.shared;

import app.domain.conceptual.*;
import app.domain.model.Company;

import java.sql.Time;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public abstract class Validator {

    /**
     * method to validate phone number
     *
     * @param phoneNumber
     * @return true/false
     */
    public static boolean validatePhoneNumber(int phoneNumber) {
        return String.valueOf(phoneNumber).length() == 9;
    }

    /**
     * method to validate fax number
     *
     * @param faxNumber
     * @return true/false
     */
    public static boolean validateFaxNumber(int faxNumber) {
        return String.valueOf(faxNumber).length() == 9;
    }

    /**
     * method to validate sns number
     *
     * @param snsNumber
     * @return true/false
     */
    public static boolean validateSnsNumber(int snsNumber) {
        return String.valueOf(snsNumber).length() == 9;
    }

    /**
     * method to validate cc number
     *
     * @param ccNumber
     * @return true/false
     */
    public static boolean validateCcNumber(int ccNumber) {
        return String.valueOf(ccNumber).length() == 8;
    }

    /**
     * method to validate email
     *
     * @param email
     * @return true/false
     */
    public static boolean validateEmail(String email) {
        return email.contains("@") && email.contains(".pt") && !email.contains(".com") || email.contains("@") && email.contains(".com") && !email.contains(".pt");
    }

    /**
     * method to validate website
     *
     * @param website
     * @return true/false
     */
    public static boolean validateWebsite(String website) {
        return website.contains(".com") || website.contains(".pt");
    }

    /**
     * method to validate the time
     *
     * @param strTime
     * @return true/false
     */
    public static boolean validateTime(String strTime) {
        String[] arrTime = strTime.split(":");
        int hour = Integer.parseInt(arrTime[0]);
        int minute = Integer.parseInt(arrTime[1]);
        int second = Integer.parseInt(arrTime[2]);
        return strTime != null && (hour >= 0 && hour < 24) && (minute >= 0 && minute < 60) && (second >= 0 && second < 60);
    }

    /**
     * method to validate the time schedule
     *
     * @param time
     * @param center
     * @return true/false
     */
    public static boolean validateTimeSchedule(Time time, VaccinationCenter center) {
        return time.after(center.getOpeningHour()) && time.before(center.getClosingHour());
    }

    /**
     * method to validate the closing hour
     *
     * @param opHour
     * @param clHour
     * @return true/false
     */
    public static boolean validateClosingHour(String opHour, String clHour) {
        String[] openingArr = opHour.split(":");
        String[] closingArr = clHour.split(":");
        return Integer.parseInt(closingArr[0]) - Integer.parseInt(openingArr[0]) > 7;
    }

    /**
     * method to validate the opening hour
     *
     * @param opHour
     * @return true/false
     */
    public static boolean validateOpeningHour(String opHour) {
        final Time MIN_OP = new Time(1, 0, 0);
        final Time MAX_OP = new Time(23, 0, 0);
        Time openingHour = Time.valueOf(opHour);
        return openingHour.after(MIN_OP) && openingHour.before(MAX_OP);
    }

    /**
     * method to validate the age schedule
     *
     * @param birthDate
     * @param vc
     * @return true/false
     */
    public static boolean validateAgeSchedule(Date birthDate, Vaccine vc) {
        int age = getAge(birthDate), minAge = 0, maxAge = 0;
        boolean validate = false;
        minAge = vc.getAdministration().getRange().getMinAge();
        maxAge = vc.getAdministration().getRange().getMaxAge();
        if (age >= minAge && age <= maxAge) {
            validate = true;
        }
        return validate;
    }

    /**
     * method to get the age of a sns user
     *
     * @param birthDate
     * @return age (int)
     */
    public static int getAge(Date birthDate) {
        int age = 0;

        final int YEAR = Calendar.getInstance().getTime().getYear();
        final int MONTH = Calendar.getInstance().getTime().getMonth();
        final int DAY = Calendar.getInstance().getTime().getDate();

        int year = birthDate.getYear();
        int month = birthDate.getMonth();
        int day = birthDate.getDate();

        if (YEAR > year && MONTH > month) {
            age = YEAR - year;
        }
        if (YEAR > year && MONTH < month) {
            age = YEAR - year + 1;
        }
        if (YEAR > year && MONTH == month && DAY == day) {
            age = YEAR - year + 1;
        }

        return age;
    }

    /**
     * method to validate if the user has waited the necessary time to take the vaccine
     *
     * @param schedule
     * @return true/false
     * @throws ParseException
     */
    public static boolean validateTimeSinceLastVaccine(VaccineSchedule schedule) {
        final int MIN_DAYS = 100;

        Date date1 = schedule.getScheduleDate();
        Date date2;

        long difDays = MIN_DAYS;

        Client client = getClientBySnsNumber(schedule.getSnsNumber());

        for (VaccineSchedule vcSchedule : client.getCompletedList()) {
            date2 = vcSchedule.getScheduleDate();
            difDays = getDifferenceInDays(date1, date2);
        }

        if (difDays >= MIN_DAYS || client.getCompletedList().isEmpty()) {
            return true;
        } else {
            Alert.message("Invalid time since the last vaccine");
            return false;
        }

    }

    /**
     * method to get the difference in days between two dates
     *
     * @param first
     * @param last
     * @return days (long)
     */
    private static long getDifferenceInDays(Date first, Date last) {
        long dt = (first.getTime() - last.getTime()) + 3600000;
        return (dt / 86400000L);
    }

    /**
     * method to validate the number of doses in the schedule
     *
     * @param schedule
     *
     * @return true/false
     */
    public static boolean validateNumberDosesSchedule(VaccineSchedule schedule) {
        Vaccine vc = Company.getVcStore().getFirstVaccineOfType(schedule);
        if (vc == null) {
            return true;
        } else {
            final int MAX_DOSES = vc.getAdministration().getDoses();
            int total = 0;

            for (VaccineSchedule complete : Validator.getClientBySnsNumber(schedule.getSnsNumber()).getCompletedList()) {
                if (Validator.getVaccineByName(complete.getVc()).equals(vc)) {
                    total++;
                }
            }

            if (total < MAX_DOSES) {
                return true;
            } else {
                Alert.message("Number of Doses Complete. Cannot get more");
                return false;
            }
        }
    }

    /**
     * method to validate the number of doses
     *
     * @param schedule
     *
     * @return true/false
     */
    public static boolean validateNumberOfDoses(VaccineSchedule schedule) {
        Client client = Validator.getClientBySnsNumber(schedule.getSnsNumber());
        Vaccine vc = Company.getVcStore().getFirstVaccineOfType(schedule);
        if (vc == null) {
            return true;
        } else {
            final int MAX_DOSES = vc.getAdministration().getDoses();
            int total = 0;
            for (VaccineSchedule vcSchedule : client.getCompletedList()) {
                if (Validator.getVaccineByName(vcSchedule.getVc()).equals(vc)) {
                    total++;
                }
            }
            if (total < MAX_DOSES) {
                return true;
            } else {
                Alert.message("Cannot Get More Doses!");
                return false;
            }
        }
    }

    /**
     * method to convert the time to seconds
     *
     * @param time
     *
     * @return seconds
     */
    public static int convertTimeSeconds(Time time) {
        return time.getHours() * 3600 + time.getMinutes() * 60 + time.getSeconds();
    }

    /**
     * method to get the client by sns number
     *
     * @param snsNumber
     *
     * @return client
     */
    public static Client getClientBySnsNumber(int snsNumber) {
        for (Client snsUser : Company.getClientStore().getClientList()) {
            if (snsUser.getSnsNumber() == snsNumber) {
                return snsUser;
            }
        }
        return null;
    }

    /**
     * method to get the current hour
     *
     * @return current hour
     */
    public static Time getCurrentHour() {
        Calendar currentDate = Calendar.getInstance();
        int hour = currentDate.get(Calendar.HOUR_OF_DAY);
        int minute = currentDate.get(Calendar.MINUTE);
        int second = currentDate.get(Calendar.SECOND);
        String strTime = String.format("%d:%d:%d", hour, minute, second);
        return Time.valueOf(strTime);
    }

    /**
     * method to verify if a date is the same as the current date
     *
     * @param date
     *
     * @return true/false
     */
    public static boolean dateEqualsCurrentDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        int currentDay = calendar.getTime().getDate();
        int currentMonth = calendar.getTime().getMonth();
        int currentYear = calendar.getTime().getYear();

        int day = date.getDate();
        int month = date.getMonth();
        int year = date.getYear();

        if (currentDay == day && currentMonth == month && currentYear == year) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * method to verify if two dates are the same
     *
     * @param date1
     * @param date2
     *
     * @return true/false
     */
    public static boolean dateEqualsToDate(Date date1, Date date2) {
        int day1 = date1.getDate();
        int month1 = date1.getMonth();
        int year1 = date1.getYear();

        int day2 = date2.getDate();
        int month2 = date2.getMonth() - 1;
        int year2 = date2.getYear() - 1900;

        return day1 == day2 && month1 == month2 && year1 == year2;
    }

    /**
     * method to verify if exists any client registered with the sns number
     *
     * @param snsNumber
     *
     * @return true/false
     */
    public static boolean existsClientWithSnsNumber(int snsNumber) {
        for (Client snsUser : Company.getClientStore().getClientList()) {
            if (snsUser.getSnsNumber() == snsNumber) {
                return true;
            }
        }
        return false;
    }

    /**
     * method to validate if existes any vaccine registered with the name
     *
     * @param vcName
     *
     * @return true/false
     */
    public static boolean existsVaccineName(String vcName) {
        for (Vaccine vc : Company.getVcStore().getVaccineList()) {
            if (vc.getName().equals(vcName)) {
                return true;
            }
        }
        return false;
    }

    /**
     * method to get the vaccine by name
     *
     * @param vcName
     *
     * @return vaccine
     */
    public static Vaccine getVaccineByName(String vcName) {
        for (Vaccine vc : Company.getVcStore().getVaccineList()) {
            if (vc.getName().equals(vcName)) {
                return vc;
            }
        }
        return null;
    }

    /**
     * method to convert the dose in string to the respective integer value
     *
     * @param dose
     *
     * @return dose (int)
     */
    public static int convertStringDoseToInt(String dose) {
        return switch (dose) {
            case "Primeira", "First", "primeira", "first" -> 1;
            case "Segunda", "Second", "segunda", "second" -> 2;
            case "Terceira", "Third", "terceira", "third" -> 3;
            case "Quarta", "Fourth", "quarta", "fourth" -> 4;
            case "Quinta", "Fifth", "quinta", "fifth" -> 5;
            case "Sexta", "Sixth", "sexta", "sixth" -> 6;
            case "Sétima", "Seventh", "sétima", "seventh" -> 7;
            case "Oitava", "Eighth", "oitava", "eighth" -> 8;
            case "Nona", "Ninth", "nona", "ninth" -> 9;
            case "Décima", "Tenth", "décima", "tenth" -> 10;
            default -> 0;
        };
    }

    /**
     * method to convert a date in gregorian calendar to time
     *
     * @param date
     *
     * @return time
     */
    public static Time getHourOfDate(GregorianCalendar date) {
        int hour = date.get(Calendar.HOUR_OF_DAY);
        int minute = date.get(Calendar.MINUTE);
        int second = date.get(Calendar.SECOND);
        String strTime = String.format("%d:%d:%d", hour, minute, second);
        return Time.valueOf(strTime);
    }

    /**
     * method to validate if a client is fully vaccinated
     *
     * @param schedule
     *
     * @return true/false
     */
    public static boolean clientFullVaccinated(VaccineSchedule schedule) {
        Vaccine vc = getVaccineByName(schedule.getVc());
        final int MAX_DOSES = vc.getAdministration().getDoses();
        int current = 0;
        for (VaccineSchedule vcSchedule : Company.getScheduleStore().getCompletedSchedulesList()) {
            if (schedule.getSnsNumber() == vcSchedule.getSnsNumber() && vcSchedule.getVc().equals(schedule.getVc())) {
                current++;
            }
        }
        return current == MAX_DOSES;
    }

    /**
     * method to convert a date in string to date
     *
     * @param strDate
     *
     * @return Date date
     */
    public static Date convertStringToDate(String strDate) {
        String[] arrDate = strDate.split("/");
        int day = Integer.parseInt(arrDate[0]);
        int month = Integer.parseInt(arrDate[1]);
        int year = Integer.parseInt(arrDate[2]);
        return new Date(year, month, day);
    }

    /**
     * method to convert a date to string
     *
     * @param date
     *
     * @return date (string)
     */
    public static String convertDateToString(Date date) {
        int day = date.getDate();
        int month = date.getMonth();
        int year = date.getYear();
        String strDate = String.format("%d/%d/%d", day, month, year);
        return strDate;
    }

    /**
     * method to validate if the client has one or more completed schedules
     *
     * @param snsNumber
     *
     * @return true/false
     */
    public static boolean clientVaccinated(int snsNumber) {
        for (VaccineSchedule schedule : Company.getScheduleStore().getCompletedSchedulesList()) {
            if (schedule.getSnsNumber() == snsNumber) {
                return true;
            }
        }
        return false;
    }

    /**
     * method to convert the time in seconds
     *
     * @param time
     *
     * @return seconds
     */
    public static int convertTimeToSeconds(Time time) {
        return time.getHours() * 3600 + time.getMinutes() * 60 + time.getSeconds();
    }

    /**
     * method to convert the time in seconds to Time
     *
     * @param seconds
     *
     * @return time
     */
    public static Time convertSecondsToTime(int seconds) {
        int hours = seconds / 3600;
        seconds -= hours * 3600;
        int minutes = seconds / 60;
        seconds -= minutes * 60;
        return new Time(hours, minutes, seconds);
    }

    /**
     * method to convert an integer dose to string
     *
     * @param dose
     *
     * @return doses (string)
     */
    public static String convertIntDoseToString(int dose) {
        return switch (dose) {
            case 1 -> "Primeira";
            case 2 -> "Segunda";
            case 3 -> "Terceira";
            case 4 -> "Quarta";
            case 5 -> "Quinta";
            case 6 -> "Sexta";
            case 7 -> "Sétima";
            case 8 -> "Oitava";
            default -> null;
        };
    }

    /**
     * method to get the number of doses of a client
     *
     * @param schedule
     *
     * @return doses (int)
     */
    public static int getNumberOfDoses(VaccineSchedule schedule) {
        Client client = getClientBySnsNumber(schedule.getSnsNumber());
        int total = 0;
        for (VaccineSchedule vcSchedule : client.getCompletedList()) {
            if (vcSchedule.getVc().equals(schedule.getVc())) {
                total++;
            }
        }
        return total;
    }

}
