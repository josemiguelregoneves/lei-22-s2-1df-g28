package app.domain.shared;

import app.domain.conceptual.Client;
import app.domain.conceptual.VaccineSchedule;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public abstract class SMS {

    /**
     * method to send an sms after vc administration was complete
     *
     * @param schedule
     *
     * @throws FileNotFoundException
     */
    public static void sendSmsVcComplete(VaccineSchedule schedule) throws FileNotFoundException {
        Client snsUser = Validator.getClientBySnsNumber(schedule.getSnsNumber());
        File file = new File("src/main/java/app/sms/vc-complete.txt");
        PrintWriter write = new PrintWriter(file);
        write.println("Name: " + snsUser.getName());
        write.println("SNS Number: " + snsUser.getSnsNumber());
        write.println("Phone Number: " + snsUser.getPhoneNumber());
        write.println("Vaccine Administration Complete");
        write.close();
    }

    /**
     * method to send an sms after recovery time was complete
     *
     * @param schedule
     *
     * @throws FileNotFoundException
     * @throws InterruptedException
     */
    public static void sendSmsRecoveryComplete(VaccineSchedule schedule) throws FileNotFoundException, InterruptedException {
        Client snsUser = Validator.getClientBySnsNumber(schedule.getSnsNumber());
        Thread.sleep(Constants.RECOVERY_TIME);
        File file = new File("src/main/java/app/sms/recovery-complete.txt");
        PrintWriter write = new PrintWriter(file);
        write.println("Name: " + snsUser.getName());
        write.println("SNS Number: " + snsUser.getSnsNumber());
        write.println("Phone Number: " + snsUser.getPhoneNumber());
        write.println("Recovery Time Complete");
        write.close();
    }


    /**
     * method to send an sms after vaccine schedules was complete
     *
     * @param schedule
     *
     * @throws FileNotFoundException
     * @throws InterruptedException
     */
    public static void sendSmsScheduleComplete(VaccineSchedule schedule) throws FileNotFoundException, InterruptedException {
        Client snsUser = Validator.getClientBySnsNumber(schedule.getSnsNumber());
        File file = new File("src/main/java/app/sms/schedule-complete.txt");
        PrintWriter write = new PrintWriter(file);
        write.println("Name: " + snsUser.getName());
        write.println("SNS Number: " + snsUser.getSnsNumber());
        write.println("Phone Number: " + snsUser.getPhoneNumber());
        write.println("Schedule Complete");
        write.println("Vaccine Type: " + schedule.getVcType().getName());
        write.close();
    }
}
