package app.domain.shared;

public class Alert {
    /**
     * print alert message method
     *
     * @param msg
     */
    public static void message(String msg) {
        System.out.println(msg);
    }
}
