package app.domain.shared;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public abstract class GetConfigProperties {

    /**
     * method to get the config properties
     *
     * @param data
     *
     * @return config property
     */
    public static String getConfigProperties(String data) {
        Properties prop = new Properties();
        String propString;
        try {
            InputStream in = new FileInputStream("src/main/resources/config.properties");
            prop.load(in);
            propString = prop.getProperty(data);
            in.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return propString;
    }

}
