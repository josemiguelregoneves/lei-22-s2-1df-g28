package app.domain.interfaces;

import app.domain.conceptual.VaccineSchedule;

import java.io.FileNotFoundException;

public interface BubbleSort {

    /**
     * method that implements the bubble sort algorithm by ascending order
     *
     * @param arrSchedules
     *
     * @throws FileNotFoundException
     */
    void ascendingBubbleSort(VaccineSchedule[] arrSchedules) throws FileNotFoundException;


    /**
     * method that implements the bubble sort algorithm by descending order
     *
     * @param arrSchedules
     *
     * @throws FileNotFoundException
     */
    void descendingBubbleSort(VaccineSchedule[] arrSchedules) throws FileNotFoundException;

}
