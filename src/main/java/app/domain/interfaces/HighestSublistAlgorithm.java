package app.domain.interfaces;

public interface HighestSublistAlgorithm {

    /**
     * method to get the maximum sublist
     *
     * @param diffArray
     *
     * @return maxSublist
     */
    int[] getHighestSublist(int[] diffArray);

}
