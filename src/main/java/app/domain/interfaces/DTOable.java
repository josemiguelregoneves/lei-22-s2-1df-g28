package app.domain.interfaces;

public interface DTOable<T> {
    T toDTO();
}
