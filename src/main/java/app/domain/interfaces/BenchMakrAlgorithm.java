package app.domain.interfaces;

public interface BenchMakrAlgorithm {

    /**
     * bench mark algorithm to get the highest sublist
     *
     * @param diffArray
     *
     * @return sublist
     */
    int[] getSublistBenchMarkAlgorithm(int[] diffArray);

}
