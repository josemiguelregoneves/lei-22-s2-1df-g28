package app.domain.interfaces;

import app.domain.conceptual.VaccineSchedule;

import java.io.FileNotFoundException;

public interface SelectionSort {

    /**
     * method that implements the selection sort algorithm by ascending order
     *
     * @param arrSchedules
     *
     * @throws FileNotFoundException
     */
    void ascendingSelectionSort(VaccineSchedule[] arrSchedules) throws FileNotFoundException;


    /**
     * method that implements the selection sort algorithm by descending order
     *
     * @param arrSchedules
     *
     * @throws FileNotFoundException
     */
    void descendingSelectionSort(VaccineSchedule[] arrSchedules) throws FileNotFoundException;
}
