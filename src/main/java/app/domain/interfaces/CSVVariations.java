package app.domain.interfaces;

import java.io.File;
import java.io.FileNotFoundException;

public interface CSVVariations {
    /**
     * Loads and creates snsUsers contained on a CSV file
     *
     * @param file File loaded to the program
     * @throws FileNotFoundException to prevent possible errors (File not found or existant)
     */
    void readFile(File file) throws FileNotFoundException;
}
