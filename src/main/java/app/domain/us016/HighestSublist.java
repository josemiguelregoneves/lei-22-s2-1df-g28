package app.domain.us016;

import app.domain.interfaces.BenchMakrAlgorithm;
import app.domain.interfaces.HighestSublistAlgorithm;

import java.util.Arrays;

public class HighestSublist implements BenchMakrAlgorithm, HighestSublistAlgorithm {

    /**
     * bench mark algorithm to get the highest sublist
     *
     * @param diffArray
     *
     * @return sublist
     */
    @Override
    public int[] getSublistBenchMarkAlgorithm(int[] diffArray) {
        int maxSoFar = 0;
        int maxEndingHere = 0;
        int startMaxSoFar = 0;
        int endMaxSoFar = 0;
        int startMaxEndingHere = 0;

        for (int i = 0; i < diffArray.length; ++i) {
            int elem = diffArray[i];
            int endMaxEndingHere = i + 1;
            if (maxEndingHere + elem < 0) {
                maxEndingHere = 0;
                startMaxEndingHere = i + 1;
            } else {
                maxEndingHere += elem;
            }

            if (maxSoFar < maxEndingHere) {
                maxSoFar = maxEndingHere;
                startMaxSoFar = startMaxEndingHere;
                endMaxSoFar = endMaxEndingHere;
            }
        }

        return Arrays.copyOfRange(diffArray, startMaxSoFar, endMaxSoFar);
    }

    /**
     * method to get the maximum sublist
     *
     * @param diffArray
     *
     * @return maxSublist
     */
    @Override
    public int[] getHighestSublist(int[] diffArray) {
        int sum;
        int comp = 0;
        int position1 = -1;
        int position2 = -1;

        for (int line1 = 0; line1 < diffArray.length; line1++) {
            sum = 0;
            for (int line2 = line1; line2 < diffArray.length; line2++) {
                sum += diffArray[line2];
                if (sum > comp) {
                    position1 = line1;
                    position2 = line2;
                    comp = sum;
                }
            }
        }

        int[] subList = new int[(position2 - position1) + 1];
        int cont = 0;
        for (int x = position1; x <= position2; x++) {
            subList[cont] = diffArray[x];
            cont++;
        }
        return subList;
    }

    /**
     * method to get the time execution of bench mark algorithm
     *
     * @param diffArray
     *
     * @return nanoseconds
     */
    public long benchMarkTimeExecution(int[] diffArray) {
        long time1 = System.currentTimeMillis();
        int[] maxSublist = getSublistBenchMarkAlgorithm(diffArray);
        long time2 = System.currentTimeMillis();
        return time2 - time1;
    }

    /**
     * method to get the time execution of brute force algorithm
     *
     * @param diffArray
     *
     * @return nanoseconds
     */
    public long highestSublistTimeExecution(int[] diffArray) {
        long time1 = System.currentTimeMillis();
        int[] maxSublist = getHighestSublist(diffArray);
        long time2 = System.currentTimeMillis();
        return time2 - time1;
    }
}
