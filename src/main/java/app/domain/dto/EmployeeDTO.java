package app.domain.dto;

import app.domain.conceptual.VaccinationCenter;

import java.io.Serializable;
import java.util.Objects;

public class EmployeeDTO implements Serializable {

    /**
     * id
     */
    public int id;

    /**
     * name
     */
    public String name;

    /**
     * address
     */
    public String address;

    /**
     * phoneNumber
     */
    public int phoneNumber;

    /**
     * email
     */
    public String email;

    /**
     * ccNumber
     */
    public int ccNumber;

    /**
     * password
     */
    public String password;

    /**
     * idRole
     */
    public String idRole;

    /**
     * center
     */
    public VaccinationCenter center;

    /**
     * constructor with parameters
     *
     * @param name
     * @param address
     * @param phoneNumber
     * @param email
     * @param ccNumber
     * @param idRole
     * @param center
     */
    public EmployeeDTO(String name, String address, int phoneNumber, String email, int ccNumber, String idRole, VaccinationCenter center) {
        this.name = name;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.ccNumber = ccNumber;
        this.idRole = idRole;
        this.center = center;
    }

    /**
     * to String method
     *
     * @return string
     */
    @Override
    public String toString() {
        return "EmployeeDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", phoneNumber=" + phoneNumber +
                ", email='" + email + '\'' +
                ", ccNumber=" + ccNumber +
                ", password='" + password + '\'' +
                ", idRole='" + idRole + '\'' +
                ", center=" + center +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmployeeDTO that = (EmployeeDTO) o;
        return id == that.id && phoneNumber == that.phoneNumber && ccNumber == that.ccNumber && email.equals(that.email) && password.equals(that.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, phoneNumber, email, ccNumber, password);
    }
}
