package app.domain.dto;

import java.io.Serializable;
import java.util.Objects;

public class VaccineTypeDTO implements Serializable {

    /**
     * name
     */
    public String name;

    /**
     * constructor with parameters
     *
     * @param name
     */
    public VaccineTypeDTO(String name) {
        this.name = name;
    }

    /**
     * to string method
     *
     * @return string
     */
    @Override
    public String toString() {
        return "VaccineTypeDTO{" +
                "name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VaccineTypeDTO that = (VaccineTypeDTO) o;
        return name.equals(that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
