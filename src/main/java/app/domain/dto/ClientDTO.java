package app.domain.dto;

import app.domain.conceptual.VaccineSchedule;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

public class ClientDTO implements Serializable {
    /**
     * ccNumber
     */
    public int ccNumber;

    /**
     * name
     */
    public String name;

    /**
     * address
     */
    public String address;

    /**
     * sex
     */
    public String sex;

    /**
     * birthDate
     */
    public Date birthDate;

    /**
     * phoneNumber
     */
    public int phoneNumber;

    /**
     * email
     */
    public String email;

    /**
     * snsNumber
     */
    public int snsNumber;

    /**
     * password
     */
    public String password;

    /**
     * schedulesList
     */
    public ArrayList<VaccineSchedule> schedulesList;

    /**
     * completedSchedules
     */
    public ArrayList<VaccineSchedule> completed;

    /**
     * constructor with parameters
     *
     * @param ccNumber
     * @param name
     * @param address
     * @param sex
     * @param birthDate
     * @param phoneNumber
     * @param email
     * @param snsNumber
     */
    public ClientDTO(int ccNumber, String name, String address, String sex, Date birthDate, int phoneNumber, String email, int snsNumber) {
        this.ccNumber = ccNumber;
        this.name = name;
        this.address = address;
        this.sex = sex;
        this.birthDate = birthDate;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.snsNumber = snsNumber;
        this.schedulesList = new ArrayList<>();
        this.completed = new ArrayList<>();
    }

    /**
     * toString method
     *
     * @return string
     */
    @Override
    public String toString() {
        return "ClientDTO{" +
                "ccNumber=" + ccNumber +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", sex='" + sex + '\'' +
                ", birthDate=" + birthDate +
                ", phoneNumber=" + phoneNumber +
                ", email='" + email + '\'' +
                ", snsNumber=" + snsNumber +
                ", password='" + password + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClientDTO clientDTO = (ClientDTO) o;
        return ccNumber == clientDTO.ccNumber && phoneNumber == clientDTO.phoneNumber && snsNumber == clientDTO.snsNumber && email.equals(clientDTO.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ccNumber, phoneNumber, email, snsNumber, password);
    }
}
