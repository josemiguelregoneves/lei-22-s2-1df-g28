package app.domain.dto;

import app.domain.conceptual.Client;
import app.domain.conceptual.VaccinationCenter;
import app.domain.conceptual.Vaccine;
import app.domain.conceptual.VaccineType;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;
import java.util.Objects;

public class VaccineScheduleDTO implements Serializable {
    /**
     * snsNumber
     */
    public int snsNumber;

    /**
     * vcType
     */
    public VaccineType vcType;

    /**
     * vaccine
     */
    public String vc;

    /**
     * dose
     */
    public String dose;

    /**
     * lotNumber
     */
    public String lotNumber;

    /**
     * scheduleDate
     */
    public Date scheduleDate;

    /**
     * scheduleTime
     */
    public Time scheduleTime;

    /**
     * arrivalTime
     */
    public Time arrivalTime;

    /**
     * adminTime
     */
    public Time adminTime;

    /**
     * leaveTime
     */
    public Time leaveTime;

    /**
     * constructor with parameters
     *
     * @param snsNumber
     * @param vcType
     * @param scheduleDate
     * @param scheduleTime
     */
    public VaccineScheduleDTO(int snsNumber, VaccineType vcType, Date scheduleDate, Time scheduleTime) {
        this.snsNumber = snsNumber;
        this.vcType = vcType;
        this.scheduleDate = scheduleDate;
        this.scheduleTime = scheduleTime;
    }

}
