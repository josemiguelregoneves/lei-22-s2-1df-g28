package app.domain.dto;

import app.domain.conceptual.Client;
import app.domain.conceptual.VaccineSchedule;

import java.io.Serializable;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Objects;

public class VaccinationCenterDTO implements Serializable {

    /**
     * name
     */
    public String name;

    /**
     * address
     */
    public String address;

    /**
     * phoneNumber
     */
    public int phoneNumber;

    /**
     * email
     */
    public String email;

    /**
     * faxNumber
     */
    public int faxNumber;

    /**
     * website
     */
    public String website;

    /**
     * slotDuration
     */
    public Time slotDuration;

    /**
     * maxVaccines
     */
    public int maxVaccines;

    /**
     * openingHour
     */
    public Time openingHour;

    /**
     * closingHour
     */
    public Time closingHour;

    /**
     * idType
     */
    public String idType;

    /**
     * schedulesList
     */
    public ArrayList<VaccineSchedule> schedulesList;

    /**
     * schedulesWaitingRoom
     */
    public ArrayList<VaccineSchedule> schedulesWaitingRoom;

    /**
     * completedSchedulesList
     */
    public ArrayList<VaccineSchedule> completedSchedulesList;

    /**
     * constructor with parameters
     *
     * @param name
     * @param address
     * @param phoneNumber
     * @param email
     * @param faxNumber
     * @param website
     * @param slotDuration
     * @param maxVaccines
     * @param openingHour
     * @param closingHour
     * @param idType
     */
    public VaccinationCenterDTO(String name, String address, int phoneNumber, String email, int faxNumber, String website, Time slotDuration, int maxVaccines, Time openingHour, Time closingHour, String idType) {
        this.name = name;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.faxNumber = faxNumber;
        this.website = website;
        this.slotDuration = slotDuration;
        this.maxVaccines = maxVaccines;
        this.openingHour = openingHour;
        this.closingHour = closingHour;
        this.idType = idType;
    }


    @Override
    public String toString() {
        return "VaccinationCenterDTO{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", phoneNumber=" + phoneNumber +
                ", email='" + email + '\'' +
                ", faxNumber=" + faxNumber +
                ", website='" + website + '\'' +
                ", slotDuration=" + slotDuration +
                ", maxVaccines=" + maxVaccines +
                ", openingHour=" + openingHour +
                ", closingHour=" + closingHour +
                ", idType='" + idType + '\'' +
                ", schedulesWaitingRoom=" + schedulesWaitingRoom +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VaccinationCenterDTO that = (VaccinationCenterDTO) o;
        return phoneNumber == that.phoneNumber && faxNumber == that.faxNumber && address.equals(that.address) && email.equals(that.email) && website.equals(that.website);
    }

    @Override
    public int hashCode() {
        return Objects.hash(address, phoneNumber, email, faxNumber, website);
    }
}
