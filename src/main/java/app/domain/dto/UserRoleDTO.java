package app.domain.dto;

import java.io.Serializable;
import java.util.Objects;

public class UserRoleDTO implements Serializable {

    /**
     * id
     */
    public String id;

    /**
     * description
     */
    public String description;

    /**
     * constructor with parameters
     *
     * @param id
     * @param description
     */
    public UserRoleDTO(String id, String description) {
        this.id = id;
        this.description = description;
    }

    @Override
    public String toString() {
        return "UserRoleDTO{" +
                "id='" + id + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserRoleDTO that = (UserRoleDTO) o;
        return id.equals(that.id) && description.equals(that.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, description);
    }
}
