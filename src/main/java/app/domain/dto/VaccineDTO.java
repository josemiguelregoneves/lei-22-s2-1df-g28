package app.domain.dto;

import app.domain.conceptual.AdministrationProcess;
import app.domain.conceptual.VaccineType;

import java.io.Serializable;
import java.util.Objects;

public class VaccineDTO implements Serializable {

    /**
     * vaccine type
     */
    public VaccineType type;

    /**
     * administration process
     */
    public AdministrationProcess administration;

    /**
     * name
     */
    public String name;

    /**
     * constructor with parameters
     *
     * @param type
     * @param administration
     * @param name
     */
    public VaccineDTO(VaccineType type, AdministrationProcess administration, String name) {
        this.type = type;
        this.administration = administration;
        this.name = name;
    }

    @Override
    public String toString() {
        return "VaccineDTO{" +
                "type=" + type +
                ", administration=" + administration +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VaccineDTO that = (VaccineDTO) o;
        return type.equals(that.type) && name.equals(that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, name);
    }
}
