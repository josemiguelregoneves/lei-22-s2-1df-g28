package app.domain.model;

import app.controller.App;
import app.domain.conceptual.Client;
import app.domain.shared.Constants;
import app.domain.interfaces.CSVVariations;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class RegisterCSVWithHeader implements CSVVariations {

    /**
     * method to read cliens from a .csv file with a header
     *
     * @param file File loaded to the program
     *
     * @throws FileNotFoundException
     */
    @Override
    public void readFile(File file) throws FileNotFoundException {
        Scanner sc = new Scanner(file);
        String line;
        sc.nextLine();
        while (sc.hasNextLine()) {
            line = sc.nextLine();
            String[] c = line.split(";");

            int ccNumber = Integer.parseInt(c[7]);

            String name = c[0];

            String address = c[3];

            String sex;
            if (c[1].equals("")) {
                sex = "Undefined";
            } else {
                sex = c[1];
            }

            String[] arrBirthDate = c[2].split("/");
            int day = Integer.parseInt(arrBirthDate[0]);
            int month = Integer.parseInt(arrBirthDate[1]);
            int year = Integer.parseInt(arrBirthDate[2]);
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
            Date birthDate = null;
            try {
                birthDate = format.parse(day + "/" + month + "/" + year);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            int phoneNumber = Integer.parseInt(c[4]);

            String email = c[5];

            int snsNumber = Integer.parseInt(c[6]);

            Client snsUser = Company.getClientStore().createClient(ccNumber, name, address, sex, birthDate, phoneNumber, email, snsNumber);
            Company.getClientStore().saveClient(snsUser);
            App.getInstance().getAuthFacade().addUserWithRole(snsUser.getName(), snsUser.getEmail(), snsUser.getPassword(), Constants.ROLE_SNS_USER);
        }
        sc.close();
    }
}
