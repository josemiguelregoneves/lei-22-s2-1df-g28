package app.domain.model;

import app.domain.conceptual.Client;
import app.domain.conceptual.VaccinationCenter;
import app.domain.conceptual.VaccineSchedule;
import app.domain.shared.Validator;
import app.domain.us017.ClientStatusStore;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Time;
import java.util.Date;
import java.util.Scanner;

public class ReadClientSchedulesFromFile {

    /**
     * method to read all the client status from .csv file
     *
     * @param file
     * @throws FileNotFoundException
     */
    public static void readClientsFromFile(VaccinationCenter center, File file) throws FileNotFoundException {
        center.getCompletedSchedulesList().clear();
        ClientStatusStore clientStatusStore = Company.getStatusStore();
        Scanner read = new Scanner(file);
        read.nextLine();
        while (read.hasNextLine()) {
            String[] arr = read.nextLine().split(";");

            int snsNumber = Integer.parseInt(arr[0]);
            String vcName = arr[1];
            String dose = arr[2];
            String lot = arr[3];

            String[] arrSchedule = arr[4].split(" ");
            String[] arrDate = arrSchedule[0].split("/");
            int month = Integer.parseInt(arrDate[0]);
            int day = Integer.parseInt(arrDate[1]);
            int year = Integer.parseInt(arrDate[2]);
            Date scheduleDate = new Date(year, month, day);
            String[] arrTimeSchedule = arrSchedule[1].split(":");
            int hourSchedule = Integer.parseInt(arrTimeSchedule[0]);
            int minuteSchedule = Integer.parseInt(arrTimeSchedule[1]);
            int secondSchedule = 0;
            String strTimeSchedule = String.format("%d:%d:%d", hourSchedule, minuteSchedule, secondSchedule);
            Time scheduleTime = Time.valueOf(strTimeSchedule);

            String[] arrArrival = arr[5].split(" ");
            String[] arrTimeArrival = arrArrival[1].split(":");
            int hourArrival = Integer.parseInt(arrTimeArrival[0]);
            int minuteArrival = Integer.parseInt(arrTimeArrival[1]);
            int secondArrival = 0;
            String strTimeArrival = String.format("%d:%d:%d", hourArrival, minuteArrival, secondArrival);
            Time arrivalTime = Time.valueOf(strTimeArrival);


            String[] arrAdmin = arr[6].split(" ");
            String[] arrTimeAdmin = arrAdmin[1].split(":");
            int hourAdmin = Integer.parseInt(arrTimeAdmin[0]);
            int minuteAdmin = Integer.parseInt(arrTimeAdmin[1]);
            int secondAdmin = 0;
            String strTimeAdmin = String.format("%d:%d:%d", hourAdmin, minuteAdmin, secondAdmin);
            Time adminTime = Time.valueOf(strTimeAdmin);

            String[] arrLeave = arr[7].split(" ");
            String[] arrTimeLeave = arrLeave[1].split(":");
            int hourLeave = Integer.parseInt(arrTimeLeave[0]);
            int minuteLeave = Integer.parseInt(arrTimeLeave[1]);
            int secondLeave = 0;
            String strTimeLeave = String.format("%d:%d:%d", hourLeave, minuteLeave, secondLeave);
            Time leaveTime = Time.valueOf(strTimeLeave);

            VaccineSchedule schedule = new VaccineSchedule(snsNumber, vcName, dose, lot, scheduleDate,scheduleTime, arrivalTime, adminTime, leaveTime);
            Client snsUser = Validator.getClientBySnsNumber(snsNumber);

            if (Validator.existsClientWithSnsNumber(snsNumber) && Validator.existsVaccineName(vcName)) {
                center.getCompletedSchedulesList().add(schedule);
                snsUser.getCompletedList().add(schedule);
                Company.getScheduleStore().getCompletedSchedulesList().add(schedule);
            }
        }
    }

}
