package app.domain.model;

import app.domain.conceptual.Client;
import app.domain.conceptual.Vaccine;
import app.domain.conceptual.VaccineSchedule;

import java.sql.Time;
import java.util.Date;

public class CompletedAdministration {

    private VaccineSchedule schedule;

    private Vaccine vaccine;

    public CompletedAdministration(VaccineSchedule schedule, Vaccine vaccine) {
        this.schedule = schedule;
        this.vaccine = vaccine;
    }

    public VaccineSchedule getSchedule() {
        return schedule;
    }

    public void setSchedule(VaccineSchedule schedule) {
        this.schedule = schedule;
    }

    public Vaccine getVaccine() {
        return vaccine;
    }

    public void setVaccine(Vaccine vaccine) {
        this.vaccine = vaccine;
    }

}
