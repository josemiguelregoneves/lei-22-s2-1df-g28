package app.domain.model;

public class VaccinationCenterType {
    /**
     * idType
     */
    private String idType;

    /**
     * description
     */
    private String description;

    /**
     * constructor with parameters
     *
     * @param idType
     * @param description
     */
    public VaccinationCenterType(String idType, String description) {
        this.idType = idType;
        this.description = description;
    }

    /**
     * method to get idType
     *
     * @return idType
     */
    public String getIdType() {
        return idType;
    }

    /**
     * method to set idType value
     *
     * @param idType
     */
    public void setIdType(String idType) {
        this.idType = idType;
    }

    /**
     * method to get description
     *
     * @return description
     */
    public String getDescription() {
        return description;
    }

    /**
     * method to set description value
     *
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }
}
