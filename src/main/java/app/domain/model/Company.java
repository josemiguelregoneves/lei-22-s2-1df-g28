package app.domain.model;

import app.domain.mapper.*;
import app.domain.store.*;
import app.domain.us017.ClientStatusStore;
import pt.isep.lei.esoft.auth.AuthFacade;

public class Company {

    /**
     * designation
     */
    private String designation;

    /**
     * authfacade
     */
    private static AuthFacade authFacade;

    /**
     * employee role store
     */
    private static EmployeeRoleStore rStore;

    /**
     * employee store
     */
    private static EmployeeStore eStore;

    /**
     * user role store
     */
    private static UserRoleStore roleStore;

    /**
     * client store
     */
    private static ClientStore clientStore;

    /**
     * center store
     */
    private static VaccinationCenterStore centerStore;

    /**
     * vaccine store
     */
    private static VaccineStore vcStore;

    /**
     * vaccine type store
     */
    private static VaccineTypeStore vcTypeStore;

    /**
     * schedules store
     */
    private static VaccineScheduleStore scheduleStore;

    /**
     * administration process store
     */
    private static AdministrationProcessStore adminProcessStore;

    /**
     * age range store
     */
    private static AgeRangeStore ageRangeStore;

    /**
     * adverse reactions store
     */
    private static AdverseReactionsStore reactionsStore;

    /**
     * client status store
     */
    private static ClientStatusStore statusStore;

    /**
     * vaccine schedule mapper
     */
    private static VaccineScheduleMapper vcScheduleMapper;

    /**
     * vaccine mapper
     */
    private static VaccineMapper vcMapper;

    /**
     * employee mapper
     */
    private static EmployeeMapper eMapper;

    /**
     * center mapper
     */
    private static VaccinationCenterMapper centerMapper;

    /**
     * client mapper
     */
    private static ClientMapper snsUserMapper;

    /**
     * vctype mapper
     */
    private static VaccineTypeMapper vcTypeMapper;

    /**
     * user role mapper
     */
    private static UserRoleMapper roleMapper;

    /**
     * list client mapper
     */
    private static ListClientMapper listClientMapper;

    /**
     * list center mapper
     */
    private static ListCenterMapper listCenterMapper;

    /**
     * list schedules mapper
     */
    private static ListSchedulesMapper listSchedulesMapper;

    /**
     * list vaccines mapper
     */
    private static ListVaccinesMapper listVaccineMapper;

    public Company(String designation) {
        if (designation.isEmpty()) {
            throw new IllegalArgumentException("Designation cannot be blank.");
        }
        this.designation = designation;
        authFacade = new AuthFacade();

        rStore = new EmployeeRoleStore();
        eStore = new EmployeeStore();
        clientStore = new ClientStore();
        centerStore = new VaccinationCenterStore();
        vcStore = new VaccineStore();
        vcTypeStore = new VaccineTypeStore();
        scheduleStore = new VaccineScheduleStore();
        roleStore = new UserRoleStore();
        adminProcessStore = new AdministrationProcessStore();
        ageRangeStore = new AgeRangeStore();
        reactionsStore = new AdverseReactionsStore();
        statusStore = new ClientStatusStore();

        vcScheduleMapper = new VaccineScheduleMapper();
        vcMapper = new VaccineMapper();
        vcTypeMapper = new VaccineTypeMapper();
        eMapper = new EmployeeMapper();
        centerMapper = new VaccinationCenterMapper();
        snsUserMapper = new ClientMapper();
        roleMapper = new UserRoleMapper();
        listClientMapper = new ListClientMapper();
        listCenterMapper = new ListCenterMapper();
        listSchedulesMapper = new ListSchedulesMapper();
        listVaccineMapper = new ListVaccinesMapper();
    }

    /**
     * method to get the designation
     *
     * @return designation
     */
    public String getDesignation() {
        return designation;
    }

    /**
     * method to get the authFacade
     *
     * @return authFacade
     */
    public static AuthFacade getAuthFacade() {
        return authFacade;
    }

    /**
     * method to get the rStore
     *
     * @return rStore
     */
    public static EmployeeRoleStore getrStore() {
        return rStore;
    }

    /**
     * method to get the eStore
     *
     * @return eStore
     */
    public static EmployeeStore geteStore() {
        return eStore;
    }

    /**
     * method to get the roleStore
     *
     * @return roleStore
     */
    public static UserRoleStore getRoleStore() {
        return roleStore;
    }

    /**
     * method to get the clientStore
     *
     * @return clientStore
     */
    public static ClientStore getClientStore() {
        return clientStore;
    }

    /**
     * method to get the centerStore
     *
     * @return centerStore
     */
    public static VaccinationCenterStore getCenterStore() {
        return centerStore;
    }

    /**
     * method to get the vcStore
     *
     * @return vcStore
     */
    public static VaccineStore getVcStore() {
        return vcStore;
    }

    /**
     * method to get the vcTypeStore
     *
     * @return vcTypeStore
     */
    public static VaccineTypeStore getVcTypeStore() {
        return vcTypeStore;
    }

    /**
     * method to get the scheduleStore
     *
     * @return scheduleStore
     */
    public static VaccineScheduleStore getScheduleStore() {
        return scheduleStore;
    }

    /**
     * method to get the adminProcessStore
     *
     * @return adminProcessStore
     */
    public static AdministrationProcessStore getAdminProcessStore() {
        return adminProcessStore;
    }

    /**
     * method to get the ageRangeStore
     *
     * @return ageRangeStore
     */
    public static AgeRangeStore getAgeRangeStore() {
        return ageRangeStore;
    }

    /**
     * method to get the reactionsStore
     *
     * @return reactionsStore
     */
    public static AdverseReactionsStore getReactionsStore() {
        return reactionsStore;
    }

    /**
     * method to get the client status store
     *
     * @return statusStore
     */
    public static ClientStatusStore getStatusStore() {
        return statusStore;
    }

    /**
     * method to get the vcScheduleMapper
     *
     * @return vcScheduleMapper
     */
    public static VaccineScheduleMapper getVcScheduleMapper() {
        return vcScheduleMapper;
    }

    /**
     * method to get the vcMapper
     *
     * @return vcMapper
     */
    public static VaccineMapper getVcMapper() {
        return vcMapper;
    }

    /**
     * method to get the eMapper
     *
     * @return eMapper
     */
    public static EmployeeMapper geteMapper() {
        return eMapper;
    }

    /**
     * method to get the centerMapper
     *
     * @return centerMapper
     */
    public static VaccinationCenterMapper getCenterMapper() {
        return centerMapper;
    }

    /**
     * method to get the snsUserMapper
     *
     * @return snsUserMapper
     */
    public static ClientMapper getSnsUserMapper() {
        return snsUserMapper;
    }

    /**
     * method to get the vcTypeMapper
     *
     * @return vcTypeMapper
     */
    public static VaccineTypeMapper getVcTypeMapper() {
        return vcTypeMapper;
    }

    /**
     * method to get the roleMapper
     *
     * @return roleMapper
     */
    public static UserRoleMapper getRoleMapper() {
        return roleMapper;
    }

    /**
     * method to get the listClientMapper
     *
     * @return listClientMapper
     */
    public static ListClientMapper getListClientMapper() {
        return listClientMapper;
    }

    /**
     * method to get the listCenterMapper
     *
     * @return listCenterMapper
     */
    public static ListCenterMapper getListCenterMapper() {
        return listCenterMapper;
    }

    /**
     * method to get the listSchedulesMapper
     *
     * @return listSchedulesMapper
     */
    public static ListSchedulesMapper getListSchedulesMapper() {
        return listSchedulesMapper;
    }

    /**
     * method to get the listVaccineMapper
     *
     * @return listVaccineMapper
     */
    public static ListVaccinesMapper getListVaccineMapper() {
        return listVaccineMapper;
    }

    /**
     * method to set the designation
     *
     * @param designation
     */
    public void setDesignation(String designation) {
        this.designation = designation;
    }

    /**
     * method to set the authFacade
     *
     * @param authFacade
     */
    public static void setAuthFacade(AuthFacade authFacade) {
        Company.authFacade = authFacade;
    }

    /**
     * method to set the rStore
     *
     * @param rStore
     */
    public static void setrStore(EmployeeRoleStore rStore) {
        Company.rStore = rStore;
    }

    /**
     * method to set the eStore
     *
     * @param eStore
     */
    public static void seteStore(EmployeeStore eStore) {
        Company.eStore = eStore;
    }

    /**
     * method to set the roleStore
     *
     * @param roleStore
     */
    public static void setRoleStore(UserRoleStore roleStore) {
        Company.roleStore = roleStore;
    }

    /**
     * method to set the clientStore
     *
     * @param clientStore
     */
    public static void setClientStore(ClientStore clientStore) {
        Company.clientStore = clientStore;
    }

    /**
     * method to set the centerStore
     *
     * @param centerStore
     */
    public static void setCenterStore(VaccinationCenterStore centerStore) {
        Company.centerStore = centerStore;
    }

    /**
     * method to set the vcStore
     *
     * @param vcStore
     */
    public static void setVcStore(VaccineStore vcStore) {
        Company.vcStore = vcStore;
    }

    /**
     * method to set the vcTypeStore
     *
     * @param vcTypeStore
     */
    public static void setVcTypeStore(VaccineTypeStore vcTypeStore) {
        Company.vcTypeStore = vcTypeStore;
    }

    /**
     * method to set the scheduleStore
     *
     * @param scheduleStore
     */
    public static void setScheduleStore(VaccineScheduleStore scheduleStore) {
        Company.scheduleStore = scheduleStore;
    }

    /**
     * method to set the adminProcessStore
     *
     * @param adminProcessStore
     */
    public static void setAdminProcessStore(AdministrationProcessStore adminProcessStore) {
        Company.adminProcessStore = adminProcessStore;
    }

    /**
     * method to set the ageRangeStore
     *
     * @param ageRangeStore
     */
    public static void setAgeRangeStore(AgeRangeStore ageRangeStore) {
        Company.ageRangeStore = ageRangeStore;
    }

    /**
     * method to set the reactionsStore
     *
     * @param reactionsStore
     */
    public static void setReactionsStore(AdverseReactionsStore reactionsStore) {
        Company.reactionsStore = reactionsStore;
    }

    /**
     * method to set the client status store
     *
     * @param statusStore
     */
    public static void setStatusStore(ClientStatusStore statusStore) {
        Company.statusStore = statusStore;
    }

    /**
     * method to set the vcScheduleMapper
     *
     * @param vcScheduleMapper
     */
    public static void setVcScheduleMapper(VaccineScheduleMapper vcScheduleMapper) {
        Company.vcScheduleMapper = vcScheduleMapper;
    }

    /**
     * method to set the vcMapper
     *
     * @param vcMapper
     */
    public static void setVcMapper(VaccineMapper vcMapper) {
        Company.vcMapper = vcMapper;
    }

    /**
     * method to set the eMapper
     *
     * @param eMapper
     */
    public static void seteMapper(EmployeeMapper eMapper) {
        Company.eMapper = eMapper;
    }

    /**
     * method to set the centerMapper
     *
     * @param centerMapper
     */
    public static void setCenterMapper(VaccinationCenterMapper centerMapper) {
        Company.centerMapper = centerMapper;
    }

    /**
     * method to set the snsUserMapper
     *
     * @param snsUserMapper
     */
    public static void setSnsUserMapper(ClientMapper snsUserMapper) {
        Company.snsUserMapper = snsUserMapper;
    }

    /**
     * method to set the vcTypeMapper
     *
     * @param vcTypeMapper
     */
    public static void setVcTypeMapper(VaccineTypeMapper vcTypeMapper) {
        Company.vcTypeMapper = vcTypeMapper;
    }

    /**
     * method to set the roleMapper
     *
     * @param roleMapper
     */
    public static void setRoleMapper(UserRoleMapper roleMapper) {
        Company.roleMapper = roleMapper;
    }

    /**
     * method to set the listClientMapper
     *
     * @param listClientMapper
     */
    public static void setListClientMapper(ListClientMapper listClientMapper) {
        Company.listClientMapper = listClientMapper;
    }

    /**
     * method to set the listCenterMapper
     *
     * @param listCenterMapper
     */
    public static void setListCenterMapper(ListCenterMapper listCenterMapper) {
        Company.listCenterMapper = listCenterMapper;
    }

    /**
     * method to set the listSchedulesMapper
     *
     * @param listSchedulesMapper
     */
    public static void setListSchedulesMapper(ListSchedulesMapper listSchedulesMapper) {
        Company.listSchedulesMapper = listSchedulesMapper;
    }

    /**
     * method to set the listVaccineMapper
     *
     * @param listVaccineMapper
     */
    public static void setListVaccineMapper(ListVaccinesMapper listVaccineMapper) {
        Company.listVaccineMapper = listVaccineMapper;
    }
}