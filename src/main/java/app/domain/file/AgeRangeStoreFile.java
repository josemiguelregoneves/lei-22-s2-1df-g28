package app.domain.file;

import app.domain.conceptual.AgeRange;
import app.domain.conceptual.Client;
import app.domain.model.Company;
import app.domain.shared.Alert;
import app.domain.store.AdverseReactionsStore;
import app.domain.store.AgeRangeStore;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class AgeRangeStoreFile {

    /**
     * path of the file where the system data will be saved
     */
    private static final String FILE_PATH = "serialize-binary/AgeRangeStoreBinary.ser";

    /**
     * method to serialize the data to the file
     */
    public void serialize() {
        try {
            FileOutputStream out = new FileOutputStream(FILE_PATH);
            ObjectOutputStream objOut = new ObjectOutputStream(out);
            objOut.writeObject(Company.getAgeRangeStore());
            out.close();
            objOut.close();
        } catch (Exception e) {
            Alert.message(e.getMessage());
        }
    }

    /**
     * method to load the data from the file
     *
     * @return store
     */
    public AgeRangeStore load() {
        AgeRangeStore rangeStore = null;
        try {
            FileInputStream in = new FileInputStream(FILE_PATH);
            ObjectInputStream objIn = new ObjectInputStream(in);
            rangeStore = (AgeRangeStore) objIn.readObject();
            in.close();
            objIn.close();
        } catch (Exception e) {
            Alert.message(e.getMessage());
        }
        return rangeStore;
    }

}
