package app.domain.file;

import app.domain.conceptual.AdministrationProcess;
import app.domain.model.Company;
import app.domain.shared.Alert;
import app.domain.store.AdministrationProcessStore;
import app.domain.store.EmployeeStore;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class AdminProcessStoreFile {

    /**
     * path of the file where the system data will be saved
     */
    private static final String FILE_PATH = "serialize-binary/AdminProcessStoreBinary.ser";

    /**
     * method to serialize the data to the file
     */
    public void serialize() {
        try {
            FileOutputStream out = new FileOutputStream(FILE_PATH);
            ObjectOutputStream objOut = new ObjectOutputStream(out);
            objOut.writeObject(Company.getAdminProcessStore());
            out.close();
            objOut.close();
        } catch (Exception e) {
            Alert.message(e.getMessage());
        }
    }

    /**
     * method to load the data from the file
     *
     * @return store
     */
    public AdministrationProcessStore load() {
        AdministrationProcessStore adminStore = null;
        try {
            FileInputStream in = new FileInputStream(FILE_PATH);
            ObjectInputStream objIn = new ObjectInputStream(in);
            adminStore = (AdministrationProcessStore) objIn.readObject();
            in.close();
            objIn.close();
        } catch (Exception e) {
            Alert.message(e.getMessage());
        }
        return adminStore;
    }

}
