package app.domain.file;

import app.domain.conceptual.Vaccine;
import app.domain.conceptual.VaccineSchedule;
import app.domain.model.Company;
import app.domain.shared.Alert;
import app.domain.store.VaccinationCenterStore;
import app.domain.store.VaccineScheduleStore;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class VaccineScheduleStoreFile {

    /**
     * path of the file where the system data will be saved
     */
    private static final String FILE_PATH = "serialize-binary/VaccineScheduleStoreBinary.ser";

    /**
     * method to serialize the data to the file
     */
    public void serialize() {
        try {
            FileOutputStream out = new FileOutputStream(FILE_PATH);
            ObjectOutputStream objOut = new ObjectOutputStream(out);
            objOut.writeObject(Company.getScheduleStore());
            out.close();
            objOut.close();
        } catch (Exception e) {
            Alert.message(e.getMessage());
        }
    }

    /**
     * method to load the data from the file
     *
     * @return store
     */
    public VaccineScheduleStore load() {
        VaccineScheduleStore scheduleStore = null;
        try {
            FileInputStream in = new FileInputStream(FILE_PATH);
            ObjectInputStream objIn = new ObjectInputStream(in);
            scheduleStore = (VaccineScheduleStore) objIn.readObject();
            in.close();
            objIn.close();
        } catch (Exception e) {
            Alert.message(e.getMessage());
        }
        return scheduleStore;
    }

}
