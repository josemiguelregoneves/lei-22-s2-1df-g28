package app.ui;

import app.domain.shared.Alert;
import app.ui.console.*;

/**
 * @author Paulo Maio <pam@isep.ipp.pt>
 */

public class Main {

    public static void main(String[] args) {
        try {
            MainMenuUI menu = new MainMenuUI();
            menu.run();
        } catch (Exception e) {
            Alert.message(e.getMessage());
        }
    }
}
