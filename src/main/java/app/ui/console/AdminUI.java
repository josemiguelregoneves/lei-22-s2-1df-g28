package app.ui.console;

import app.ui.console.list.*;
import app.ui.console.register.*;
import app.ui.console.serialize.AdminLoadDataUI;
import app.ui.console.serialize.AdminSerializeDataUI;
import app.ui.console.us006.VaccinatedUsersUI;
import app.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Paulo Maio <pam@isep.ipp.pt>
 */

public class AdminUI implements Runnable {

    private String email;
    private String password;

    public void run() {
        System.out.println("WELCOME TO ADMIN UI");

        AdminLoadDataUI loadUI = new AdminLoadDataUI();
        loadUI.run();

        VaccinatedUsersUI vuUI = new VaccinatedUsersUI();
        vuUI.run();

        List<MenuItem> options = new ArrayList<MenuItem>();
        options.add(new MenuItem("Register New Employee", new RegisterNewEmployeeUI()));
        options.add(new MenuItem("Register New SNS User", new RegisterClientUI()));
        options.add(new MenuItem("Register Vaccination Center", new RegisterNewVaccinationCenterUI()));
        options.add(new MenuItem("Register Vaccine", new RegisterNewVaccineUI()));
        options.add(new MenuItem("Register Vaccine Type", new RegisterVaccineTypeUI()));
        options.add(new MenuItem("Show Employee's List", new ShowEmployeesListUI()));
        options.add(new MenuItem("Show Center's List", new ShowCentersListUI()));
        options.add(new MenuItem("Show Vaccine's List", new ShowVaccinesListUI()));
        options.add(new MenuItem("Load CSV File", new UserRegistrationViaCSVUI()));
        options.add(new MenuItem("Show SNS Users List", new ListClientsUI()));
        options.add(new MenuItem("List All Schedules", new ListAllSchedulesUI()));
        options.add(new MenuItem("Serialize/Save Data in the System", new AdminSerializeDataUI()));

        int option = 0;
        do {
            option = Utils.showAndSelectIndex(options, "\n\nAdmin Menu:");

            if ((option >= 0) && (option < options.size())) {
                options.get(option).run();
            }
        }
        while (option != -1);
    }
}
