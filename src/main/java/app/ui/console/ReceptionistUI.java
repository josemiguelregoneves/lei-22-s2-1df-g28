package app.ui.console;

import app.domain.conceptual.Employee;
import app.domain.conceptual.VaccinationCenter;
import app.domain.model.*;
import app.domain.shared.Constants;
import app.ui.console.register.ReceptionistScheduleVaccineUI;
import app.ui.console.utils.Utils;

import java.util.ArrayList;

public class ReceptionistUI implements Runnable {

    private AuthUI authUI = new AuthUI();

    /**
     * email
     */
    private String email;

    /**
     * password
     */
    private String password;

    /**
     * Receptionist
     */

    private Employee receptionist;

    private VaccinationCenter center;

    /**
     * run method
     */
    @Override
    public void run() {
        System.out.println("WELCOME TO RECEPTIONIST UI");

        email = authUI.getEmail();
        password = authUI.getPassword();
        receptionist = getEmployeeByEmail(email);

        int exit = 0;
        do {
            ArrayList<VaccinationCenter> centerList = Company.getCenterStore().getCentersList();
            ArrayList<String> centerName = new ArrayList<>();
            for (VaccinationCenter center : centerList) {
                centerName.add(center.getName());
            }
            int option = Utils.showAndSelectIndex(centerName, "Select a Vaccination Center");
            center = centerList.get(option);

            receptionist.setCenter(center);

            receptionistMenu();

            exit = Utils.readIntegerFromConsole("Exit Receptionist Menu?\n1 - [Yes]\n2 - [No]\nR: ");
        } while (exit == 2);
    }

    /**
     * method to get an employee by his email
     *
     * @param email employee's email
     * @return employee
     */
    public Employee getEmployeeByEmail(String email) {
        for (Employee e : Company.geteStore().getEmployeeList()) {
            if (e.getIdRole().equals(Constants.ROLE_RECEPTIONIST) && e.getEmail().equals(email)) {
                return e;
            }
        }
        return null;
    }

    public void receptionistMenu() {
        System.out.println("1 - [Check the SNS User arrival]");
        System.out.println("2 - [Schedule a Vaccine for a SNS User]");

        int option = Utils.readIntegerFromConsole("Select an option:");

        switch (option) {
            case 1:
                ReceptionistChecksArrivalUI arrivalUI = new ReceptionistChecksArrivalUI(center);
                arrivalUI.run();
                break;
            case 2:
                ReceptionistScheduleVaccineUI recepSchedulesUI = new ReceptionistScheduleVaccineUI();
                recepSchedulesUI.run();
                break;
        }
    }
}
