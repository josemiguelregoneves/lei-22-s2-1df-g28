package app.ui.console;

import app.controller.us008.NurseAdminVaccineGUI;
import app.domain.conceptual.VaccinationCenter;
import app.domain.model.Company;
import app.domain.conceptual.Employee;
import app.domain.shared.Constants;
import app.ui.console.list.ListAdverseReactionsUI;
import app.ui.console.us007.RecordAdverseReactionsLaunch;
import app.ui.console.us008.NurseAdminVaccineLaunch;
import app.ui.console.list.ListWaitingRoomUI;
import app.ui.console.us008.NurseAdminVaccineUI;
import app.ui.console.utils.Utils;

import java.util.ArrayList;

public class NurseUI implements Runnable {

    /**
     * auth UI
     */
    private AuthUI authUI = new AuthUI();

    /**
     * email
     */
    private String email;

    /**
     * password
     */
    private String password;

    /**
     * nurse
     */
    private Employee nurse;

    /**
     * center
     */
    private VaccinationCenter center;

    /**
     * runnable method
     */
    @Override
    public void run() {
        System.out.println("WELCOME TO NURSE UI");

        email = authUI.getEmail();
        password = authUI.getPassword();
        nurse = getEmployeeByEmail(email);

        int exit = 0;
        do {
            ArrayList<VaccinationCenter> centerList = Company.getCenterStore().getCentersList();
            ArrayList<String> centerName = new ArrayList<>();
            for (VaccinationCenter center : centerList) {
                centerName.add(center.getName());
            }
            int option = Utils.showAndSelectIndex(centerName, "Select a Vaccination Center");
            center = centerList.get(option);

            nurse.setCenter(center);

            nurseMenu();

            exit = Utils.readIntegerFromConsole("Exit Nurse Menu?\n1 - [Yes]\n2 - [No]\nR: ");
        } while (exit == 2);
    }

    /**
     * method to get employee by emal
     *
     * @param email
     *
     * @return employee
     */
    public Employee getEmployeeByEmail(String email) {
        for (Employee e : Company.geteStore().getEmployeeList()) {
            if (e.getIdRole().equals(Constants.ROLE_NURSE) && e.getEmail().equals(email)) {
                return e;
            }
        }
        return null;
    }

    /**
     * nurse menu
     */
    public void nurseMenu() {
        System.out.println("1 - [List the Waiting Room of some center - UI]");
        System.out.println("2 - [Administer Vaccine - UI]");
        System.out.println("3 - [Administer Vaccine - GUI]");
        System.out.println("4 - [Record Adverse Reactions - GUI]");
        System.out.println("5 - [See All Registered adverse Reactions - UI]");

        int option = Utils.readIntegerFromConsole("Select an option:");

        switch (option) {
            case 1:
                ListWaitingRoomUI listRoomUI = new ListWaitingRoomUI(center);
                listRoomUI.run();
                break;
            case 2:
                NurseAdminVaccineUI recVcAdminUI = new NurseAdminVaccineUI(center);
                recVcAdminUI.run();
                break;
            case 3:
                NurseAdminVaccineGUI.setCenter(center);
                NurseAdminVaccineLaunch adminVcGUI = new NurseAdminVaccineLaunch();
                adminVcGUI.run();
                break;
            case 4:
                RecordAdverseReactionsLaunch reactionsGUI = new RecordAdverseReactionsLaunch();
                reactionsGUI.run();
                break;
            case 5:
                ListAdverseReactionsUI listReactionUI = new ListAdverseReactionsUI();
                listReactionUI.run();
                break;
        }
    }
}
