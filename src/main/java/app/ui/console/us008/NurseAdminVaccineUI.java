package app.ui.console.us008;

import app.controller.us008.NurseAdminVaccineController;
import app.domain.conceptual.*;
import app.domain.dto.ClientDTO;
import app.domain.dto.VaccineDTO;
import app.domain.dto.VaccineScheduleDTO;
import app.domain.mapper.*;
import app.domain.model.Company;
import app.domain.shared.Alert;
import app.domain.shared.Validator;
import app.ui.console.utils.Utils;

import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.*;

public class NurseAdminVaccineUI implements Runnable {

    /**
     * controller for admin vc by nurse
     */
    private final NurseAdminVaccineController ctrl;

    /**
     * mapper for clients
     */
    private final ClientMapper clientMap;

    /**
     * mapper for schedules
     */
    private final VaccineScheduleMapper scheduleMap;

    /**
     * mapper for vaccines
     */
    private final VaccineMapper vaccineMap;

    /**
     * mapper for lists of clients
     */
    private final ListClientMapper listClientMap;

    /**
     * mapper for lists of schedules
     */
    private final ListSchedulesMapper listScheduleMap;

    /**
     * mapper for lists of vaccines
     */
    private final ListVaccinesMapper listVaccineMap;

    /**
     * vaccination center
     */
    private VaccinationCenter center;

    /**
     * constructor with parameters
     *
     * @param center
     */
    public NurseAdminVaccineUI(VaccinationCenter center) {
        this.ctrl = new NurseAdminVaccineController();
        this.clientMap = Company.getSnsUserMapper();
        this.scheduleMap = Company.getVcScheduleMapper();
        this.vaccineMap = Company.getVcMapper();
        this.listClientMap = Company.getListClientMapper();
        this.listScheduleMap = Company.getListSchedulesMapper();
        this.listVaccineMap = Company.getListVaccineMapper();
        this.center = center;
    }

    /**
     * run method
     */
    @Override
    public void run() {
        System.out.println("<<<< VACCINE ADMINISTRATION >>>>");

        /**
         * get the available clients
         */
        ArrayList<ClientDTO> clientListDto = ctrl.getClientsList(center);
        ArrayList<Client> clientList = listClientMap.toModel(clientListDto);

        /**
         * select an client option
         */
        int clientOption = Utils.showAndSelectIndex(clientList, "Select an SNS User");

        /**
         * get the client by the selected option
         */
        ClientDTO clientDto = ctrl.selectClient(clientList, clientOption);
        Client client = clientMap.toModel(clientDto);

        /**
         * get the available schedules and select one
         */
        ArrayList<VaccineScheduleDTO> userSchedulesDto = ctrl.getSchedulesOfClient(client);
        ArrayList<VaccineSchedule> userSchedules = listScheduleMap.toModel(userSchedulesDto);

        /**
         * select an schedule option
         */
        int scheduleOption = Utils.showAndSelectIndex(userSchedules, "Select a Schedule");

        /**
         * get the schedule by the selected option
         */
        VaccineScheduleDTO scheduleDto = ctrl.selectSchedule(userSchedules, scheduleOption);
        VaccineSchedule schedule = scheduleMap.toModel(scheduleDto);

        /**
         * vaccine type to for the schedule
         */
        VaccineType vcType = schedule.getVcType();

        /**
         * get the available vaccines and select one
         */
        ArrayList<VaccineDTO> vcListDto = ctrl.getAvailableVaccinesForSchedule(schedule);
        ArrayList<Vaccine> vcList = listVaccineMap.toModel(vcListDto);

        /**
         * select an vaccine option
         */
        int vcOption = Utils.showAndSelectIndex(vcList, "Select a Vaccine for " + vcType.getName());

        /**
         * get the vaccine by the selected option
         */
        VaccineDTO vcDto = ctrl.selectVaccine(vcList, vcOption);
        Vaccine vc = vaccineMap.toModel(vcDto);

        if (Validator.validateNumberOfDoses(schedule) && Validator.validateTimeSinceLastVaccine(schedule)) {

            /**
             * method to show the selected schedule data
             */
            showScheduleData(schedule);

            int accept = Utils.readIntegerFromConsole("Do you want to administer this Vaccine to the SNS User?\n1 - [Yes]\n2 - [No]\nR: ");
            if (accept == 1) {
                System.out.println("Vaccine Administration Complete");

                /**
                 * send an sms after complete the administration
                 */
                try {
                    ctrl.sendAdminSms(schedule);
                } catch (FileNotFoundException e) {
                    Alert.message(e.getMessage());
                }

                /**
                 * record the vaccine administration data
                 */
                ctrl.recordVaccineAdmin(schedule, center, vc);

                if (Validator.clientFullVaccinated(schedule)) {
                    Company.getScheduleStore().getFullVaccinated().add(schedule);
                }

                /**
                 * send an sms after the end of the recovery period
                 */
                ctrl.sendLeavingSms(schedule);
            }
        }
    }

    /**
     * method to show the schedule data
     *
     * @param vcSchedule
     */
    private void showScheduleData(VaccineSchedule vcSchedule) {
        System.out.println("Scheduled vaccine:");
        System.out.println("SNS Number: " + Validator.getClientBySnsNumber(vcSchedule.getSnsNumber()).getName());
        System.out.println("Vaccine type: " + vcSchedule.getVcType().getName());
        System.out.println("Schedule Date: " + vcSchedule.getScheduleDate());
        System.out.println("Schedule Time: " + vcSchedule.getScheduleTime());
    }
}
