package app.ui.console.us008;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class NurseAdminVaccineLaunch extends Application implements Runnable {

    /**
     * method to start the stage
     *
     * @param stage
     *
     * @throws Exception
     */
    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader(NurseAdminVaccineLaunch.class.getResource("nurse-admin-vc.fxml"));
        fxmlLoader.setLocation(getClass().getClassLoader().getResource("nurse-admin-vc.fxml"));
        Scene scene = new Scene(fxmlLoader.load());
        stage.setTitle("Vaccine Administration");
        stage.setScene(scene);
        stage.setFullScreen(true);
        stage.setResizable(false);
        stage.show();
    }

    /**
     * method to run the scene
     */
    @Override
    public void run() {
        launch();
    }

}
