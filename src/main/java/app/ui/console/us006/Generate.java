package app.ui.console.us006;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Generate extends Application implements Runnable {

    /**
     * runnable method
     */
    @Override
    public void run() {
        launch();
    }

    /**
     * method to start the stage
     *
     * @param stage
     *
     * @throws Exception
     */
    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader(VaccinatedUsersUI.class.getResource("report_generated.fxml"));
        fxmlLoader.setLocation(getClass().getClassLoader().getResource("report_generated.fxml"));
        Scene scene = new Scene(fxmlLoader.load());
        stage.setTitle("Alert!");
        stage.setScene(scene);
        stage.show();
    }
}
