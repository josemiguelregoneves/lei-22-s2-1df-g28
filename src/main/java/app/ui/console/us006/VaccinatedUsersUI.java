package app.ui.console.us006;

import app.controller.javafx.VaccinatedUsersController;
import app.ui.console.us006.AlreadyGenerated;
import app.ui.console.us006.Generate;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

public class VaccinatedUsersUI implements Runnable {

    /**
     * Controller for vaccinated Users
     */
    private VaccinatedUsersController vuController;

    /**
     * array of the type Runnable that contains two classes
     */
    private final Runnable[] arr = {new AlreadyGenerated(), new Generate()};

    /**
     * constructor without parameters that will initialize the controller
     */
    public VaccinatedUsersUI() {
        this.vuController = new VaccinatedUsersController();
    }

    /**
     * Run method
     */
    @Override
    public void run() {

        String runTime = "";
        try {
            runTime = vuController.getConfigs();
        } catch (IOException e) {
            e.printStackTrace();
        }

        LocalTime time = LocalTime.parse(runTime);

        Calendar date = Calendar.getInstance();
        date.set(Calendar.HOUR_OF_DAY, time.getHour());
        date.set(Calendar.MINUTE, time.getMinute());
        date.set(Calendar.SECOND, time.getSecond());
        date.set(Calendar.MILLISECOND, 0);

        LocalDateTime dateNow = LocalDateTime.now();

        if (time.getHour() == dateNow.getHour() && time.getMinute() == dateNow.getMinute() && time.getSecond() < dateNow.getSecond() || time.getHour() == dateNow.getHour() && time.getMinute() < dateNow.getMinute() || time.getHour() < dateNow.getHour()) {
            arr[0].run();
        } else {
            Timer timer = new Timer();

            TimerTask task = new TimerTask() {
                @Override
                public void run() {
                    vuController.fileWritter();
                    timer.cancel();
                    arr[1].run();
                }
            };

            timer.schedule(task, date.getTime());
        }
    }
}
