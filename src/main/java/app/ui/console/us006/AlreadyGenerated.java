package app.ui.console.us006;

import app.domain.shared.Alert;

public class AlreadyGenerated implements Runnable{

    /**
     * runnable method
     */
    @Override
    public void run() {
        Alert.message("(US006) - Record Daily Vaccinated People by Center - The Data was Already Generated/Time has passed!");
    }
}
