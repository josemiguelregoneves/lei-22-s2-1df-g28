package app.ui.console.serialize;

import app.domain.conceptual.*;
import app.domain.file.*;
import app.domain.model.Company;
import app.ui.console.utils.Utils;

import java.util.ArrayList;

public class AdminSerializeDataUI implements Runnable{

    /**
     * employee store file
     */
    private final EmployeeStoreFile employeeSerialize;

    /**
     * client store file
     */
    private final ClientStoreFile clientSerialize;

    /**
     * vaccination center store file
     */
    private final VaccinationCenterStoreFile centerSerialize;

    /**
     * vaccine store file
     */
    private final VaccineStoreFile vaccineSerialize;

    /**
     * vaccine type store file
     */
    private final VaccineTypeStoreFile vcTypeSerialize;

    /**
     * vaccine schedule store file
     */
    private final VaccineScheduleStoreFile scheduleSerialize;

    /**
     * admin process store file
     */
    private final AdminProcessStoreFile adminSerialize;

    /**
     * age range store file
     */
    private final AgeRangeStoreFile ageRangeSerialize;

    /**
     * adverse reactions store file
     */
    private final AdverseReactionsStoreFile reactionsSerialize;

    /**
     * constructor that initialize the files
     */
    public AdminSerializeDataUI() {
        this.employeeSerialize = new EmployeeStoreFile();
        this.clientSerialize = new ClientStoreFile();
        this.centerSerialize = new VaccinationCenterStoreFile();
        this.vaccineSerialize = new VaccineStoreFile();
        this.vcTypeSerialize = new VaccineTypeStoreFile();
        this.scheduleSerialize = new VaccineScheduleStoreFile();
        this.adminSerialize = new AdminProcessStoreFile();
        this.ageRangeSerialize = new AgeRangeStoreFile();
        this.reactionsSerialize = new AdverseReactionsStoreFile();
    }

    /**
     * runnable method
     */
    @Override
    public void run() {

        int accept = Utils.readIntegerFromConsole("Serialize Data?\n1 - [Yes]\n2 - [No]\nR: ");

        if (accept == 1) {
            /**
             * serialize the employee store
             */
            employeeSerialize.serialize();

            /**
             * serialize the client store
             */
            clientSerialize.serialize();

            /**
             * serialize the vaccination center store
             */
            centerSerialize.serialize();

            /**
             * serialize the vaccine store
             */
            vaccineSerialize.serialize();

            /**
             * serialize the vaccine type store
             */
            vcTypeSerialize.serialize();

            /**
             * serialize the schedules store
             */
            scheduleSerialize.serialize();

            /**
             * serialize the administration process store
             */
            adminSerialize.serialize();

            /**
             * serialize the age range store
             */
            ageRangeSerialize.serialize();

            /**
             * serialize the ractions store
             */
            reactionsSerialize.serialize();

            System.out.println("Serialize Complete!");
        }

    }

}
