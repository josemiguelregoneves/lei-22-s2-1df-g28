package app.ui.console.us017;

import app.controller.us017.SortClientScheduleController;
import app.domain.conceptual.VaccinationCenter;
import app.domain.conceptual.VaccineSchedule;
import app.domain.shared.Alert;
import app.domain.shared.Constants;
import app.domain.us017.SortArrivalTime;
import app.domain.us017.SortLeavingTime;
import app.ui.console.utils.Utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;

public class SortClientScheduleUI implements Runnable {

    /**
     * controller for sort clients list
     */
    private SortClientScheduleController ctrl;

    /**
     * vaccination center
     */
    private VaccinationCenter center;

    /**
     * constructor with parameters
     *
     * @param center
     */
    public SortClientScheduleUI(VaccinationCenter center) {
        this.ctrl = new SortClientScheduleController();
        this.center = center;
    }

    /**
     * method to get the vaccination center
     *
     * @return center
     */
    public VaccinationCenter getCenter() {
        return center;
    }

    /**
     * method to set the vaccination center value
     *
     * @param center
     */
    public void setCenter(VaccinationCenter center) {
        this.center = center;
    }

    /**
     * runnable method
     */
    @Override
    public void run() {

        /**
         * create a list with two sort algorithms
         */
        ArrayList<String> algorithmList = new ArrayList<>();
        algorithmList.add(Constants.STR_BUBBLE);
        algorithmList.add(Constants.STR_SELECTION);

        /**
         * create a list with two parameters for sort
         */
        ArrayList<String> parameterList = new ArrayList<>();
        parameterList.add(Constants.STR_ARRIVAL);
        parameterList.add(Constants.STR_LEAVING);

        /**
         * create a list with two sort orders
         */
        ArrayList<String> orderList = new ArrayList<>();
        orderList.add(Constants.STR_ASCENDING);
        orderList.add(Constants.STR_DESCENDING);

        /**
         * get the algorithm option
         */
        int algorithmOp = Utils.showAndSelectIndex(algorithmList, "Select an Algorithm:");

        /**
         * get the parameter option
         */
        int parameterOp = Utils.showAndSelectIndex(parameterList, "Select an Parameter:");

        /**
         * get the order option
         */
        int orderOp = Utils.showAndSelectIndex(orderList, "Select an Order:");

        /**
         * get the algorithm
         */
        String algorithm = algorithmList.get(algorithmOp);

        /**
         * get the parameter
         */
        String parameter = parameterList.get(parameterOp);

        /**
         * get the order
         */
        String order = orderList.get(orderOp);

        /**
         * get the completed schedules list
         */
        ArrayList<VaccineSchedule> completedList = center.getCompletedSchedulesList();

        /**
         * convert the list to array and sort it
         */
        try {
            VaccineSchedule[] arrCompleted = completedList.toArray(new VaccineSchedule[0]);
            long time1 = System.nanoTime();
            ctrl.sortArrayListByArrivalLeave(algorithm, parameter, order, arrCompleted);
            long time2 = System.nanoTime();
            ctrl.showSortedList(arrCompleted);
            System.out.println(time2 - time1);
        } catch (FileNotFoundException e) {
            Alert.message(e.getMessage());
        }
    }
}
