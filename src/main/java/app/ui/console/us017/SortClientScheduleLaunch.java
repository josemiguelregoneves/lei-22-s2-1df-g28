package app.ui.console.us017;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class SortClientScheduleLaunch extends Application implements Runnable {
    /**
     * method to start the stage
     *
     * @param stage
     *
     * @throws Exception
     */
    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader(SortClientScheduleLaunch.class.getResource("sort-completed-schedules.fxml"));
        fxmlLoader.setLocation(getClass().getClassLoader().getResource("sort-completed-schedules.fxml"));
        Scene scene = new Scene(fxmlLoader.load());
        stage.setTitle("Sort Client Status");
        stage.setScene(scene);
        stage.setFullScreen(true);
        stage.show();
    }

    /**
     * method to run the scene
     */
    @Override
    public void run() {
        launch();
    }
}
