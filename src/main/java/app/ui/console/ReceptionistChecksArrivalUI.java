package app.ui.console;

import app.controller.ReceptionistChecksArrivalController;
import app.domain.conceptual.Client;
import app.domain.conceptual.Employee;
import app.domain.conceptual.VaccinationCenter;
import app.domain.conceptual.VaccineSchedule;
import app.domain.model.Company;
import app.domain.shared.Alert;
import app.domain.shared.Validator;
import app.ui.console.utils.Utils;

import java.sql.Time;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class ReceptionistChecksArrivalUI implements Runnable {

    private VaccinationCenter center;

    private ArrayList<VaccineSchedule> schedulesWaitingRoom;

    private ReceptionistChecksArrivalController arrivalController = new ReceptionistChecksArrivalController();

    public ReceptionistChecksArrivalUI(VaccinationCenter center) {
        this.center = center;
    }

    private VaccineSchedule schedule;

    @Override
    public void run() {

        int op = 0;
        do {
            int snsNumber = Utils.readIntegerFromConsole("Enter a SNS Number: ");

            if (!Validator.validateSnsNumber(snsNumber)) {
                do {
                    snsNumber = Utils.readIntegerFromConsole("Invalid SNS Number. Type a new one (9 digits): ");
                } while (!Validator.validateSnsNumber(snsNumber));
            }

            schedule = arrivalController.getScheduleBySnsNumber(snsNumber);

            if (Validator.dateEqualsCurrentDate(schedule.getScheduleDate()) && schedule.getArrivalTime() == null && !center.getSchedulesWaitingRoom().contains(schedule)) {
                schedule.setArrivalTime(Validator.getCurrentHour());
                center.getSchedulesWaitingRoom().add(schedule);
                Company.getScheduleStore().getWaitingSchedulesList().add(schedule);
            } else {
                Alert.message("Invalid Date");
            }

            op = Utils.readIntegerFromConsole("New Arrival Check?\n1 - [Yes]\n2 - [No]\nR: ");
        } while (op == 1);
    }
}
