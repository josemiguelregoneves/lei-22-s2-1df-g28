package app.ui.console.list;

import app.domain.conceptual.Client;
import app.domain.conceptual.VaccineSchedule;
import app.domain.model.Company;

public class ListClientsUI implements Runnable {
    @Override
    public void run() {

        for (Client snsUser : Company.getClientStore().getClientList()) {
            System.out.println("Name: " + snsUser.getName());
            System.out.println("SNS Number: " + snsUser.getSnsNumber());
            System.out.println("Email: " + snsUser.getEmail());
            System.out.println("Password: " + snsUser.getPassword());

            System.out.println("SCHEDULES");
            for (VaccineSchedule schedule : snsUser.getSchedules()) {
                System.out.println(schedule);
            }

            System.out.println("COMPLETED SCHEDULES");
            for (VaccineSchedule schedule : snsUser.getCompletedList()) {
                System.out.println(schedule);
            }

            System.out.println();
        }

    }
}
