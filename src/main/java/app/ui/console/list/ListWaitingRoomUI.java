package app.ui.console.list;

import app.controller.list.ListWaitingRoomController;
import app.domain.conceptual.Client;
import app.domain.conceptual.VaccinationCenter;
import app.domain.conceptual.VaccineSchedule;
import app.domain.dto.ClientDTO;
import app.domain.dto.VaccinationCenterDTO;
import app.domain.dto.VaccineScheduleDTO;
import app.domain.mapper.ListCenterMapper;
import app.domain.mapper.ListClientMapper;
import app.domain.mapper.ListSchedulesMapper;
import app.domain.mapper.VaccinationCenterMapper;
import app.domain.model.Company;
import app.domain.shared.Alert;
import app.ui.console.utils.Utils;

import java.util.ArrayList;

public class ListWaitingRoomUI implements Runnable {

    private VaccinationCenter center;

    private ListWaitingRoomController listWaitingRoomCtrl = new ListWaitingRoomController();

    private ListSchedulesMapper listScheduleMapper = Company.getListSchedulesMapper();

    private ListCenterMapper listCenterMapper = Company.getListCenterMapper();

    private ListClientMapper listClientMapper = Company.getListClientMapper();

    public ListWaitingRoomUI(VaccinationCenter center) {
        this.center = center;
    }

    @Override
    public void run() {

        ArrayList<VaccineScheduleDTO> schedulesWaitingDto = listWaitingRoomCtrl.getSchedulesWaiting(center);
        ArrayList<VaccineSchedule> schedulesWaiting = listScheduleMapper.toModel(schedulesWaitingDto);
        ArrayList<ClientDTO> waitingRoomDto = listWaitingRoomCtrl.getWaitingRoom(schedulesWaiting);
        ArrayList<Client> waitingRoom = listClientMapper.toModel(waitingRoomDto);
        showWaitingRoom(waitingRoom);
    }

    /**
     * method to show the waiting room data
     *
     * @param waitingRoom
     */
    private void showWaitingRoom(ArrayList<Client> waitingRoom) {
        if (waitingRoom == null || waitingRoom.isEmpty()) {
            Alert.message("Waiting Room is Empty");
        } else {
            int index = 1;
            for (Client snsUser : waitingRoom) {
                System.out.println("SNS USER Nº" + index);
                System.out.println("Name: " + snsUser.getName());
                System.out.println("Sex: " + snsUser.getSex());
                System.out.println("Birth Date: " + snsUser.getBirthDate());
                System.out.println("SNS Number: " + snsUser.getSnsNumber());
                System.out.println("Phone Number: " + snsUser.getPhoneNumber());
                System.out.println();
                index++;
            }
        }
    }
}
