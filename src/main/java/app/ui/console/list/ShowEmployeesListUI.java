package app.ui.console.list;

import app.controller.list.ShowEmployeesListController;
import app.domain.shared.Alert;
import app.domain.shared.Constants;
import app.ui.console.utils.Utils;

public class ShowEmployeesListUI implements Runnable {
    /**
     * controller for show the employee's list
     */
    private ShowEmployeesListController eShowController = new ShowEmployeesListController();

    /**
     * run method
     */
    @Override
    public void run() {
        int option = Utils.readIntegerFromConsole("Choose an option:\n1 - [Show all employees]\n2 - [Show employees with a specific role]\nR: ");
        switch (option) {
            case 1:
                eShowController.showEmployeeList(eShowController.getEmployeeList());
                break;
            case 2:
                int role = Utils.readIntegerFromConsole("What's the role?\n1 - [NURSE]\n2 - [RECEPTIONIST]\n3 - [COORDINATOR]\nR: ");
                eShowController.showEmployeeList(eShowController.getEmployeeListWithRole(convertOptionToRole(role)));
                break;
            default:
                Alert.message("Invalid option");
        }
    }

    /**
     * convert an int option to the respective role
     *
     * @param option
     * @return role
     */
    private String convertOptionToRole(int option) {
        String role = "";
        switch (option) {
            case 1:
                role = Constants.ROLE_NURSE;
                break;
            case 2:
                role = Constants.ROLE_RECEPTIONIST;
                break;
            case 3:
                role = Constants.ROLE_COORDINATOR;
                break;
            default:
                Alert.message("Invalid option");
        }
        return role;
    }
}
