package app.ui.console.list;

import app.domain.conceptual.AdverseReactions;
import app.domain.model.Company;

public class ListAdverseReactionsUI implements Runnable {

    @Override
    public void run() {
        for (AdverseReactions reaction : Company.getReactionsStore().getReactionsList()) {
            System.out.println(reaction);
        }
    }
}
