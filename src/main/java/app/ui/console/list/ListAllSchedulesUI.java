package app.ui.console.list;

import app.domain.conceptual.VaccineSchedule;
import app.domain.model.Company;
import app.domain.shared.Validator;

public class ListAllSchedulesUI implements Runnable {

    @Override
    public void run() {
        for (VaccineSchedule schedule : Company.getScheduleStore().getVcScheduleList()) {
            System.out.println("Name: " + Validator.getClientBySnsNumber(schedule.getSnsNumber()));
            System.out.println("Date: " + schedule.getScheduleDate());
            System.out.println("Time: " + schedule.getScheduleTime());
            System.out.println("Vaccine Type: " + schedule.getVcType().getName());
            System.out.println();
        }
    }

}
