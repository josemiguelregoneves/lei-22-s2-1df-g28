package app.ui.console.list;

import app.controller.list.ShowCentersListController;
import app.domain.shared.Alert;
import app.domain.shared.Constants;
import app.ui.console.utils.Utils;

public class ShowCentersListUI implements Runnable {
    /**
     * controller for show the employee's list
     */
    private ShowCentersListController vcShowController = new ShowCentersListController();

    /**
     * run method
     */
    @Override
    public void run() {
        int option = Utils.readIntegerFromConsole("Choose an option:\n1 - [Show all centers]\n2 - [Show centers of a specific type]\nR: ");
        switch (option) {
            case 1:
                vcShowController.showCenterList(vcShowController.getCenterList());
                break;
            case 2:
                int type = Utils.readIntegerFromConsole("What's the type?\n1 - [HEALTH CARE CENTER]\n2 - [COMUNITY MASS CENTER]\nR: ");
                vcShowController.showCenterList(vcShowController.getCenterListWithType(convertOptionToRole(type)));
                break;
            default:
                Alert.message("Invalid option");
        }
    }

    /**
     * convert an int option to the respective role
     *
     * @param option
     * @return type
     */
    private String convertOptionToRole(int option) {
        String type = "";
        switch (option) {
            case 1:
                type = Constants.TYPE_HEALTH_CARE_CENTER;
                break;
            case 2:
                type = Constants.TYPE_COMUNITY_MASS_CENTER;
                break;
            default:
                Alert.message("Invalid option");
        }
        return type;
    }
}
