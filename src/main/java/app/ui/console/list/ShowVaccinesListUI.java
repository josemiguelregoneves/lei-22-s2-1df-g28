package app.ui.console.list;

import app.controller.list.ShowVaccinesListController;
import app.domain.conceptual.VaccineType;
import app.domain.shared.Alert;
import app.ui.console.utils.Utils;

public class ShowVaccinesListUI implements Runnable {
    /**
     * controller for show the vaccine's list
     */
    private ShowVaccinesListController vcShowController = new ShowVaccinesListController();

    /**
     * run method
     */
    @Override
    public void run() {
        int option = Utils.readIntegerFromConsole("Choose an option:\n1 - [Show all Vaccines]\n2 - [Show Vaccines with a specific Type]\nR: ");
        switch (option) {
            case 1:
                vcShowController.showVaccinesList(vcShowController.getVaccineList());
                break;
            case 2:
                String vcType = Utils.readLineFromConsole("Enter the Vaccine Type Name: ");
                vcShowController.showVaccinesList(vcShowController.getVaccineListByVcType(new VaccineType(vcType)));
                break;
            default:
                Alert.message("Invalid option");
        }
    }
}