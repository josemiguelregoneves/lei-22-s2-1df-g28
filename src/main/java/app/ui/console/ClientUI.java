package app.ui.console;

import app.controller.register.VaccineScheduleController;
import app.domain.conceptual.VaccineSchedule;
import app.domain.dto.VaccineScheduleDTO;
import app.domain.conceptual.Client;
import app.domain.model.Company;
import app.domain.shared.Alert;
import app.domain.shared.Validator;
import app.ui.console.register.ClientScheduleVaccineUI;
import app.ui.console.utils.Utils;

public class ClientUI implements Runnable {

    private VaccineScheduleController vcScheduleController;
    private AuthUI authUI;
    private String email;
    private String password;
    private Client snsUser;
    private VaccineSchedule vcSchedule;

    public ClientUI() {
        this.authUI = new AuthUI();
        this.vcScheduleController = new VaccineScheduleController();
    }

    @Override
    public void run() {
        System.out.println("WELCOME TO SNS USER UI");

        email = authUI.getEmail();
        password = authUI.getPassword();
        snsUser = getClientByEmail(email);

        int exit = 0;
        do {
            clientMenu();
            exit = Utils.readIntegerFromConsole("Exit Client Menu?\n1 - [Yes]\n2 - [No]\nR: ");
        } while (exit == 2);
    }

    public Client getClientByEmail(String email) {
        for (Client c : Company.getClientStore().getClientList()) {
            if (c.getEmail().equals(email)) {
                return c;
            }
        }
        return null;
    }

    public void clientMenu() {
        System.out.println("1 - [Schedule a Vaccine]");
        System.out.println("2 - [See Your Account]");
        int option = Utils.readIntegerFromConsole("Select an option:");

        switch (option) {
            case 1:
                if (Company.getCenterStore().getCentersList() != null && Company.getVcStore().getVaccineList() != null) {
                    ClientScheduleVaccineUI scheduleUI = new ClientScheduleVaccineUI(snsUser);
                    scheduleUI.run();
                } else {
                    Alert.message("No center/vaccine detected");
                }
                break;
            case 2:
                System.out.println("Name: " + snsUser.getName());
                System.out.println("Email: " + snsUser.getEmail());
                System.out.println("Birth Date: " + snsUser.getBirthDate());
                System.out.println("Phone Number: " + snsUser.getPhoneNumber());
                System.out.println("SNS Number: " + snsUser.getSnsNumber());
                break;
        }
    }
}
