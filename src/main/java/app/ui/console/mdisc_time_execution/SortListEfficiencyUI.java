package app.ui.console.mdisc_time_execution;

import app.controller.us017.SortClientScheduleController;
import app.domain.conceptual.VaccinationCenter;
import app.domain.conceptual.VaccineSchedule;
import app.domain.us017.SortArrivalTime;

import java.util.ArrayList;

public class SortListEfficiencyUI implements Runnable {

    /**
     * controller for sort clients list
     */
    private SortClientScheduleController ctrl;

    /**
     * vaccination center
     */
    private VaccinationCenter center;

    /**
     * constructor with parameters
     *
     * @param center
     */
    public SortListEfficiencyUI(VaccinationCenter center) {
        this.ctrl = new SortClientScheduleController();
        this.center = center;
    }

    /**
     * runnable method
     */
    @Override
    public void run() {
        ArrayList<VaccineSchedule> completedList = center.getCompletedSchedulesList();
        VaccineSchedule[] arrCompleted = completedList.toArray(new VaccineSchedule[0]);

        int[] inputSize = {100, 600, 1100, 1600, 2100, 2600, 3100, 3600, 4100, 4600, 5100, 5600, 6100, 6600, 7000};

        SortArrivalTime bubble = new SortArrivalTime();

        System.out.println("Bubble Sort Algorithm (Ascending/Arrival)");
        for (int i = 0; i < inputSize.length; i++) {
            VaccineSchedule[] intervalComplete = intervalArrCompleted(arrCompleted, inputSize[i]);
            long time1 = System.nanoTime();
            bubble.ascendingBubbleSort(intervalComplete);
            long time2 = System.nanoTime();
            System.out.println(time2 - time1);
        }

        System.out.println("Selection Sort Algorithm (Ascending/Arrival)");
        for (int i = 0; i < inputSize.length; i++) {
            VaccineSchedule[] intervalComplete = intervalArrCompleted(arrCompleted, inputSize[i]);
            long time1 = System.nanoTime();
            bubble.ascendingSelectionSort(intervalComplete);
            long time2 = System.nanoTime();
            System.out.println(time2 - time1);
        }

    }

    private VaccineSchedule[] intervalArrCompleted(VaccineSchedule[] arrCompleted, int interval) {
        VaccineSchedule[] intervalCompleted = new VaccineSchedule[interval];
        for (int i = 0; i < interval; i++) {
            intervalCompleted[i] = arrCompleted[i];
        }
        return intervalCompleted;
    }
}
