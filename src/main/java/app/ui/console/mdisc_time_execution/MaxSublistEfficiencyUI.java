package app.ui.console.mdisc_time_execution;

import app.controller.us016.EfficiencyCenterController;
import app.domain.shared.Validator;
import app.ui.console.utils.Utils;

import java.util.Date;

public class MaxSublistEfficiencyUI implements Runnable {
    /**
     * controller to get the center efficiency
     */
    private EfficiencyCenterController ctrl;

    /**
     * constructor without parameters that initialize the controller
     */
    public MaxSublistEfficiencyUI() {
        this.ctrl = new EfficiencyCenterController();
    }

    /**
     * runnable method
     */
    @Override
    public void run() {

        /**
         * enter and get the date
         */
        String strDate = Utils.readLineFromConsole("Enter the Date (dd/MM/yyyy): ");
        Date date = Validator.convertStringToDate(strDate);

        int[] intervals = {1, 5, 10, 20, 30};

        for (int i = 0; i < intervals.length; i++) {
            int[] diffArray = ctrl.getDiffArray(intervals[i], date);
            int[] maxSublist = ctrl.getMaxSublist(diffArray);
        }

    }
}
