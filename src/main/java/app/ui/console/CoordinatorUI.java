package app.ui.console;

import app.controller.us015.FullyVaccinatedUsersGUI;
import app.controller.us016.EfficiencyCenterGUI;
import app.controller.us017.SortClientScheduleGUI;
import app.domain.conceptual.Employee;
import app.domain.conceptual.VaccinationCenter;
import app.domain.model.Company;
import app.domain.shared.Constants;
import app.ui.console.us015.FullyVaccinatedUsersLaunch;
import app.ui.console.us016.EfficiencyCenterLaunch;
import app.ui.console.mdisc_time_execution.MaxSublistEfficiencyUI;
import app.ui.console.us017.SortClientScheduleUI;
import app.ui.console.us017.SortClientScheduleLaunch;
import app.ui.console.us015.FullVaccinatedUsersUI;
import app.ui.console.us016.DiffArrivalLeaveUI;
import app.ui.console.mdisc_time_execution.SortListEfficiencyUI;
import app.ui.console.utils.Utils;

import java.util.ArrayList;

public class CoordinatorUI implements Runnable {

    /**
     * auth UI
     */
    private AuthUI authUI = new AuthUI();

    /**
     * email
     */
    private String email;

    /**
     * password
     */
    private String password;

    /**
     * employee
     */
    private Employee coordinator;

    /**
     * center
     */
    private VaccinationCenter center;

    /**
     * runnable method
     */
    @Override
    public void run() {
        System.out.println("WELCOME TO CENTER COORDINATOR UI");

        email = authUI.getEmail();
        password = authUI.getPassword();
        coordinator = getEmployeeByEmail(email);

        int exit = 0;
        do {
            ArrayList<VaccinationCenter> centerList = Company.getCenterStore().getCentersList();
            ArrayList<String> centerName = new ArrayList<>();
            for (VaccinationCenter center : centerList) {
                centerName.add(center.getName());
            }
            int option = Utils.showAndSelectIndex(centerName, "Select a Vaccination Center");
            center = centerList.get(option);

            coordinator.setCenter(center);

            coordinatorMenu();

            exit = Utils.readIntegerFromConsole("Exit Coordinator Menu?\n1 - [Yes]\n2 - [No]\nR: ");
        } while (exit == 2);
    }

    /**
     * coordinator menu
     */
    public void coordinatorMenu() {
        System.out.println("1 - [Read Schedules From File - UI]");
        System.out.println("2 - [Import and Sort Completed Schedules from a .CSV File - UI]");
        System.out.println("3 - [Import and Sort Completed Schedules from a .CSV File - GUI]");
        System.out.println("4 - [Get/Write Full Vaccinated By Date in .CSV File - UI]");
        System.out.println("5 - [Get/Write Full Vaccinated By Date in .CSV File - GUI]");
        System.out.println("6 - [Get Maximum Sublist - UI]");
        System.out.println("7 - [Get Maximum Sublist - GUI]");
        System.out.println("8 - [US016 Analysis - UI]");
        System.out.println("9 - [US017 Analysis - UI]");

        int option = Utils.readIntegerFromConsole("Select an option:");

        switch (option) {
            case 1:
                ReadFileDataUI readUI = new ReadFileDataUI(center);
                readUI.run();
                break;
            case 2:
                SortClientScheduleUI sortUI = new SortClientScheduleUI(center);
                sortUI.run();
                break;
            case 3:
                SortClientScheduleGUI.setCenter(center);
                SortClientScheduleLaunch sortGUI = new SortClientScheduleLaunch();
                sortGUI.run();
                break;
            case 4:
                FullVaccinatedUsersUI fullVcUI = new FullVaccinatedUsersUI(center);
                fullVcUI.run();
                break;
            case 5:
                FullyVaccinatedUsersGUI.setCenter(center);
                FullyVaccinatedUsersLaunch fullVcGUI = new FullyVaccinatedUsersLaunch();
                fullVcGUI.run();
                break;
            case 6:
                DiffArrivalLeaveUI diffUI = new DiffArrivalLeaveUI(center);
                diffUI.run();
                break;
            case 7:
                EfficiencyCenterGUI.setCenter(center);
                EfficiencyCenterLaunch effGUI = new EfficiencyCenterLaunch();
                effGUI.run();
                break;
            case 8:
                MaxSublistEfficiencyUI effUI = new MaxSublistEfficiencyUI();
                effUI.run();
                break;
            case 9:
                SortListEfficiencyUI effSortUI = new SortListEfficiencyUI(center);
                effSortUI.run();
                break;
        }
    }

    /**
     * method to get the employee by name
     *
     * @param email
     *
     * @return employee
     */
    public Employee getEmployeeByEmail(String email) {
        for (Employee e : Company.geteStore().getEmployeeList()) {
            if (e.getIdRole().equals(Constants.ROLE_COORDINATOR) && e.getEmail().equals(email)) {
                return e;
            }
        }
        return null;
    }
}
