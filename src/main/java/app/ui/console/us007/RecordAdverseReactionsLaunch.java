package app.ui.console.us007;

import app.controller.App;
import app.ui.console.us008.NurseAdminVaccineLaunch;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class RecordAdverseReactionsLaunch extends Application implements Runnable {

    /**
     * method to run the scene
     */
    @Override
    public void run() {
        launch();
    }

    /**
     * method to start the stage
     *
     * @param stage
     *
     * @throws Exception
     */
    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader(NurseAdminVaccineLaunch.class.getResource("record-adverse-reactions.fxml"));
        fxmlLoader.setLocation(getClass().getClassLoader().getResource("record-adverse-reactions.fxml"));
        Scene scene = new Scene(fxmlLoader.load());
        stage.setTitle("Adverse Reactions");
        stage.setScene(scene);
        stage.setFullScreen(true);
        stage.setResizable(false);
        stage.show();
    }
}
