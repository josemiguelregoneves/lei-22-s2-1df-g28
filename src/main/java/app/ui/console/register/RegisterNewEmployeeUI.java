package app.ui.console.register;

import app.controller.register.RegisterNewEmployeeController;
import app.domain.conceptual.Employee;
import app.domain.conceptual.VaccinationCenter;
import app.domain.dto.EmployeeDTO;
import app.domain.model.*;
import app.domain.shared.Alert;
import app.domain.shared.Constants;
import app.domain.shared.Validator;
import app.ui.console.utils.Utils;

import java.util.ArrayList;

public class RegisterNewEmployeeUI implements Runnable {
    /**
     * controller for register a new employee
     */
    private RegisterNewEmployeeController eController = new RegisterNewEmployeeController();

    @Override
    public void run() {
        EmployeeDTO employeeDto;

        String name = Utils.readLineFromConsole("Name: ");

        String address = Utils.readLineFromConsole("Address: ");

        int phoneNumber = Utils.readIntegerFromConsole("Phone Number: ");
        if (!Validator.validatePhoneNumber(phoneNumber)) {
            do {
                phoneNumber = Utils.readIntegerFromConsole("Invalid Phone Number. Type a new one (9 digits): ");
            } while (!Validator.validatePhoneNumber(phoneNumber));
        }

        String email = Utils.readLineFromConsole("Email: ");
        if (!Validator.validateEmail(email)) {
            do {
                email = Utils.readLineFromConsole("Invalid Email. Type a new one (***@***.com or ***@***.pt): ");
            } while (!Validator.validateEmail(email));
        }

        int ccNumber = Utils.readIntegerFromConsole("CC Number: ");
        if (!Validator.validateCcNumber(ccNumber)) {
            do {
                ccNumber = Utils.readIntegerFromConsole("Invalid CC Number. Type a new one (8 digits): ");
            } while (!Validator.validateCcNumber(ccNumber));
        }

        ArrayList<VaccinationCenter> centerList = Company.getCenterStore().getCentersList();
        ArrayList<String> centerNameList = new ArrayList<>();
        for (VaccinationCenter center : centerList) {
            centerNameList.add(center.getName());
        }
        int centerOp = -1;
        VaccinationCenter center = null;
        if (centerList.isEmpty() || centerList == null) {
            System.out.println("No Vaccination Center detected");
        } else {
            centerOp = Utils.showAndSelectIndex(centerNameList, "Select a Vaccination Center:");
            center = centerList.get(centerOp);
        }

        int option = Utils.readIntegerFromConsole("Select the ID Role of the Employee:\n1 - [NURSE]\n2 - [RECEPTIONIST]\n3 - [COORDINATOR]\n4 - [ADMINISTRATOR]\nR: ");
        String idRole = convertOptionToRole(option);

        if (centerOp != -1 && center != null) {

            employeeDto = new EmployeeDTO(name, address, phoneNumber, email, ccNumber, idRole, center);

            int save = Utils.readIntegerFromConsole("Do you want to save this employee?\n1 - [Yes]\n2 - [No]\nR: ");

            if (save == 1) {
                Employee employee = eController.createEmployee(employeeDto);

                System.out.println("Login Data:");
                System.out.println("Email: " + employee.getEmail());
                System.out.println("Password: " + employee.getPassword());

                eController.saveEmployee(employee);
            }
        } else {
            Alert.message("Cannot create/save employee");
        }
    }

    private String convertOptionToRole(int option) {
        String role = "";
        do {
            switch (option) {
                case 1:
                    role = Constants.ROLE_NURSE;
                    break;
                case 2:
                    role = Constants.ROLE_RECEPTIONIST;
                    break;
                case 3:
                    role = Constants.ROLE_COORDINATOR;
                    break;
                case 4:
                    role = Constants.ROLE_ADMIN;
                    break;
                default:
                    option = Utils.readIntegerFromConsole("Invalid ID Role, type a valid one:\n1 - [NURSE]\n2 - [RECEPTIONIST]\n3 - [COORDINATOR]\n4 - [ADMINISTRATOR]\nR: ");
                    break;
            }
        } while (role.equals(""));
        return role;
    }
}