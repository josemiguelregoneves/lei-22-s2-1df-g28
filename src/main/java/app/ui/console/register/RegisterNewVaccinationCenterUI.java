package app.ui.console.register;

import app.controller.register.RegisterNewVaccinationCenterController;
import app.domain.conceptual.VaccinationCenter;
import app.domain.dto.VaccinationCenterDTO;
import app.domain.shared.Constants;
import app.domain.shared.Validator;
import app.ui.console.utils.Utils;

import java.sql.Time;

public class RegisterNewVaccinationCenterUI implements Runnable {
    /**
     * Vaccination Center Controller
     */
    private RegisterNewVaccinationCenterController centerController = new RegisterNewVaccinationCenterController();

    /**
     * run method
     */
    @Override
    public void run() {
        VaccinationCenterDTO centerDto;

        String name = Utils.readLineFromConsole("Name: ");

        String address = Utils.readLineFromConsole("Address: ");

        int phoneNumber = Utils.readIntegerFromConsole("Phone Number: ");
        if (!Validator.validatePhoneNumber(phoneNumber)) {
            do {
                phoneNumber = Utils.readIntegerFromConsole("Invalid Phone Number. Type a new one (9 digits): ");
            } while (!Validator.validatePhoneNumber(phoneNumber));
        }

        String email = Utils.readLineFromConsole("Email: ");
        if (!Validator.validateEmail(email)) {
            do {
                email = Utils.readLineFromConsole("Invalid Email. Type a new one (***@***.com or ***@***.pt): ");
            } while (!Validator.validateEmail(email));
        }

        int faxNumber = Utils.readIntegerFromConsole("Fax Number: ");
        if (!Validator.validateFaxNumber(faxNumber)) {
            do {
                faxNumber = Utils.readIntegerFromConsole("Invalid Fax Number. Type a new one (9 digits): ");
            } while (!Validator.validateFaxNumber(faxNumber));
        }

        String website = Utils.readLineFromConsole("Website: ");
        if (!Validator.validateWebsite(website)) {
            do {
                website = Utils.readLineFromConsole("Invalid Website. Type a new one (***.com or ***.pt): ");
            } while (!Validator.validateWebsite(website));
        }

        String slotDur = Utils.readLineFromConsole("Slot Duration (hh:mm:ss)h: ");
        Time slotDuration = Time.valueOf(slotDur);
        if (!Validator.validateTime(slotDur)) {
            do {
                slotDur = Utils.readLineFromConsole("Invalid Slot Duration. Type a new one");
                slotDuration = Time.valueOf(slotDur);
            } while (!Validator.validateTime(slotDur));
        }

        int maxVaccines = Utils.readIntegerFromConsole("Max Vaccines per Slot: ");

        String opHour = Utils.readLineFromConsole("Opening Hour (hh:mm:ss): ");
        Time openingHour = Time.valueOf(opHour);
        if (!Validator.validateTime(opHour)) {
            do {
                opHour = Utils.readLineFromConsole("Invalid Time. Type a new one");
                openingHour = Time.valueOf(opHour);
            } while (!Validator.validateTime(opHour));
        }
        if (!Validator.validateOpeningHour(opHour)) {
            do {
                opHour = Utils.readLineFromConsole("Invalid Opening Hour. Opening Hour must be between [01:00:00 - 23:00:00]h. Type a new one");
                openingHour = Time.valueOf(opHour);
            } while (!Validator.validateOpeningHour(opHour));
        }

        String clHour = Utils.readLineFromConsole("Closing Hour (hh:mm:ss): ");
        Time closingHour = Time.valueOf(clHour);
        if (!Validator.validateTime(clHour)) {
            do {
                clHour = Utils.readLineFromConsole("Invalid Time. Type a new one");
                closingHour = Time.valueOf(clHour);
            } while (!Validator.validateTime(clHour));
        }
        if (!Validator.validateClosingHour(opHour, clHour)) {
            do {
                clHour = Utils.readLineFromConsole("Invalid Closing Hour. The Center Schedule must be at least 8h. Type a new one");
                closingHour = Time.valueOf(clHour);
            } while (!Validator.validateClosingHour(opHour, clHour));
        }

        int option = Utils.readIntegerFromConsole("Select the ID Type of the Vaccination Center:\n1 - [HEALTH CARE CENTER]\n2 - [COMUNITY MASS CENTER]\nR: ");
        String idType = convertOptionToType(option);

        centerDto = new VaccinationCenterDTO(name, address, phoneNumber, email, faxNumber, website, slotDuration, maxVaccines, openingHour, closingHour, idType);

        int save = Utils.readIntegerFromConsole("Do you want to save this center?\n1 - [Yes]\n2 - [No]\nR: ");

        if (save == 1) {
            VaccinationCenter center = centerController.createCenter(centerDto);
            centerController.saveCenter(center);
        }
    }

    private String convertOptionToType(int option) {
        String type = "";
        do {
            switch (option) {
                case 1:
                    type = Constants.TYPE_HEALTH_CARE_CENTER;
                    break;
                case 2:
                    type = Constants.TYPE_COMUNITY_MASS_CENTER;
                    break;
                default:
                    option = Utils.readIntegerFromConsole("Invalid ID Type of the Vaccination Center, type a valid one:\n1 - [HEALTH CARE CENTER]\n2 - [COMUNITY MASS CENTER]\nR: ");
                    break;
            }
        } while (type.equals(""));
        return type;
    }
}
