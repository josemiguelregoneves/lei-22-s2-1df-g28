package app.ui.console.register;

import app.controller.register.RegisterNewVaccineController;
import app.domain.conceptual.AdministrationProcess;
import app.domain.conceptual.AgeRange;
import app.domain.conceptual.Vaccine;
import app.domain.conceptual.VaccineType;
import app.domain.dto.VaccineDTO;
import app.domain.model.*;
import app.domain.shared.Alert;
import app.ui.console.utils.Utils;

import java.util.ArrayList;

public class RegisterNewVaccineUI implements Runnable {

    private RegisterNewVaccineController vcController = new RegisterNewVaccineController();


    @Override
    public void run() {

        VaccineDTO vaccineDto;

        ArrayList<VaccineType> vcTypeList = Company.getVcTypeStore().getVaccineTypeList();
        ArrayList<String> vcTypeNameList = new ArrayList<>();
        for (VaccineType type : vcTypeList) {
            vcTypeNameList.add(type.getName());
        }
        int vcTypeOp = -1;
        VaccineType vcType = null;
        if (vcTypeList.isEmpty() || vcTypeList == null) {
            System.out.println("No Vaccine Type detected");
        } else {
            vcTypeOp = Utils.showAndSelectIndex(vcTypeNameList, "Select a Vaccine Type:");
            vcType = vcTypeList.get(vcTypeOp);
        }

        System.out.println("<<<< ENTER ADMINISTRATION PROCESS >>>>");

        System.out.println("<<<< AGE RANGE >>>>");
        int minAge = Utils.readIntegerFromConsole("Min Age: ");
        int maxAge = Utils.readIntegerFromConsole("Max Age: ");
        if (maxAge <= minAge) {
            do {
                System.out.println("Max Age must be higher than Min Age. Type again");
                minAge = Utils.readIntegerFromConsole("Min Age: ");
                maxAge = Utils.readIntegerFromConsole("Max Age: ");
            } while (maxAge <= minAge);
        }

        AgeRange range = new AgeRange(minAge, maxAge);

        int dosage = Utils.readIntegerFromConsole("Enter the Dosage (ml): ");

        int doses = Utils.readIntegerFromConsole("Enter the number of doses: ");

        AdministrationProcess adminProcess = new AdministrationProcess(range, dosage, doses);

        String vcName = Utils.readLineFromConsole("Enter Vaccine Name: ");

        if (vcTypeOp != -1 && vcType != null) {
            vaccineDto = new VaccineDTO(vcType, adminProcess, vcName);

            int save = Utils.readIntegerFromConsole("Do you want to save the created vaccine?\n1 - [Yes]\n2 - [No]\nR: ");

            if (save == 1) {
                Vaccine vc = vcController.createVaccine(vaccineDto);
                vcController.saveVaccine(vc);
            }
        } else {
            Alert.message("Cannot create/save vaccine");
        }

    }
}
