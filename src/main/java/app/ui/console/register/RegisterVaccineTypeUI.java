package app.ui.console.register;

import app.controller.register.RegisterVaccineTypeController;
import app.domain.conceptual.VaccineType;
import app.domain.dto.VaccineTypeDTO;
import app.ui.console.utils.Utils;

public class RegisterVaccineTypeUI implements Runnable {

    private RegisterVaccineTypeController vcTypeController = new RegisterVaccineTypeController();

    @Override
    public void run() {
        VaccineTypeDTO vcTypeDto;

        String name = Utils.readLineFromConsole("Enter Vaccine Type Name: ");

        vcTypeDto = new VaccineTypeDTO(name);

        int save = Utils.readIntegerFromConsole("Do you want to save the created vaccine type?\n1 - [Yes]\n2 - [No]\nR: ");

        if (save == 1) {
            VaccineType vcType = vcTypeController.createVaccineType(vcTypeDto);
            vcTypeController.saveVaccineType(vcType);
        }
    }
}
