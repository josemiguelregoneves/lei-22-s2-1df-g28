package app.ui.console.register;

import app.controller.register.UserRegistrationViaCSVController;
import app.domain.shared.Alert;
import app.ui.console.utils.Utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;


public class UserRegistrationViaCSVUI implements Runnable {

    /**
     * Controller to register snsUsers contained on a CSV file
     */
    private UserRegistrationViaCSVController csvController = new UserRegistrationViaCSVController();

    /**
     * run method
     */
    public void run() {
        String path = Utils.readLineFromConsole("File path: ");
        File file = new File(path);
        try {
            csvController.chooseCsvReader(file);
        } catch (FileNotFoundException e) {
            Alert.message("No File Found");
        }
    }
}
