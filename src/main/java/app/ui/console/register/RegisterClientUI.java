package app.ui.console.register;

import app.controller.register.RegisterClientController;
import app.domain.conceptual.Client;
import app.domain.conceptual.Employee;
import app.domain.dto.ClientDTO;
import app.domain.shared.Constants;
import app.domain.shared.Validator;
import app.ui.console.utils.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RegisterClientUI implements Runnable {
    /**
     * controller to register a new client
     */
    private RegisterClientController cController = new RegisterClientController();

    /**
     * run method
     */
    @Override
    public void run() {

        ClientDTO snsUserDto;

        String name = Utils.readLineFromConsole("Name: ");

        String address = Utils.readLineFromConsole("Address: ");

        int sexOption = Utils.readIntegerFromConsole("Sex of the SNS User\n1 - [MALE]\n2 - [FEMALE]\nR: ");
        String sex = convertOptionToSex(sexOption);

        int ccNumber = Utils.readIntegerFromConsole("CC Number: ");
        if (!Validator.validateCcNumber(ccNumber)) {
            do {
                ccNumber = Utils.readIntegerFromConsole("Invalid CC Number. Type a new one (8 digits): ");
            } while (!Validator.validateCcNumber(ccNumber));
        }

        String birthDateStr = Utils.readLineFromConsole("Birth date of the SNS User (dd/mm/yyyy): ");
        String[] arrBirthDate = birthDateStr.split("/");
        int day = Integer.parseInt(arrBirthDate[0]);
        int month = Integer.parseInt(arrBirthDate[1]);
        int year = Integer.parseInt(arrBirthDate[2]);

        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        Date birthDate = null;
        try {
            birthDate = format.parse(day + "/" + month + "/" + year);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        int phoneNumber = Utils.readIntegerFromConsole("Phone Number: ");
        if (!Validator.validatePhoneNumber(phoneNumber)) {
            do {
                phoneNumber = Utils.readIntegerFromConsole("Invalid Phone Number. Type a new one (9 digits): ");
            } while (!Validator.validatePhoneNumber(phoneNumber));
        }

        String email = Utils.readLineFromConsole("Email: ");
        if (!Validator.validateEmail(email)) {
            do {
                email = Utils.readLineFromConsole("Invalid Email. Type a new one (***@***.com or ***@***.pt): ");
            } while (!Validator.validateEmail(email));
        }

        int snsNumber = Utils.readIntegerFromConsole("SNS Number: ");
        if (!Validator.validateSnsNumber(snsNumber)) {
            do {
                snsNumber = Utils.readIntegerFromConsole("Invalid SNS Number. Type a new one (9 digits): ");
            } while (!Validator.validateSnsNumber(snsNumber));
        }

        snsUserDto = new ClientDTO(ccNumber, name, address, sex, birthDate, phoneNumber, email, snsNumber);

        int save = Utils.readIntegerFromConsole("Do you want to save the created SNS User?\n1 - [Yes]\n2 - [No]\nR: ");

        if (save == 1) {
            Client snsUser = cController.createClient(snsUserDto);

            System.out.println("Login Data:");
            System.out.println("Email: " + snsUser.getEmail());
            System.out.println("Password: " + snsUser.getPassword());

            cController.saveClient(snsUser);
        }
    }

    private String convertOptionToSex(int option) {
        String sex = "";
        do {
            switch (option) {
                case 1:
                    sex = Constants.SEX_MALE;
                    break;
                case 2:
                    sex = Constants.SEX_FEMALE;
                    break;
                default:
                    option = Utils.readIntegerFromConsole("Invalid Genre:\n1 - [MALE]\n2 - [FEMALE]\nR: ");
                    break;
            }
        } while (sex.equals(""));
        return sex;
    }


}


