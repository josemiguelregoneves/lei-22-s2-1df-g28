package app.ui.console.register;

import app.controller.register.VaccineScheduleController;
import app.domain.conceptual.*;
import app.domain.dto.VaccineScheduleDTO;
import app.domain.model.*;
import app.domain.shared.Alert;
import app.domain.shared.SMS;
import app.domain.shared.Validator;
import app.ui.console.utils.Utils;

import java.io.FileNotFoundException;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class ReceptionistScheduleVaccineUI implements Runnable {
    /**
     * controller to schedule a new vaccine
     */
    private VaccineScheduleController scheduleController = new VaccineScheduleController();

    private Client snsUser;

    /**
     * run method
     */
    @Override
    public void run() {

        ArrayList<Client> clientList = Company.getClientStore().getClientList();
        ArrayList<String> clientNameList = new ArrayList<>();
        for (Client snsUser : clientList) {
            clientNameList.add(snsUser.getName() + " - " + snsUser.getSnsNumber());
        }
        int userOp = -1;
        snsUser = null;
        if (clientList.isEmpty() || clientList == null) {
            Alert.message("No SNS Users detected");
        } else {
            userOp = Utils.showAndSelectIndex(clientNameList, "Select a SNS User:");
            snsUser = clientList.get(userOp);
        }

        ArrayList<VaccineType> vcTypeList = Company.getVcTypeStore().getVaccineTypeList();
        ArrayList<String> vcNameList = new ArrayList<>();
        for (VaccineType vcType : vcTypeList) {
            vcNameList.add(vcType.getName());
        }
        int vcOp = -1;
        VaccineType vcType = null;
        if (vcTypeList.isEmpty() || vcTypeList == null) {
            Alert.message("No Vaccines Types detected");
        } else {
            vcOp = Utils.showAndSelectIndex(vcNameList, "Select a Vaccine Type:");
            vcType = vcTypeList.get(vcOp);
        }

        ArrayList<VaccinationCenter> centerList = Company.getCenterStore().getCentersList();
        ArrayList<String> centerNameList = new ArrayList<>();
        for (VaccinationCenter center : centerList) {
            centerNameList.add(center.getName());
        }
        int centerOp = -1;
        VaccinationCenter center = null;
        if (centerList.isEmpty() || centerList == null) {
            Alert.message("No Centers detected");
        } else {
            centerOp = Utils.showAndSelectIndex(centerNameList, "Select a Vaccination Center:");
            center = centerList.get(centerOp);
        }

        String strDate = Utils.readLineFromConsole("Date (dd/MM/yyyy):");
        String[] arrDate = strDate.split("/");
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        Date scheduleDate = null;
        try {
            scheduleDate = format.parse(Integer.parseInt(arrDate[0]) + "/" + Integer.parseInt(arrDate[1]) + "/" + Integer.parseInt(arrDate[2]));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String strTime = Utils.readLineFromConsole("Time (hh:mm:ss):");
        Time scheduleTime = Time.valueOf(strTime);
        if (!Validator.validateTimeSchedule(scheduleTime, center)) {
            do {
                Alert.message("Invalid Time. Time must be between " + center.getOpeningHour() + " and " + center.getClosingHour());
                strTime = Utils.readLineFromConsole("Time (hh:mm:ss): ");
                scheduleTime = Time.valueOf(strTime);
            } while (!Validator.validateTimeSchedule(scheduleTime, center));
        }

        VaccineScheduleDTO vcScheduleDto = new VaccineScheduleDTO(snsUser.getSnsNumber(), vcType, scheduleDate, scheduleTime);
        VaccineSchedule vcSchedule = scheduleController.createVaccineSchedule(vcScheduleDto);

        if (Validator.validateNumberDosesSchedule(vcSchedule)) {
            int save = Utils.readIntegerFromConsole("Do you want to save this vaccine schedule?\n1 - [Yes]\n2 - [No]\nR: ");

            if (save == 1) {
                if (Validator.validateTimeSinceLastVaccine(vcSchedule)) {

                    showScheduleData(vcSchedule);

                    scheduleController.saveVaccineSchedule(vcSchedule);
                    snsUser.getSchedules().add(vcSchedule);
                    center.getSchedulesList().add(vcSchedule);
                }
            } else {
                Alert.message("The vaccine was not scheduled");
            }
        }
    }

    private void showScheduleData(VaccineSchedule vcSchedule) {
        System.out.println("Scheduled vaccine:");
        System.out.println("SNS Number: " + snsUser.getSnsNumber());
        System.out.println("Vaccine type: " + vcSchedule.getVcType().getName());
        System.out.println("Schedule Date: " + vcSchedule.getScheduleDate());
        System.out.println("Schedule Time: " + vcSchedule.getScheduleTime());

    }
}
