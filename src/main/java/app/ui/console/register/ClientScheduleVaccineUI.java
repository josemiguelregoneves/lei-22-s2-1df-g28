package app.ui.console.register;

import app.controller.register.VaccineScheduleController;
import app.domain.conceptual.*;
import app.domain.dto.VaccineScheduleDTO;
import app.domain.model.*;
import app.domain.shared.Alert;
import app.domain.shared.SMS;
import app.domain.shared.Validator;
import app.ui.console.utils.Utils;

import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.sql.Time;
import java.util.Date;

public class ClientScheduleVaccineUI implements Runnable {

    private Client snsUser;

    private VaccineScheduleController scheduleController = new VaccineScheduleController();

    public ClientScheduleVaccineUI(Client snsUser) {
        this.snsUser = snsUser;
    }

    @Override
    public void run() {

        ArrayList<VaccineType> vcTypeList = Company.getVcTypeStore().getVaccineTypeList();
        ArrayList<String> vcNameList = new ArrayList<>();
        for (VaccineType vcType : vcTypeList) {
            vcNameList.add(vcType.getName());
        }
        int vcOp = -1;
        VaccineType vcType = null;
        if (vcTypeList.isEmpty() || vcTypeList == null) {
            Alert.message("No Vaccines Types detected");
        } else {
            vcOp = Utils.showAndSelectIndex(vcNameList, "Select a Vaccine Type:");
            vcType = vcTypeList.get(vcOp);
        }

        ArrayList<VaccinationCenter> centerList = Company.getCenterStore().getCentersList();
        ArrayList<String> centerNameList = new ArrayList<>();
        for (VaccinationCenter center : centerList) {
            centerNameList.add(center.getName());
        }
        int centerOp = -1;
        VaccinationCenter center = null;
        if (centerList.isEmpty() || centerList == null) {
            Alert.message("No Centers detected");
        } else {
            centerOp = Utils.showAndSelectIndex(centerNameList, "Select a Vaccination Center:");
            center = centerList.get(centerOp);
        }

        String strDate = Utils.readLineFromConsole("Date (dd/MM/yyyy):");
        String[] arrDate = strDate.split("/");
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        Date scheduleDate = null;
        try {
            scheduleDate = format.parse(Integer.parseInt(arrDate[0]) + "/" + Integer.parseInt(arrDate[1]) + "/" + Integer.parseInt(arrDate[2]));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String strTime = Utils.readLineFromConsole("Time (hh:mm:ss):");
        Time scheduleTime = Time.valueOf(strTime);
        if (!Validator.validateTimeSchedule(scheduleTime, center)) {
            do {
                Alert.message("Invalid Time. Time must be between " + center.getOpeningHour() + " and " + center.getClosingHour());
                strTime = Utils.readLineFromConsole("Time (hh:mm:ss): ");
                scheduleTime = Time.valueOf(strTime);
            } while (!Validator.validateTimeSchedule(scheduleTime, center));
        }

        VaccineScheduleDTO vcScheduleDto = new VaccineScheduleDTO(snsUser.getSnsNumber(), vcType, scheduleDate, scheduleTime);
        VaccineSchedule vcSchedule = scheduleController.createVaccineSchedule(vcScheduleDto);

        if (Validator.validateNumberDosesSchedule(vcSchedule)) {
            int save = Utils.readIntegerFromConsole("Do you want to save this vaccine schedule?\n1 - [Yes]\n2 - [No]\nR: ");

            if (save == 1) {
                if (Validator.validateTimeSinceLastVaccine(vcSchedule)) {

                    showScheduleData(vcSchedule);

                    scheduleController.saveVaccineSchedule(vcSchedule);
                    snsUser.getSchedules().add(vcSchedule);
                    center.getSchedulesList().add(vcSchedule);
                }
            } else {
                Alert.message("The vaccine was not scheduled");
            }
        }
    }

    private void showScheduleData(VaccineSchedule vcSchedule) {
        System.out.println("Scheduled vaccine:");
        System.out.println("SNS Number: " + snsUser.getSnsNumber());
        System.out.println("Vaccine type: " + vcSchedule.getVcType().getName());
        System.out.println("Schedule Date: " + vcSchedule.getScheduleDate());
        System.out.println("Schedule Time: " + vcSchedule.getScheduleTime());

    }
}
