package app.ui.console.us015;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class FullyVaccinatedUsersLaunch extends Application implements Runnable {

    /**
     * method to start the stage
     *
     * @param stage
     *
     * @throws Exception
     */
    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader(FullyVaccinatedUsersLaunch.class.getResource("number-full-vacinated.fxml"));
        fxmlLoader.setLocation(getClass().getClassLoader().getResource("number-full-vacinated.fxml"));
        Scene scene = new Scene(fxmlLoader.load());
        stage.setTitle("Get Full Vaccinated");
        stage.setScene(scene);
        stage.setFullScreen(true);
        stage.setResizable(false);
        stage.show();
    }

    /**
     * method to run the scene
     */
    @Override
    public void run() {
        launch();
    }

}
