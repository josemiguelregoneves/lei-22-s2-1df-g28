package app.ui.console.us015;

import app.controller.us015.FullyVaccinatedUsersController;
import app.domain.conceptual.VaccinationCenter;
import app.domain.conceptual.VaccineSchedule;
import app.domain.dto.VaccineScheduleDTO;
import app.domain.model.Company;
import app.domain.shared.Validator;
import app.ui.console.utils.Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;

public class FullVaccinatedUsersUI implements Runnable {

  /**
   * controller for get the fully vaccinated users per day
   */
  private final FullyVaccinatedUsersController ctrl;

  /**
   * vaccination center
   */
  private VaccinationCenter center;

  /**
   * constructor with parameters
   *
   * @param center
   */
  public FullVaccinatedUsersUI(VaccinationCenter center) {
    this.ctrl = new FullyVaccinatedUsersController();
    this.center = center;
  }

  /**
   * method to get the vaccination center value
   *
   * @return center
   */
  public VaccinationCenter getCenter() {
    return center;
  }

  /**
   * method to set the vaccination center value
   *
   * @param center
   */
  public void setCenter(VaccinationCenter center) {
    this.center = center;
  }

  /**
   * runnable method
   */
  @Override
  public void run() {

    /**
     * string of minimum date
     */
    String strDate1 = Utils.readLineFromConsole("Enter the Min Date (dd/MM/yyyy): ");

    /**
     * minimum date
     */
    Date minDate = Validator.convertStringToDate(strDate1);

    /**
     * string of maximum date
     */
    String strDate2 = Utils.readLineFromConsole("Enter the Max Date (dd/MM/yyyy): ");

    /**
     * maximum date
     */
    Date maxDate = Validator.convertStringToDate(strDate2);

    /**
     * list of all dates of the interval
     */
    ArrayList<Date> datesList = ctrl.datesBetweenInterval(minDate, maxDate);

    System.out.println("Full Vaccinated:");

    /**
     * full vaccinated schedules of the center
     */
    ArrayList<VaccineScheduleDTO> fullVaccinatedSchedulesDTO = ctrl.getFullCompletedSchedules(center);
    ArrayList<VaccineSchedule> fullVaccinatedSchedules = Company.getListSchedulesMapper()
        .toModel(fullVaccinatedSchedulesDTO);
    for (VaccineSchedule schedule : fullVaccinatedSchedules) {
      System.out.println(schedule);
    }

    /**
     * total full vaccinated per day list
     */
    int[] totalFull = ctrl.getFullVaccinatedNumberPerDay(datesList, fullVaccinatedSchedules);

    for (int i = 0; i < totalFull.length; i++) {
      System.out.printf("Date: %s\nTotal Full Vaccinated: %d\n", Validator.convertDateToString(datesList.get(i)),
          totalFull[i]);
      System.out.println();
    }

    /**
     * file path
     */
    String filePath = "excel/fullVaccinatedByDay.csv";

    /**
     * record the data in the .csv file
     */
    ctrl.writeFullVaccinated(new File(filePath), datesList, totalFull);
  }
}
