package app.ui.console;

import app.controller.AuthController;
import app.domain.conceptual.Client;
import app.domain.model.Company;
import app.domain.conceptual.Employee;
import app.domain.shared.Constants;
import app.ui.console.utils.Utils;
import pt.isep.lei.esoft.auth.mappers.dto.UserRoleDTO;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

/**
 * @author Paulo Maio <pam@isep.ipp.pt>
 */

public class AuthUI implements Runnable {
    private AuthController ctrl;

    private static String email;
    private static String password;

    public AuthUI() {
        ctrl = new AuthController();
    }

    public void run() {

        boolean success = doLogin();

        if (success) {
            List<UserRoleDTO> roles = this.ctrl.getUserRoles();
            if ((roles == null) || (roles.isEmpty())) {
                System.out.println("User has not any role assigned.");
            } else {
                UserRoleDTO role = selectsRole(roles);
                if (!Objects.isNull(role)) {
                    List<MenuItem> rolesUI = getMenuItemForRoles();
                    this.redirectToRoleUI(rolesUI, role);
                } else {
                    System.out.println("User did not select a role.");
                }
            }
        }
        this.logout();
    }

    private List<MenuItem> getMenuItemForRoles() {
        List<MenuItem> rolesUI = new ArrayList<>();
        rolesUI.add(new MenuItem(Constants.ROLE_ADMIN, new AdminUI()));
        rolesUI.add(new MenuItem(Constants.ROLE_COORDINATOR, new CoordinatorUI()));
        rolesUI.add(new MenuItem(Constants.ROLE_NURSE, new NurseUI()));
        rolesUI.add(new MenuItem(Constants.ROLE_RECEPTIONIST, new ReceptionistUI()));
        rolesUI.add(new MenuItem(Constants.ROLE_SNS_USER, new ClientUI()));
        // To complete with other user roles and related RoleUI

        //
        return rolesUI;
    }

    private boolean doLogin() {
        System.out.println("\nLogin UI:");

        int maxAttempts = 3;
        boolean success = false;
        do {
            maxAttempts--;
            email = Utils.readLineFromConsole("Enter UserId/Email: ");
            password = Utils.readLineFromConsole("Enter Password: ");

            success = ctrl.doLogin(email, password);
            if (!success) {
                System.out.println("Invalid UserId and/or Password. \n You have  " + maxAttempts + " more attempt(s).");

            }

        } while (!success && maxAttempts > 0);
        return success;
    }

    private void logout() {
        ctrl.doLogout();
    }

    private void redirectToRoleUI(List<MenuItem> rolesUI, UserRoleDTO role) {
        boolean found = false;
        Iterator<MenuItem> it = rolesUI.iterator();
        while (it.hasNext() && !found) {
            MenuItem item = it.next();
            found = item.hasDescription(role.getDescription());
            if (found)
                item.run();
        }
        if (!found)
            System.out.println("There is no UI for users with role '" + role.getDescription() + "'");
    }

    private UserRoleDTO selectsRole(List<UserRoleDTO> roles) {
        if (roles.size() == 1) {
            return roles.get(0);
        } else {
            return (UserRoleDTO) Utils.showAndSelectOne(roles, "Select the role you want to adopt in this session:");
        }
    }

    private void redirectToUserUI() {
        String idRole = getRoleByLogin();
        switch (idRole) {
            case Constants.ROLE_ADMIN:
                AdminUI admin = new AdminUI();
                admin.run();
                break;
            case Constants.ROLE_NURSE:
                NurseUI nurse = new NurseUI();
                nurse.run();
                break;
            case Constants.ROLE_COORDINATOR:
                CoordinatorUI coordinator = new CoordinatorUI();
                coordinator.run();
                break;
            case Constants.ROLE_RECEPTIONIST:
                ReceptionistUI receptionist = new ReceptionistUI();
                receptionist.run();
                break;
            case Constants.ROLE_SNS_USER:
                ClientUI snsUser = new ClientUI();
                snsUser.run();
                break;
        }
    }

    private String getRoleByLogin() {
        for (Employee e : Company.geteStore().getEmployeeList()) {
            if (e.getEmail().equals(email) && e.getPassword().equals(password)) {
                return e.getIdRole();
            }
        }
        for (Client snsUser : Company.getClientStore().getClientList()) {
            if (snsUser.getEmail().equals(email) && snsUser.getPassword().equals(password)) {
                return Constants.ROLE_SNS_USER;
            }
        }
        return "";
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

/*
    * public void run() {
        String[] loginData = new String[2];
        boolean success = doLogin(loginData);
        if (success) {
            String idRole = getRoleByLogin(loginData[0], loginData[1]);
            redirectToUserUI(idRole);
        }
        this.logout();
    }
    * */

    /*
    * private boolean doLogin(String[] loginData)
    {
        System.out.println("\nLogin UI:");

        int maxAttempts = 3;
        boolean success = false;
        do {
            maxAttempts--;
            String id = Utils.readLineFromConsole("Enter UserId/Email: ");
            String pwd = Utils.readLineFromConsole("Enter Password: ");

            loginData[0] = id;
            loginData[1] = pwd;

            success = ctrl.doLogin(id, pwd);
            if (!success) {
                System.out.println("Invalid UserId and/or Password. \n You have  " + maxAttempts + " more attempt(s).");
            }

        } while (!success && maxAttempts > 0);
        return success;
    }
    * */
}
