package app.ui.console;

/**
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class DevTeamUI implements Runnable {

    public DevTeamUI() {

    }

    public void run() {
        System.out.println("\n");
        System.out.printf("Development Team:\n");
        System.out.printf("\t José Neves - 1200901@isep.ipp.pt \n");
        System.out.printf("\t João Tavares - 1211483@isep.ipp.pt \n");
        System.out.printf("\t Alexandre Pereira - 1201159@isep.ipp.pt \n");
        System.out.printf("\t Gonçalo Amaral - 1201839@isep.ipp.pt \n");
        System.out.printf("\t Miguel Dias - 1211859@isep.ipp.pt \n");
        System.out.println("\n");
    }
}
