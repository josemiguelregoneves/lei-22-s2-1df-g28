package app.ui.console.us016;

import app.controller.us016.EfficiencyCenterController;
import app.controller.us017.SortClientScheduleController;
import app.domain.conceptual.VaccinationCenter;
import app.domain.conceptual.VaccineSchedule;
import app.domain.model.Company;
import app.domain.shared.Alert;
import app.domain.shared.Constants;
import app.domain.shared.GetConfigProperties;
import app.domain.shared.Validator;
import app.domain.us016.HighestSublist;
import app.ui.console.utils.Utils;
import org.apache.commons.lang3.Validate;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

public class DiffArrivalLeaveUI implements Runnable {

    /**
     * controller to get the center efficiency
     */
    private EfficiencyCenterController ctrl;

    /**
     * vaccination center
     */
    private VaccinationCenter center;

    /**
     * constructor with parameters
     *
     * @param center
     */
    public DiffArrivalLeaveUI(VaccinationCenter center) {
        this.ctrl = new EfficiencyCenterController();
        this.center = center;
    }

    /**
     * runnable method
     */
    @Override
    public void run() {

        /**
         * enter and get the date
         */
        String strDate = Utils.readLineFromConsole("Enter the Date (dd/MM/yyyy): ");
        Date date = Validator.convertStringToDate(strDate);

        /**
         * get the completed schedules of that date
         */
        ArrayList<VaccineSchedule> completed = Company.getScheduleStore().getCompletedSchedulesByDate(date);

        if (completed != null && !completed.isEmpty()) {
            /**
             * select an interval in minutes
             */
            int interval = Utils.readIntegerFromConsole("Enter the interval (1/5/10/20/30) min.: ");

            /**
             * get the array of the difference between arrival and leaving time for each interval
             */
            int[] diffArray = ctrl.getDiffArray(interval, date);

            /**
             * print the diff array
             */
            System.out.println("Diff List: " + Arrays.toString(diffArray));

            /**
             * get the highest sublist
             */
            int[] maxSubList = ctrl.getMaxSublist(diffArray);

            /**
             * get the sum of the highest sublist
             */
            int maxSum = ctrl.getMaxSumSublist(maxSubList);

            /**
             * print the highest sublist
             */
            System.out.println("Max Sublist: " + Arrays.toString(maxSubList));

            /**
             * print the sum of the highest sublist
             */
            System.out.println("Max Sum: " + maxSum);

            /**
             * minimum and maximum hour of the sublist
             */
            Time[] timeInterval = ctrl.getMinMaxHour(maxSubList, diffArray, interval);

            System.out.println("Minimum Hour: " + String.valueOf(timeInterval[0]));
            System.out.println("Maximum Hour: " + String.valueOf(timeInterval[1]));

        } else {
            Alert.message("Invalid Date/Cannot Find Completed Schedules");
        }

    }


}
