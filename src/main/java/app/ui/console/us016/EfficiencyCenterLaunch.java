package app.ui.console.us016;

import app.ui.console.us015.FullyVaccinatedUsersLaunch;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class EfficiencyCenterLaunch extends Application implements Runnable {

    /**
     * method to start the stage
     *
     * @param stage
     *
     * @throws Exception
     */
    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader(EfficiencyCenterLaunch.class.getResource("diff-max-sublist.fxml"));
        fxmlLoader.setLocation(getClass().getClassLoader().getResource("diff-max-sublist.fxml"));
        Scene scene = new Scene(fxmlLoader.load());
        stage.setTitle("Maximum Sublist");
        stage.setScene(scene);
        stage.setFullScreen(false);
        stage.setResizable(false);
        stage.show();
    }

    /**
     * method to run the scene
     */
    @Override
    public void run() {
        launch();
    }
}
