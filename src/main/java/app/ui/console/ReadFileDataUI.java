package app.ui.console;

import app.controller.us017.SortClientScheduleController;
import app.domain.conceptual.VaccinationCenter;
import app.domain.shared.Alert;
import app.ui.console.utils.Utils;

import java.io.File;
import java.io.FileNotFoundException;

public class ReadFileDataUI implements Runnable {
    /**
     * controller for sort clients list
     */
    private SortClientScheduleController ctrl;

    /**
     * vaccination center
     */
    private VaccinationCenter center;

    /**
     * constructor with parameters
     *
     * @param center
     */
    public ReadFileDataUI(VaccinationCenter center) {
        this.ctrl = new SortClientScheduleController();
        this.center = center;
    }

    /**
     * method to get the vaccination center
     *
     * @return center
     */
    public VaccinationCenter getCenter() {
        return center;
    }

    /**
     * method to set the vaccination center value
     *
     * @param center
     */
    public void setCenter(VaccinationCenter center) {
        this.center = center;
    }
    @Override
    public void run() {
        /**
         * enter the file path
         */
        String fiePath = Utils.readLineFromConsole("Enter the File Path: ");

        /**
         * read the schedules from the file
         */
        try {
            ctrl.readSchedulesFromFile(center, new File(fiePath));
        } catch (FileNotFoundException e) {
            Alert.message(e.getMessage());
        }
    }
}
