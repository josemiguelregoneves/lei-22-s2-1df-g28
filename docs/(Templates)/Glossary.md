# Glossary

**Terms, Expressions and Acronyms (TEA) must be organized alphabetically.**

| **_TEA_** (EN)                        | **_TEA_** (PT)                               | **_
Description_** (EN)                                                                                           |
| :------------------------------------ | -------------------------------------------- | ---------------------------------------------------------------------------------------------------------------- |
| **Administrator**                     | **
Administrador**                            | Person who will configure and manage the core information                                                        |
| **Age groups**                        | **Grupos de
idades**                         | Age groups to administer different doses of the vaccine                                                          |
| **AGES**                              | **
AGES**                                     | Cluster of health centers (Agrupamento de Centros de Saúde)                                                      |
| **ARS**                               | **
ARS**                                      | Regional health administration (Administração Regional de Saúde)                                                 |
| **Astra Zeneca**                      | **Astra
Zeneca**                             | Vaccine to counter Covid-19                                                                                      |
| **CamelCase**                         | **
CamelCase**                                | The practice of writing phrases without spaces or punctuation                                                    |
| **Closing hour**                      | **Hora de
fecho**                            | Closing hour of the vaccination center                                                                           |
| **Community mass vaccination center** | **Centro de vacinação comunitário em
massa** | Mass vaccination center only admninistrate one type of vaccine                                                   |
| **Coordinator**                       | **
Coordenador**                              | Person who has the responsability to manage the Covid-19 vaccination process                                     |
| **Covid-19**                          | **
Covid-19**                                 | Pandemic Virus                                                                                                   |
| **DGS**                               | **
DGS**                                      | General health directive (Direção Geral de Saúde)                                                                |
| **Dosage**                            | **
Dosagem**                                  | Amount of vaccine to be administered (ml)                                                                        |
| **Dose**                              | **
Dose**                                     | Vaccine dosages per age group                                                                                    |
| **Employee**                          | **
Funcionário**                              | Person who works on the vaccination center                                                                       |
| **EU**                                | **
UE**                                       | Acronym for European Union                                                                                       |
| **European Union**                    | **União
Europeia**                           | Political and economic union located primarily in Europe                                                         |
| **FIFO**                              | **
FIFO**                                     | Stands for "First In, First Out"                                                                                 |
| **GUI**                               | **
GUI**                                      | Graphical User Interface                                                                                         |
| **Health care center**                | **Centro de
saúde**                          | Health care center can admninister any type of vaccine                                                           |
| **I/O**                               | **I/O**                                      | Acronym for _
Input/Output_                                                                                       |
| **IDE**                               | **IDE**                                      | Acronym for _Integrated
Development Environment_                                                                 |
| **Input/Output**                      | **
Input/Output**                             | The communication between an information processing system, such as a computer, and the outside world            |
| **IntelliJ**                          | **
IntelliJ**                                 | Integrated Development Environment (IDE) written in Java for developing computer software                        |
| **JaCoCo**                            | **
JaCoCo**                                   | Free Java code coverage library distributed under the Eclipse Public License                                     |
| **Javadoc**                           | **
Javadoc**                                  | Documentation generator created by Sun Microsystems for the Java language                                        |
| **JavaFX 11**                         | **JavaFX
11**                                | Open source, next generation client application platform for desktop, mobile and embedded systems built on Java. |
| **JUnit 5 framework**                 | **JUnit 5
framework**                        | The Software Test Management App that Unifies Testing & Development                                              |
| **Maximum number of vaccines**        | **Número máximo de
vacinas**                 | Maximum number of vaccines to be administrated on each slot                                                      |
| **Moderna**                           | **
Moderna**                                  | Vaccine to counter Covid-19                                                                                      |
| **NetBeans**                          | **
NetBeans**                                 | Integrated Development Environment (IDE) written in Java for developing computer software                        |
| **Nurse**                             | **
Enfermeiro**                               | Person responsible for administrating the vaccines and check the health state of the client                      |
| **Ongoing outbreak**                  | **Surto
atual**                              | Current outbreak                                                                                                 |
| **OO**                                | **OO**                                       | Acronym for _Oriented
Objects_                                                                                   |
| **Opening hour**                      | **Hora de
abertura**                         | Opening hour of the vaccination center                                                                           |
| **Oriented Objects**                  | **Orientação a
Objetos**                     | Programming paradigm based on the concept of "objects"                                                           |
| **Pfizer**                            | **
Pfizer**                                   | Vaccine to counter Covid-19                                                                                      |
| **Receptionist**                      | **
Rececionista**                             | Employee responsible for check the SNS User data and validate his vaccination process                            |
| **Recovery Period**                   | **Periodo de
Recuperação**                   | Time until the Client is considered able to leave the recovery room                                              |
| **Scalable Vector Graphics**          | **gráficos vetoriais
escalonáveis**          | XML-based vector image format for two-dimensional graphics with support for interactivity and animation          |
| **Schedule**                          | **
Agendar**                                  | Schedule the date and time the user wants to be vaccinated                                                       |
| **Slot duration**                     | **Duração do
lote**                          | Duration of each slot of vaccines                                                                                |
| **SMS**                               | **
SMS**                                      | Short Message Service                                                                                            |
| **SNS**                               | **
SNS**                                      | National Health System ("Serviço Nacional de Saúde" in Portuguese)                                               |
| **SNS User**                          | **Utente
SNS**                               | Person who will be vaccinated                                                                                    |
| **SNS User number**                   | **Número de utente de
saúde**                | Number for each SNS User                                                                                         |
| **SVG**                               | **SVG**                                      | Acronym for _Scalable Vector
Graphics_                                                                           |
| **Vaccination Certificate**           | **Certificado de
vacinação**                 | Certificate that proves the complete vaccination processs                                                        |
