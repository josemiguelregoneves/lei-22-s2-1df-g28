# Use Case Diagram (UCD)

**In the scope of this project, there is a direct relationship of _1 to 1_ between Use Cases (UC) and User Stories (US)
.**

However, be aware, this is a pedagogical simplification. On further projects and curricular units might also exist \_1
to N **and/or** N to 1 relationships between US and UC.

**Insert below the Use Case Diagram in a SVG format**

![Use Case Diagram](../sprintA/UCD.svg)

**For each UC/US, it must be provided evidences of applying main activities of the software development process (
requirements, analysis, design, tests and code). Gather those evidences on a separate file for each UC/US and set up a
link as suggested below.**

# Use Cases / User Stories

## User Stories will be in order of the appearance in UCD.svg

| UC/US  | Description       |
| :----- | :---------------- |
| US 001 | [US001](US001.md) |
| US 002 | [US002](US002.md) |
| US 003 | [US003](US003.md) |
| US 004 | [US004](US004.md) |
| US 005 | [US005](US005.md) |
| US 006 | [US006](US006.md) |
| US 007 | [US007](US007.md) |
| US 008 | [US008](US008.md) |
| US 009 | [US009](US009.md) |
| US 010 | [US010](US010.md) |
| US 011 | [US011](US011.md) |
| US 012 | [US012](US012.md) |
| US 013 | [US013](US013.md) |
| US 014 | [US014](US014.md) |
| US 015 | [US015](US015.md) |
