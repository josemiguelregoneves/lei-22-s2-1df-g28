# OO Analysis #

The construction process of the domain model is based on the client specifications, especially the nouns (for _
concepts_) and verbs (for _relations_) used.

## Rationale to identify domain conceptual classes ##

To identify domain conceptual classes, start by making a list of candidate conceptual classes inspired by the list of
categories suggested in the book "Applying UML and Patterns: An Introduction to Object-Oriented Analysis and Design and
Iterative Development".

### _Conceptual Class Category List_ ###

| Category |Candidate Classes |
| -------- | ----------------- |
| Places | Community Mass Vaccination Center, Health Care Center |
| Roles of People or Organizations| User, Nurse, Receptionist, Center Coordinator, Administrator |
| Transactions | Vaccination Scheduling, Vaccine Administration, Vaccination Certificate |
| Transaction Records/Registers | User Data Registration, After-Administration Vaccine Data Registration |
| Products or Services related to Transaction or Transaction Line | Vaccine, Adverse Reactins Check-up, User Presence verification, Vaccination Center Monitoring and Analysis |
| Organizations (Other) | DGS |
| Noteworthy Events | Vaccination Scheduling, Vaccine Administration |
| Other (External) Systems | SMS Vaccination Confirmation |

### **Rationale to identify associations between conceptual classes**###

An association is a relationship between instances of objects that indicates a relevant connection and that is worth of
remembering, or it is derivable from the List of Common Associations:

+ **_A_** is physically or logically part of **_B_**
+ **_A_** is physically or logically contained in/on **_B_**
+ **_A_** is a description for **_B_**
+ **_A_** known/logged/recorded/reported/captured in **_B_**
+ **_A_** uses or manages or owns **_B_**
+ **_A_** is related with a transaction (item) of **_B_**
+ etc.

| Concept (A) |  Association    |  Concept (B) |
| ----------- | :-------------: | -----------: |
| C1          | verb1                  | C2           |
| ...          | ...                    | ...          |

## Domain Model

**Do NOT forget to identify concepts atributes too.**

**Insert below the Domain Model Diagram in a SVG format**

![DM.svg](DM.svg)
