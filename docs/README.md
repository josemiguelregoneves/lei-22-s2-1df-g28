# Integrating Project for the 2nd Semester of LEI-ISEP 2021-22

# 1. Team Members

The teams consists of students identified in the following table.

| Student Number | Name | |-----------------|--| | **1200901**     | José Neves | | **1201159**     | Alexandre Pereira
| | **1201839**     | Gonçalo Amaral | | **1211483**     | João Tavares | | **1211859**     | Miguel Dias |

# 2. Task Distribution ###

Throughout the project's development period, the distribution of _tasks / requirements / features_ by the team members
was carried out as described in the following table.

**Keep this table must always up-to-date.**

| Task                      | [Sprint A](SprintA/README.md) | [Sprint B](SprintB/README.md) | [Sprint C](SprintC/README.md) |  [Sprint D](SprintD/README.md) |
|-----------------------------|------------|------------|------------|------------|
| Glossary  |  [all](SprintA/Glossary.md)   |   [all](SprintB/Glossary.md)  |   [all](SprintC/Glossary.md)  | [all](SprintD/Glossary.md)  |
| Use Case Diagram (UCD)  |  [all](SprintA/UCD.md)   |   [all](SprintB/UCD.md)  |   [all](SprintC/UCD.md)  | [all](SprintD/UCD.md)  |
| Supplementary Specification   |  [all](SprintA/FURPS.md)   |   [all](SprintB/FURPS.md)  |   [all](SprintC/FURPS.md)  | [all](SprintD/FURPS.md)  |
| Domain Model  |  [all](SprintA/DM.md)   |   [all](SprintB/DM.md)  |   [all](SprintC/DM.md)  | [all](SprintD/DM.md)  |
| US 003 (SDP Activities)  |  | [1201839](SprintB/us-03/ESOFT_REQUIREMENTS/US_003.md) | [all](SprintB/us-03/ESOFT_REQUIREMENTS/US_003.md) | [all](SprintB/us-03/ESOFT_REQUIREMENTS/US_003.md) |
| US 009 (SDP Activities)  |  | [1200901](SprintB/us-09/ESOFT_REQUIREMENTS/US_009.md) | [all](SprintB/us-09/ESOFT_REQUIREMENTS/US_009.md) | [all](SprintB/us-09/ESOFT_REQUIREMENTS/US_009.md) |
| US 010 (SDP Activities)  |  | [1211483](SprintB/us-10/esoft-requirements/US_10.md) | [all](SprintB/us-10/esoft-requirements/US_10.md) | [all](SprintB/us-10/esoft-requirements/US_10.md) |
| US 011 (SDP Activities)  |  | [1201839](SprintB/us-11/esoft-requirements/US_11.md) | [all](SprintB/us-11/esoft-requirements/US_11.md) | [all](SprintB/us-11/esoft-requirements/US_11.md) |
| US 012 (SDP Activities)  |  | [Optional](SprintB) | [Optional](SprintB) | [Optional](SprintB) |
| US 013 (SDP Activities)  |  | [1201159](SprintB/us-13/esoft-requirements/US_13.md) | [all](SprintB/us-13/esoft-requirements/US_13.md) | [all](SprintB/us-13/esoft-requirements/US_13.md) |
| US 001 (SDP Activities)  |  |  | [1201839](SprintC/US-01/esoft-requirements/US_01.md) | [all](SprintC/US-01/esoft-requirements/US_01.md) |
| US 002 (SDP Activities)  |  |  | [1211859](SprintC/US-02/esoft-requirements/US_02.md) | [all](SprintC/US-02/esoft-requirements/US_02.md) |
| US 004 (SDP Activities)  |  |  | [1201159](SprintC/US-04/esoft-requirements/US_04.md) | [all](SprintC/US-04/esoft-requirements/US_04.md) |
| US 005 (SDP Activities)  |  |  | [1211483](SprintC/US-005/esoft-requirements/US_005.md) | [all](SprintC/US-005/esoft-requirements/US_005.md) |
| US 014 (SDP Activities)  |  |  | [1200901](SprintC/US-014/ESOFT_REQUIREMENTS/US_014.md) | [all](SprintC/US-014/ESOFT_REQUIREMENTS/US_014.md) |
| US 006 (SDP Activities)  |  |  |  | [1200901](SprintD/US006/REQUIREMENTS%20ENGINEERING/US_006.md) |
| US 007 (SDP Activities)  |  |  |  | [Optional](SprintD/US007/ESOFT_REQUIREMENTS/US_007.md) |
| US 008 (SDP Activities)  |  |  |  | [1211483](SprintD/US008/ESOFT_REQUIREMENTS/US_008.md) |
| US 015 (SDP Activities)  |  |  |  | [1201839](SprintD/US015/ESOFT_REQUIREMENTS/US_015.md) |
| US 016 (SDP Activities)  |  |  |  | [1211859](SprintD/US016/ESOFT_REQUIREMENTS/US_016.md) |
| US 017 (SDP Activities)  |  |  |  | [1201159](SprintD/US017/ESOFT_REQUIREMENTS/US_017.md) |

