# US 009 - Vaccination Center Registration

## 1. Requirements Engineering

### 1.1. User Story Description

As an administrator, I want to load a set of users from a CSV file.

### 1.2. Customer Specifications and Clarifications

#### From the Specifications Document:

#### From the client Clarifications:

Question: "Regarding US014, I would like to clarify if the CSV file only contains information about SNS users of if the
CSV file may also contain some information about employees from that vaccination center."

Answer: The CSV file only contains information about SNS users.

Question: "What would be the sequence of parameters to be read on the CSV? For example: "Name | User Number".

Answer: Name, Sex, Birth Date, Address, Phone Number, E-mail, SNS User Number and Citizen Card Number.

Question: "When the admin wants to upload a CSV file to be read, should the file be stored at a specific location on the
computer (e.g. the desktop) or should the admin be able to choose the file he wants to upload in a file explorer?"

Answer: The Administrator should write the file path. In Sprint C we do not ask students to develop a graphical user
interface.

### 1.3. Acceptance Criteria

- AC1:
    - one type of CSV file must have a header and the column separation is done using “;” character;
    - the other type of CSV file does not have a header and the column separation is done using “,” character.

### 1.4. Found out Dependencies

To load a CSV file with users informations, there must be a company and a ClientStore first.

### 1.5 Input and Output Data

#### Input Data

- Typed data: id, password, CSV location;
- Selected data: (none)

#### Output Data

- Success or insuccess of the operation and registration or not of the users in the CSV file

### 1.6. System Sequence Diagram (SSD)

![US014_SSD](US014_SSD.svg)

### 1.7 Other Relevant Remarks

Special Requirements:

- The path of the file must be specifyed

How often this US is held:

- Whenever the Administrator wants/needs to load a large amount of users or wants/needs to load a CSV file.

## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt

![US014_MD](US014_MD.svg)

### 2.2. Other Remarks

Note: no other remarks available

## 3. Design - User Story Realization

### 3.1. Rationale

**The rationale grounds on the SSD interactions and the identified input/output data.**

| Interaction ID | Question: Which class is responsible for... | Answer  | Justification (with patterns)  |
|:-------------  |:--------------------- |:------------|:---------------------------- |
| Step 1:  selects the option to load a set of users from a CSV file         | ... Load the CSV file?                             | RegisterCSVWithHeader or RegisterCSVWithoutHeader            | RegisterCSVWithHeader(File file) or RegisterCSVWithoutHeader(File file)                              |
| Step 2: requests the location of the CSV file         | n/a                             | n/a             | n/a                              |
| Step 3: types requested data         | ... saving the input data?                             | Utils             | readLineFromConsole()                              |
| Step 4: create the users on the system         | ...create the user                             | ClientStore             | createClient()
| Step 5: confirms the data         | ... saving the created Client?                             | ClientStore             | saveClient()                              |
| Step 8: informs the operation success/insuccess         | ... informing operation success/insuccess?                             | ClientStore             | n/a                              |

### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are:

* RegisterCSVWithHeader
* RegisterCSVWithoutHeader

Other software classes (i.e. Pure Fabrication) identified:

* UserRegistrationViaCSVUI
* UserRegistrationViaCSVController

## 3.2. Sequence Diagram (SD)

![US014_SD](US014_SD.svg)

## 3.3. Class Diagram (CD)

![US014_CD](US014_CD.svg)

# 4. Tests

*In this section, it is suggested to systematize how the tests were designed to allow a correct measurement of
requirements fulfilling.*

**_DO NOT COPY ALL DEVELOPED TESTS HERE_**

**Common Data**

    private static RegisterClientController clientCtrl = new RegisterClientController();

    private static ClientDTO CLIENT_DTO;
    private static ClientMapper CLIENT_MAPPER;
    private static ClientStore CLIENT_STORE;

    private void getData() {

        CLIENT_DTO = new ClientDTO(11111111, "u1", "a1", Constants.SEX_MALE, new Date(2002, 01, 01), 111111111, "u1@gmail.com", 111111111);
        CLIENT_MAPPER = new ClientMapper();
        CLIENT_STORE = new ClientStore();

        Company.setSnsUserMapper(CLIENT_MAPPER);
        Company.setClientStore(CLIENT_STORE);
    }

**Test1: createClient()**

    @Test
    public void createClient() {
        getData();
        Client expected = CLIENT_MAPPER.toModel(CLIENT_DTO);
        Client actual = clientCtrl.createClient(CLIENT_DTO);
        Assert.assertEquals(expected.getName(), actual.getName());
        Assert.assertEquals(expected.getAddress(), actual.getAddress());
        Assert.assertEquals(expected.getSnsNumber(), actual.getSnsNumber());
        Assert.assertEquals(expected.getEmail(), actual.getEmail());
        Assert.assertEquals(expected.getCcNumber(), actual.getCcNumber());
        Assert.assertEquals(expected.getSex(), actual.getSex());
        Assert.assertEquals(expected.getBirthDate(), actual.getBirthDate());
    }

# 5. Construction (Implementation)

- At first an SSD and SD design was applied
- After this design the code was implemented according to the design

# 6. Integration and Demo

- This functionality is related to many others in the system, so many classes and methods are shared (they are the same)
  and their interpretation is easier when completed

# 7. Observations

- The SD design implementation was a little confusing





