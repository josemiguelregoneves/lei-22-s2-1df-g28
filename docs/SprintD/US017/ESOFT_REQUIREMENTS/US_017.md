# US 017 - As a center coordinator, I want to import data from a legacy system that was used in the past to manage centers.

## 1. Requirements Engineering

### 1.1. User Story Description

As a center coordinator, I want to import data from a legacy system that was used in the past to manage centers.

### 1.2. Customer Specifications and Clarifications

#### From the Specifications Document:

#### From the client Clarifications:


### 1.3. Acceptance Criteria

- AC1:
    - Two sorting algorithms should be implemented (to be chosen manually by the coordinator).

- AC2:
    - The center coordinator must be able to choose the file that is to be uploaded.	

### 1.4. Found out Dependencies

To load a file with legacy data, there must be a registered center coordinator.

### 1.5 Input and Output Data

#### Input Data

- Typed data: file location;

#### Output Data

- Success or insuccess of the operation.

### 1.6. System Sequence Diagram (SSD)

![US017_SSD](US017_SSD.svg)

### 1.7 Other Relevant Remarks

Special Requirements:

- The path of the file must be specified

How often this US is held:

- Whenever a coordinator wants/needs to load data from a legacy system

## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt

![US017_MD](US017_MD.svg)

### 2.2. Other Remarks

Note: no other remarks available

## 3. Design - User Story Realization

### 3.1. Rationale

**The rationale grounds on the SSD interactions and the identified input/output data.**

| Interaction ID                                               | Question: Which class is responsible for... | Answer      | Justification (with patterns)  |
|:-------------------------------------------------------------|:--------------------------------------------|:------------|:---------------------------- |
| Step 1: selects the option to load data from a legacy system | ... loading the data                        | ReadClientSchedulesFromFile | readClientFromFile()
| Step 2: requests the location of the file                    | n/a                                         | n/a         | n/a                              |
| Step 3: types requested data                                 | ... saving the input data?                  | Utils       | readLineFromConsole()                              |
| Step 4: informs the operation success/insuccess              | ... informing operation success/insuccess?  | SortClientScheduleUI | n/a                              |

### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are:


  * ReadClientSchedulesFromFile

Other software classes (i.e. Pure Fabrication) identified:

* SortClientScheduleUI
* SortClientScheduleController

## 3.2. Sequence Diagram (SD)

![US017_SD](US017_SD.svg)

## 3.3. Class Diagram (CD)

![US017_CD](US017_CD.svg)

# 4. Tests

*In this section, it is suggested to systematize how the tests were designed to allow a correct measurement of
requirements fulfilling.*

**_DO NOT COPY ALL DEVELOPED TESTS HERE_**

# 5. Construction (Implementation)

- At first an SSD and SD design was applied
- After this design the code was implemented according to the design

# 6. Integration and Demo

- This functionality is related to many others in the system, so many classes and methods are shared (they are the same)
  and their interpretation is easier when completed

# 7. Observations

- The SD design implementation was a little confusing





