import java.util.ArrayList;

public class SNSUserStore {
    /**
     * list of all snsUser that arrive at the vaccination center
     */
    private ArrayList<SNSUser> snsUsersList = new ArrayList<>();

    /**
     * constant of the first index of the list
     */
    private static final int FIRST_INDEX = 0;

    /**
     * method to get the sns users list
     *
     * @return snsUsersList
     */
    public ArrayList<SNSUser> getSnsUsersList() {
        return snsUsersList;
    }

    /**
     * method to ser the sns users list
     *
     * @param snsUsersList
     */
    public void setSnsUsersList(ArrayList<SNSUser> snsUsersList) {
        this.snsUsersList = snsUsersList;
    }

    /**
     * method to check/validate and add the sns user to the waiting room list
     *
     * @param user snsUser
     * @return true/false
     */
    public boolean checkSnsUser(SNSUser user) {
        if (user != null) {
            snsUsersList.add(user);
            return true;
        } else {
            return false;
        }
    }

    /**
     * method to get the sns user the arrive first at the center
     *
     * @return firstUser
     */
    public SNSUser getFirstSnsUser() {
        SNSUser firstUser = snsUsersList.get(FIRST_INDEX);
        snsUsersList.remove(FIRST_INDEX);
        return firstUser;
    }
}
