import java.util.Date;

public class SNSUser {
    /**
     * name
     */
    private String name;

    /**
     * address
     */
    private String address;

    /**
     * phone number
     */
    private int phoneNumber;

    /**
     * email
     */
    private String email;

    /**
     * birth date
     */
    private Date birthDate;

    /**
     * citizen card number
     */
    private int ccNumber;

    /**
     * tin number
     */
    private int tinNumber;

    /**
     * sns number
     */
    private int snsNumber;

    public SNSUser(String name, String address, int phoneNumber, String email, Date birthDate, int ccNumber, int tinNumber, int snsNumber) {
        this.name = name;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.birthDate = birthDate;
        this.ccNumber = ccNumber;
        this.tinNumber = tinNumber;
        this.snsNumber = snsNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public int getCcNumber() {
        return ccNumber;
    }

    public void setCcNumber(int ccNumber) {
        this.ccNumber = ccNumber;
    }

    public int getTinNumber() {
        return tinNumber;
    }

    public void setTinNumber(int tinNumber) {
        this.tinNumber = tinNumber;
    }

    public int getSnsNumber() {
        return snsNumber;
    }

    public void setSnsNumber(int snsNumber) {
        this.snsNumber = snsNumber;
    }
}
