# Supplementary Specification (FURPS+)

## Functionality

_Specifies functionalities that:_

- _are common across several US/UC;_
- _are not related to US/UC, namely: Audit, Reporting and Security._

- Password must hold seven alphanumeric characters, including three capital letters and two digits
- User Authentication
- Send an SMS to the client informing he can leave the vaccination center
- Both kinds of vaccination centers are characterized by a name, an address, a phone number, an e-mail address, a fax
  number, a website address, opening and closing hours, slot duration (e.g.: 5 minutes) and the maximum number of
  vaccines that can be given per slot (e.g.: 10 vaccines per slot)
- The vaccine administration process comprises one or more age groups
- The application should check the vaccination center capacity for that day/time
- Confirm that the vaccination is scheduled and inform the user that (s)he should be at the selected vaccination center
  at the scheduled day and time.
- As if the SNS User allows the program to send an SMS to him/her with information about the scheduled appointment.
- If the user authorizes the sending of the SMS, the application should send an SMS message when the vaccination event
  is scheduled and registered in the system.
- Allow the Recepcionist to schedule the vaccine of an SNS User at one vaccination center.
- Allow the Recepcionist to register the arrival of the user to take the respective vaccine.
- If the information is correct, the receptionist acknowledges the system that the user is ready to take the vaccine
- Check the list of SNS users that are present in the vaccination center to take the vaccine
- Give instructions regarding the vaccine to be administered
- Allow each nurse to register the event in the system
- The system should be able to notify (e.g.: SMS or email) the user that his/her recovery period has ended.
- The nurse should be able to record the adverse reactions in the system.
- Any SNS user can request the issuance of the EU COVID Digital Certificate

## Usability

_Evaluates the user interface. It has several subcategories,among them: error prevention; interface aesthetics and
design; help and documentation; consistency and standards._

- The application was development taking into account the correction of errors
- The application provides to the SNS User many functionalities (eg: schedule the vaccination process, requests the
  certificate, receive SMS and control the recovery time)
- The application provides to the Nurse many functionalities (eg: check SNS User datas and vaccine type)
- The application can also used by the Center Coordinator to monitor the vaccination process
- The program provides a simple and interactive menu for the user
- The main menu is divided into submenus (one for each type of employee/user)
- It contains a user manual development

## Reliability

_Refers to the integrity, compliance and interoperability of the software. The requirements to be considered are:
frequency and severity of failure, possibility of recovery, possibility of prediction, accuracy, average time between
failures._

- To reduce the program errors, the team did very unitary tests, debugging and compilation processes
- All program code has been revised several times to catch and correct as many faults/errors
- The program also has a strong design system behind it to facilitate program development
- All design was done using software engineering techniques such as Domain Model, FURPS+ and Use Cases Diagram
- With all these attributes and steps the team ensured the integrity and success of the application

## Performance

_Evaluates the performance requirements of the software, namely: response time, start-up time, recovery time, memory
consumption, CPU usage, load capacity and application availability._

- Simple components in interface will improve the use of the application
- Optimization processes will be applied in the application
- With all the factors refeered in Reability and Performance, the performance requirements of the software were improved

## Supportability

_The supportability requirements gathers several characteristics, such as: testability, adaptability, maintainability,
compatibility, configurability, installability, scalability and more._

- The application must support, at least, the Portuguese and the English languages.
- The application contains a user manual
- The application must have a simple and interactive menu
- Adapted to other diseases and pandemic events
- Easy to configure the application interface

## +

## Design Constraints

_Specifies or constraints the system design process. Examples may include: programming languages, software process,
mandatory standards/patterns, use of development tools, class library, etc._

JAVA programming language, JavaFX 11, JUnit 5, JaCoCo plugin, Trello, SCRUM development method, CamelCase, Javadoc
documentation, Unit tests, use of IDEs such as IntelliJ, NetBeans, VisualStudio Code

- To make the Graphical User Interface the team used JAVAFX
- The team used the Visual Studio Code, Intellij IDEA and Visual Paradigm
- All the code was written in JAVA, using Intellij IDEA
- The Glossary, Use Cases Diagram, Table of Associations, Table of Classes and FURPS+ were made in the Visual Studio
  Code and Intellij IDEA
- The Domain Model was made in Visual Paradigm
- To manage the team's work, Trello and Bitbucket were used
- All design was done using software engineering techniques such as Domain Model, Conceptual Classes Table, Associations
  Table, FURPS+ and Use Cases Diagram
- During the project, many native and non native java libraries were imported

## Implementation Constraints

_Specifies or constraints the code or construction of a system such as: mandatory standards/patterns, implementation
languages,database integrity, resource limits, operating system._

- Our team only implements JAVA language (JAVA Basics, JAVA OO and JAVA Swing)
- All the issues were fixed using JAVA

## Interface Constraints

_Specifies or constraints the features inherent to the interaction of the system being developed with other external
systems._

- The application provides an interface to comunicate with the employees and other users

## Physical Constraints

_Specifies a limitation or physical requirement regarding the hardware used to house the system, as for example:
material, shape, size or weight._

- The product consists of an application so it must be used on a computer or mobile phone
- For nurses who works all the day with de application, it takes very battery from the
- In the case of nurses who work all day with the application, the battery consumption of the device used is higher
