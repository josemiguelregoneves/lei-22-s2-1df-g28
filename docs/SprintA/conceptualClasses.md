# Conceptual Classes #

| Category |Candidate Classes |
| -------- | ----------------- |
| Places | Vaccination Centers (Community Mass Vaccination Center, Health Care Center, Other Centers) |
| Roles of People or Organizations| SNS User, Nurse, Receptionist, Center Coordinator, Administrator |
| Transactions | Vaccination Scheduling, Vaccine Administration, Vaccination Process, Vaccination Certificate |
| Transaction Records/Registers | SNS User Data Registration, After-Administration Vaccine Data Registration |
| Products or Services related to Transaction or Transaction Line | Vaccine, Adverse Reactins Check-up, User Presence verification, Vaccination Center Monitoring and Analysis |
| Organizations (Other) | Company (DGS) |
| Noteworthy Events | Vaccination Scheduling, Vaccine Administration, Vaccination Process |
| Description of things | Vaccine Type |
| Other (External) Systems | SMS Vaccination Confirmation, Charts, Statistics, Reports |
